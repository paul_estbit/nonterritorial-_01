
import re
import hashlib
import hmac
import random
import string
from string import letters
import logging
import json

from google.appengine.ext import blobstore
from google.appengine.ext.webapp import blobstore_handlers
from google.appengine.ext import ndb

from google.appengine.api import images
from google.appengine.api import mail
from google.appengine.api import app_identity
from google.appengine.api import search

from google.appengine.datastore.datastore_query import Cursor

import cloudstorage as gcs
import slugify

#for mandrill api
from google.appengine.api import urlfetch

import model

app_id = app_identity.get_application_id()

secret = 'Nont3RRiT0rA1'
sender_email = "shilgalis@nonterritorial.net"

# Mandrill
mandrill_key = "9y6hFh8u9Ii6IZj5Ib2Mbg"# "qODMJ7be5Cy68y7M7z6q4w"


#PW HASHING
def make_pw_hash(name, pw, salt = None):
    if not salt:
        salt = make_salt()
    h = hashlib.sha256(name + pw + salt).hexdigest()
    return '%s,%s' % (salt, h)
    
def make_salt(length=5):
    return ''.join(random.choice(letters) for x in xrange(length))
    
# returns a cookie with a value value|hashedvalue
def make_secure_val(val):
    return '%s|%s' % (val, hmac.new(secret, val).hexdigest())
# returns the origional value and validates if given hashed cookie matches our hash of the value    
def check_secure_val(secure_val):
    val = secure_val.split('|')[0]
    if secure_val == make_secure_val(val):
        return val
        
def valid_pw(name, password, h):
    salt = h.split(',')[0]
    return h == make_pw_hash(name, password, salt)


#REGEX for register validtion
USER_RE = re.compile(r"^[a-zA-Z0-9_-]{3,20}$")
def valid_username(username):
    return username and USER_RE.match(username)

def unique_username(username):
    existing = model.User.query(model.User.username == username).get()
    if existing:
        return False
    else:
        return True

PASS_RE = re.compile(r"^.{3,20}$")
def valid_password(password):
    return password and PASS_RE.match(password)

EMAIL_RE  = re.compile(r'^[\S]+@[\S]+\.[\S]+$')
def valid_email(email):
    return email and EMAIL_RE.match(email)
def unique_email(email):
    existing = model.User.query(model.User.email == email).get()
    if existing:
        return False
    else:
        return True

# =========================================================
# Users & Auth
# =========================================================

def changeEmail(new_email, userNameID, password, userObj):
    pw_hash = userObj.pw_hash
    errors = []
    if valid_pw(userNameID, password, pw_hash):
        if not valid_email(new_email):
            errors.append("Please provide a valid email.")
        elif not unique_email(new_email):
            errors.append("That email's taken.")
        else:
            if userObj.email == userNameID:
                new_pw_hash = make_pw_hash(new_email, password)
                userObj.pw_hash = new_pw_hash
            userObj.email = new_email
            userObj.put()
    else:
        errors.append("Wrong password")

    return errors

def changeUsername(new_username, userNameID, password, userObj):
    pw_hash = userObj.pw_hash
    errors = []

    logging.error(" . . . . . .. . . ")
    logging.error(password)
    logging.error(userNameID)
    logging.error(valid_pw(userNameID, password, pw_hash))
    logging.error(" . . . . . . . . . .")

    if valid_pw(userNameID, password, pw_hash):
        if not valid_username(new_username):
            errors.append("Please provide a valid username ( can only be numbers, letters and has to be over 3 characters long ).")
        elif not unique_username(new_username):
            errors.append("That username's taken.")
        else:
            if userObj.username == userNameID:
                new_pw_hash = make_pw_hash(new_username, password)
                userObj.pw_hash = new_pw_hash
            slug = slugify.slugify(new_username)
            userObj.slug = slug
            userObj.username = new_username
            userObj.put()
    else:
        errors.append("Wrong password")

    return errors

def changePassword(new_password, userNameID, password, userObj):
    pw_hash = userObj.pw_hash
    errors = []

    logging.error(" . . . . . .. . . ")
    logging.error(password)
    logging.error(userNameID)
    logging.error(valid_pw(userNameID, password, pw_hash))
    logging.error(" . . . . . . . . . .")

    if valid_pw(userNameID, password, pw_hash):
        if not valid_password(password):
            errors.append("Please provide a valid password ( it needs to be between 3 and 20 characters long ).")
        else:
            new_pw_hash = make_pw_hash(userNameID, new_password)
            userObj.pw_hash = new_pw_hash
            userObj.put()
    else:
        errors.append("Wrong password")

    return errors

def check_login(self):
    if not self.user:
        self.redirect("/login")

# ================================
# Using this to create a new temporary password
# ================================
def generate_random_token(N):
    return ''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(N))

# =========================================================
# NDB stuff
# =========================================================

def getKey(kind, ID):
    return ndb.Key(kind, ID)

def getModel(kind):
    return ndb.Model._lookup_model(kind)

def existingEntity(kind, property, value):
    Model = getModel(kind)
    q = Model.query()
    q = q.filter(Model._properties[property] == value)
    entity = q.get()
    if entity:
        return entity
    else:
        return False

def getCursor(self):
    curs = Cursor(urlsafe=self.request.get('cursor'))
    return curs


def stringLocationFromJson(stringJson):
    try:
        location = json.loads(stringJson)
        latitude = location["lat"]
        longitude = location["lng"]
        stringLocation = str(latitude)+','+str(longitude)
    except:
        logging.error("error getting string location from json: %s" % stringJson)
        stringLocation = ""
    return stringLocation



# =========================================================
# Slide Show functions
# =========================================================
def create_slideshow(userObj, title, description):

    flightNumber = userObj.ideaCount + 1
    userObj.ideaCount = flightNumber
    userObj.put()

    #  edited to always make new slideshows unapproved
    # if userObj.approved:
    #     approved = True
    # else:
    #     approved = False
    approved = False

    formated_flight_number = "%03d" % (flightNumber,)

    slug = slugify.slugify(title)
    new_slideshow = model.SlideShow(
        user=userObj.key, 
        flightCode=userObj.flightCode,
        flightNumber=formated_flight_number,
        name=userObj.name, 
        surname=userObj.surname, 
        userType=userObj.userType,
        title=title, 
        description=description, 
        slug=slug,
        location=userObj.location,
        address=userObj.address,
        country=userObj.country,
        locality=userObj.locality,
        geopoint=userObj.geopoint,
        approved=approved
        )
    new_slideshow.put()

    # saving a reference to the slideshow to keep track of the fact that freeports and airports can only add 1 slideshow each
    if userObj.userType == "freeport" or userObj.userType == "airport" or userObj.userType == "gallery":
        userObj.editedSpace = True
        userObj.slideShow = new_slideshow.key
        userObj.put()

    return new_slideshow

def create_slide(self, slideShowObj, slideType):

    title = self.request.get("title")
    description = self.request.get("description")

    slideTitle = self.request.get("slideTitle")
    slideDescription = self.request.get("slideDescription")
    slideVideoUrl = self.request.get("slideVideoUrl")

    logging.error("- - - - - - - - - VIDEO - - - - - - - - - - ")
    logging.error(slideVideoUrl)

    def get_format(download_url, format):
        # logging.info("FORMAT: %s " % format)
        original_file = model.File.query(model.File.download_url == download_url).get()
        # logging.info("original_file: %s " % original_file.key.id())
        original_filename = original_file.filename
        # logging.info("original_filename: %s " % original_filename)
        import os
        filename_no_extension = os.path.splitext(original_filename)[0]
        # logging.info("filename_no_extension: %s " % filename_no_extension)
        mp4_filename = "%s.mp4" % filename_no_extension
        webm_filename = "%s.webm" % filename_no_extension
        ogv_filename = "%s.ogv" % filename_no_extension

        # logging.info("mp4_filename: %s " % mp4_filename)

        format_url = None

        if format == "mp4":
            new_file = model.File.query(model.File.filename == mp4_filename).get()
            if new_file:
                format_url = new_file.download_url
                # logging.info("format_url: %s " % format_url)
        elif format == "webm":
            new_file = model.File.query(model.File.filename == webm_filename).get()
            if new_file:
                format_url = new_file.download_url
        elif format == "ogv":
            new_file = model.File.query(model.File.filename == ogv_filename).get()
            if new_file:
                format_url = new_file.download_url

        return format_url

    if title:
        slideTitle = title
    if description:
        slideDescription = description

    file_exists = False
    try:
        file_req = self.request.POST["file"]
        # logging.info("yes there is a file here.....")
        file_exists = True
        logging.info("file exists.....%s" % file_exists)
    except:
        file_req = None
        # logging.info("nope, no file here.....")



    if file_exists:
        import fileHandlers
        uploading_user = self.user
        if uploading_user.userType == "artist" or uploading_user.userType == "airport" or uploading_user.userType == "freeport" or uploading_user.userType == "gallery":
            logging.info("lets upload the file....")
            file_obj = fileHandlers.upload_file(file_req)
            logging.info("file_obj: %s" % file_obj.key.id())
            file_key = file_obj.key
            image_url = file_obj.image_url
            logging.info("image_url: %s" % image_url)
            video_url = file_obj.download_url
        else:
            file_key = None
            image_url = None
            video_url = None
    else:
        file_key = None
        image_url = None
        video_url = None

    video_embed = self.request.get("video_embed")
    slideOrder = self.request.get("slideOrder")

    if slideVideoUrl:
        logging.info("there is a video URL... get the different formats..........")
        video_url_mp4 = get_format(slideVideoUrl, 'mp4')
        video_url_webm = get_format(slideVideoUrl, 'webm')
        video_url_ogv = get_format(slideVideoUrl, 'ogv')
    else:
        video_url_mp4 = None
        video_url_webm = None
        video_url_ogv = None
        if video_url:
            slideVideoUrl = video_url

    new_slide = model.Slide(
        user=self.user.key, 
        slideShow=slideShowObj.key, 
        slideType=slideType,
        title = slideTitle,
        description = slideDescription,
        file_obj = file_key,
        image_url = image_url,
        video_url = slideVideoUrl,
        video_url_mp4 = video_url_mp4,
        video_url_webm = video_url_webm,
        video_url_ogv = video_url_ogv,
        video_embed = video_embed,
        slideOrder = int( slideOrder )
        )
    new_slide.put()

    import search
    search.indexSlide(new_slide)

    return new_slide

def update_slide(self, slideShowObj, slideObj):
    title = self.request.get("title")
    description = self.request.get("description")

    slideTitle = self.request.get("slideTitle")
    slideDescription = self.request.get("slideDescription")
    slideVideoUrl = self.request.get("slideVideoUrl")
    logging.error("slideVideoUrl . . . .  . . >>>")
    logging.error(slideVideoUrl)

    def get_format(download_url, format):
        logging.info("FORMAT: %s " % format)
        original_file = model.File.query(model.File.download_url == download_url).get()
        logging.info("original_file: %s " % original_file.key.id())
        original_filename = original_file.filename
        logging.info("original_filename: %s " % original_filename)
        import os
        filename_no_extension = os.path.splitext(original_filename)[0]
        logging.info("filename_no_extension: %s " % filename_no_extension)
        mp4_filename = "%s.mp4" % filename_no_extension
        webm_filename = "%s.webm" % filename_no_extension
        ogv_filename = "%s.ogv" % filename_no_extension

        logging.info("mp4_filename: %s " % mp4_filename)

        format_url = None

        if format == "mp4":
            new_file = model.File.query(model.File.filename == mp4_filename).get()
            if new_file:
                format_url = new_file.download_url
                logging.info("format_url: %s " % format_url)
        elif format == "webm":
            new_file = model.File.query(model.File.filename == webm_filename).get()
            if new_file:
                format_url = new_file.download_url
        elif format == "ogv":
            new_file = model.File.query(model.File.filename == ogv_filename).get()
            if new_file:
                format_url = new_file.download_url

        return format_url

    if title:
        slideTitle = title
        slideShow = slideObj.slideShow.get()
        slideShow.title = title
        slideShow.put()
    if description:
        slideDescription = description

    file_exists = False
    try:
        file_req = self.request.POST["file"]
        file_exists = True
    except:
        file_req = None

    if file_exists:
        import fileHandlers
        file_obj = fileHandlers.upload_file(file_req)

        if slideObj.file_obj:
            if slideObj.image_url:
                fileHandlers.delete_from_gcs(slideObj.file_obj.get().gcs_filename, True)
            else:
                fileHandlers.delete_from_gcs(slideObj.file_obj.get().gcs_filename, False)

        file_key = file_obj.key
        image_url = file_obj.image_url
        video_url = file_obj.download_url
    else:
        file_key = None
        image_url = None
        video_url = None

    video_embed = self.request.get("video_embed")
    slideOrder = self.request.get("slideOrder")

    if slideVideoUrl:
        logging.info("there is a video URL... get the different formats..........")
        video_url_mp4 = get_format(slideVideoUrl, 'mp4')
        video_url_webm = get_format(slideVideoUrl, 'webm')
        video_url_ogv = get_format(slideVideoUrl, 'ogv')
        video_url = slideVideoUrl
    else:
        video_url_mp4 = slideObj.video_url_mp4
        video_url_webm = slideObj.video_url_webm
        video_url_ogv = slideObj.video_url_ogv


    # new_slide = model.Slide(
    #     user=self.user.key, 
    #     slideShow=slideShowObj.key, 
    #     slideType=slideType,
    #     title = slideTitle,
    #     description = slideDescription,
    #     file_obj = file_key,
    #     image_url = image_url,
    #     video_url = video_url,
    #     video_embed = video_embed,
    #     slideOrder = int( slideOrder )
    #     )

    # slideObj.user
    # slideObj.slideShow
    # slideObj.slideType = slideType
    slideObj.title = slideTitle
    slideObj.description = slideDescription
    slideObj.file_obj = file_key
    slideObj.image_url = image_url
    slideObj.video_url = video_url
    slideObj.video_url_mp4 = video_url_mp4
    slideObj.video_url_webm = video_url_webm
    slideObj.video_url_ogv = video_url_ogv
    slideObj.video_embed = video_embed
    slideObj.slideOrder = int( slideOrder )

    slideObj.put()

    import search
    search.indexSlide(slideObj)

    return slideObj


# =========================================================
# Flight Code
# =========================================================
def get_flight_code(userObj):
    def check_and_create_code(userObj, attempt):
        logging.info("creating flight code......")
        random_letters = "abcdefghijklmnopqrstuvwxyznonterritorial"
        attempt_start = attempt
        attempt_end = attempt + 2
        name = userObj.name.lower()
        surname = userObj.surname.lower()
        
        if attempt > ( len(surname) - 3 ):
            surname = random_letters

        code = "%s%s" % ( name[:1], surname[attempt_start:attempt_end] )
        
        logging.info(".-.-.-.-.")
        logging.info(code)
        logging.info(surname[attempt_start:attempt_end])
        logging.info(attempt)

        if not model.User.query(model.User.flightCode == code.upper()).get():
            logging.info("founde a unique code: %s" % code)
            return code.upper()
        else:
            attempt += 1
            logging.info("new attempt: %s" % attempt)
            return check_and_create_code(userObj, attempt)

    if userObj.name == "Your name" or userObj.surname =="Your surname":
        return False
    else:
        if len(userObj.name) > 0 and len(userObj.surname) > 0:
            logging.info( check_and_create_code(userObj, 0) )
            final_code = check_and_create_code(userObj, 0)
            
            return final_code
            # return code


# =========================================================
# Search & Location
# =========================================================

def stringToGeoPt(latLng):
    latitude = float( latLng.split(",")[0] )
    longitude = float( latLng.split(",")[1] )
    if latitude and longitude:
        #geopoint = search.GeoPoint( latitude, longitude )
        geopoint = ndb.GeoPt(latitude, longitude)
    else:
        geopoint = None

    return geopoint

def stringToSearchGeoPt(latLng):
    latitude = float( latLng.split(",")[0] )
    longitude = float( latLng.split(",")[1] )
    if latitude and longitude:
        geopoint = search.GeoPoint( latitude, longitude )
    else:
        geopoint = None

    return geopoint


# =========================================================
# Codes and unique strings
# =========================================================
# only works 1-999, 
def get_short_code(blog_number):
    letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
    if blog_number > 999:
        letter_index = blog_number - 1000
        letter_index = int(letter_index) / 100
        letter_index = letter_index
        logging.error("Letter Index: %s" % str(letter_index))
        letter_component = letters[letter_index]
        subtract = 1000 + ( letter_index*100 )
        number_component = blog_number - subtract
        number_component = "%02d" % (number_component,)
        short_code = letter_component + number_component
    else:
        short_code = blog_number

    return str(short_code)

def checkIdeaCode(ideaCode):
    errors = []
    try:
        ideaCode.decode('ascii')
    except:
        ideaCode = False
        errors.append("Please make sure your code is a standard ascii character (A - Z)")
    
    if ideaCode:
        ideaCode = ideaCode.upper()
    
    ideaCode_RE = re.compile(r"^[A-Z]{3}")
    
    if ideaCode and ideaCode_RE.match(ideaCode):
        return {"code": ideaCode, "errors": []}
    else:
        errors.append("Please make sure your code is exactly 3 letters long and contains no numbers")
        return {"code": False, "errors": errors}


# =========================================================
# Counters
# =========================================================
def get_counter():
    counter = model.Counter.query().get()
    if not counter:
        counter = model.Counter()
        counter.put()
    return counter 

def increment_counter(countType):
    counter = model.Counter.query().get()
    if not counter:
        counter = model.Counter()
    count = getattr(counter, countType)
    count += 1
    setattr(counter, countType, count)
    counter.put()
def decrement_counter(countType):
    counter = model.Counter.query().get()
    if not counter:
        counter = model.Counter()
    count = getattr(counter, countType)
    count -= 1
    setattr(counter, countType, count)
    counter.put()


# =========================================================
# Ideas
# =========================================================

def get_file_preview_html(fileEntity):
    
    fileID = fileEntity.key.id()

    if fileEntity.fileType == "image":
        download_html = "<img src='%s'>" % fileEntity.image_url
    else:
        download_html = "<a href='%s'>Download</a>" % fileEntity.download_url

    html ="""
        <div id="file-container-%s">
        %s
        <br>
        <p>
          <input type="checkbox" name="delete-file-%s" id="delete-%s" value="%s" />
          <label for="delete-%s">Delete</label>
        </p>
        </div>
    """ % ( fileID, download_html, fileID, fileID, fileID, fileID )

    return html


def setIdeaNumber(idea, userObj):
    counter = get_counter()
    ideaCount = counter.ideas + 1
    flightCode = userObj.flightCode
    if not flightCode:
        return False
    else:
        ideaNumber = flightCode + str(ideaCount)
        return ideaNumber

def save_idea(self):
    import fileHandlers

    userObj = self.user

    errors = []
    error = False

    name = self.request.get("name")
    description = self.request.get("description")
    ideaLocation = self.request.get("ideaLocation")
    notes = self.request.get("notes")
    ideaCover = self.request.get("ideaCover")

    address = self.request.get("address")
    country = self.request.get("country")
    locality = self.request.get("locality")

    tags = self.request.get("tags")
    if tags:
        tags = tags.split(",")
    else:
        tags = []
    
    public = self.request.get("public")
    if public == "yes":
        public = True
    elif public == "no":
        public = False
    else:
        public = False

    try:
        stringLocation = stringLocationFromJson(ideaLocation)
        # latitude = location["lat"]
        # longitude = location["lng"]
        # stringLocation = str(latitude)+','+str(longitude)
    except:
        error = True
        errors.append("You need to select a location")
        logging.error("no location provided or there was an error")

    slug = slugify.slugify(name)

    ideaID = self.request.get("ideaID")

    deleted_file_ids = []

    idea = model.Idea()

    if ideaID:
        logging.error("------------------------ there is an ideaID")
        idea = model.Idea.get_by_id(int(ideaID))
        new_file_list = []
        for fileKey in idea.files:
            file_id_to_delete = self.request.get('delete-file-%s' % fileKey.id())
            if file_id_to_delete:
                file_to_delete = model.File.get_by_id(int(file_id_to_delete))
                if file_to_delete.fileType == "image":
                    isImage = True
                else:
                    isImage = False
                deleted_file_ids.append(file_to_delete.key.id())
                fileHandlers.delete_from_gcs(file_to_delete.gcs_filename, isImage)
                file_to_delete.key.delete()
            else:
                new_file_list.append(fileKey)

            idea.files = new_file_list
        # !! add file handling
        # - flag to delete old file
        # - overwrite existing file(s)
        # - don't do anything, i.e. leave the file as it is

        already_exists = existingEntity("Idea", "slug", slug)
        if already_exists:
            # logging.error("------------------------ there is already an idea with this name")
            # logging.error("------------------------ id of existing idea: %s " % str(already_exists.key.id()))
            # logging.error("------------------------ this idea %s " % str(ideaID))
            if already_exists.key.id() != int(ideaID):
                error = True
                errors.append("The name '%s' has already been taken, please choose another name." % name)
            else:
                logging.error("------------------------ there is no ideaID")
                #increment_counter('ideas')

        else:
            logging.error("------------------------ there is no ideaID")
            #increment_counter('ideas')

    else:
        already_exists = existingEntity("Idea", "slug", slug)
        if already_exists:
            # logging.error("------------------------ there is already an idea with this name")
            # logging.error("------------------------ id of existing idea: %s " % str(already_exists.key.id()))
            if already_exists.key.id():
                error = True
                errors.append("The name '%s' has already been taken, please choose another name." % name)
            else:
                logging.error("------------------------ there is no ideaID")
                increment_counter('ideas')

        else:
            logging.error("------------------------ there is no ideaID")
            increment_counter('ideas')


    #file_obj = fileHandlers.upload_file(file_req)

    #files = idea.files
    #files.append(file_obj.key)

    if userObj.flightCode:
        ideaNumber = setIdeaNumber(idea, userObj)
    else:
        error = True
        errors.append("You need to set a Flight Code")

    if not ideaNumber:
        error = True
        logging.error("Could not create a unique idea number - userID %s" % userObj.key.id())
    
    if not error:
        idea.number = ideaNumber
        idea.location = stringLocation
        idea.geopoint = stringToGeoPt(stringLocation)
        idea.address = address
        idea.country = country
        idea.locality = locality
        idea.slug = slug
        idea.artist = userObj.key
        idea.name = name
        idea.description = description
        idea.notes = notes
        if public:
            idea.public = public
        if ideaCover:
            idea.cover_image = ideaCover
        idea.put()

        save_tag_idea(tags, idea)

        # indexing the idea so that its searchable
        import search
        search.indexIdea(idea)

        idea_return = {
            "idea":idea,
            "errors": errors,
            "deleted_file_ids": deleted_file_ids
        }

        return idea_return

    else:
        idea_return = {
            "idea":False,
            "errors": errors,
            "deleted_file_ids": deleted_file_ids
        }

        return idea_return


def save_tag_idea(tags, idea):
    logging.error(" --------- NOT IMPLEMENTING TAGS........ ADD THIS FUNCTIONALITY BELOW ---------")
    # old_tag_ideas = model.TagIdea.query(model.TagIdea.User == idea.artist, model.TagIdea.Idea == idea.key).fetch()

    # new_tag_ideas = []
    # for tagID in tags:
    #     tagObj = model.Tag.get_by_id(int(tagID))
    #     tag_idea = model.TagIdea.query(model.TagIdea.Tag == tagObj.key, model.TagIdea.User == idea.key).get()
    #     if not tag_idea:
    #         tag_idea = model.TagIdea( User=idea.artist, Tag=tagObj.key, Idea=idea.key )
    #         tag_idea.put()

    #     new_tag_ideas.append(tag_idea)



# =========================================================
# Google APIs
# =========================================================

def goog_shorten_url(long_url):
    import urllib2

    # req_url = "https://www.googleapis.com/urlshortener/v1/url"
    # form_fields = {
    #   "key": url_shortener_key,
    #   "longUrl": long_url
    # }
    # form_data = urllib.urlencode(form_fields)
    # result = urlfetch.fetch(url=req_url,
    #     payload=form_data,
    #     method=urlfetch.POST,
    #     headers={'Content-Type': 'application/x-www-form-urlencoded'})

    # logging.error(result)
    # logging.error(dir(result))
    # logging.error(result.final_url)


    # using urllib2
    post_url = 'https://www.googleapis.com/urlshortener/v1/url'
    postdata = {'longUrl':long_url, "key": url_shortener_key}
    headers = {'Content-Type':'application/json'}
    req = urllib2.Request(
        post_url,
        json.dumps(postdata),
        headers
    )
    result = urllib2.urlopen(req).read()
    logging.error(result)
    logging.error( json.loads(result)['id'] )

    # using Requests
    # post_url = 'https://www.googleapis.com/urlshortener/v1/url'
    # payload = {'longUrl': long_url, "key": url_shortener_key}
    # headers = {'content-type': 'application/json'}
    # r = requests.post(post_url, data=json.dumps(payload), headers=headers)
    # logging.error("RESPONSE URL: ")
    # logging.error(r)



# =========================================================
# Emails
# =========================================================

def render_and_send_email_template(email_type, recipient_email, recipient_name, text):

    body = "Your new temporary password is: %s , be sure to change your password when you log in." % text
    html_body = "Your new temporary password is: %s <br> be sure to change your password when you log in." % text

    # mail.send_mail('nonterritorial@gmail.com', recipient_email, "Nonterritorial", body)

    try:
        message = mail.EmailMessage(sender='info@nonterritorial.net', subject="Nonterritorial - temporary password")
        message.to = recipient_email
        message.body = body
        message.html = html_body
        message.send()
    except:
        logging.error("first email attempt failed")
        message = mail.EmailMessage(sender='nonterritorial@gmail.com', subject="Nonterritorial - temporary password")
        message.to = recipient_email
        message.body = body
        message.html = html_body
        message.send()


def send_approve_user_mail(userObj):
    body = "Congratulations you've been approved as an artist on Nonterritorial.net. You can now sign in and start uploading your ideas."
    html_body = 'Congratulations you\'ve been approved as an artist on Nonterritorial.net. You can now <a href="http://nonterritorial.net/login">sign in</a> and start uploading your ideas.'
    # mail.send_mail('nonterritorial@gmail.com', recipient_email, "Nonterritorial", body)
    try:
        message = mail.EmailMessage(sender='info@nonterritorial.net', subject="Nonterritorial - Artist approval granted")
        message.to = userObj.email
        message.body = body
        message.html = html_body
        message.send()
    except:
        logging.error("first email attempt failed")
        message = mail.EmailMessage(sender='nonterritorial@gmail.com', subject="Nonterritorial - Artist approval granted")
        message.to = userObj.email
        message.body = body
        message.html = html_body
        message.send()

def gallery_contact_email(gallery_email, user_email, message):
    
    body = "You have a message from %s :  %s" % (user_email, message)

    try:
        message = mail.EmailMessage(sender='info@nonterritorial.net', subject="Nonterritorial - Gallery Contact Request")
        message.to = gallery_email
        message.body = body
        message.send()
    except:
        logging.error("first email attempt failed")
        message = mail.EmailMessage(sender='nonterritorial@gmail.com', subject="Nonterritorial - Gallery Contact Request")
        message.to = gallery_email
        message.body = body
        message.send()


def send_mandrill_email(subject, html, text, to_email, from_email, inline_css):
    
    url = "https://mandrillapp.com/api/1.0/messages/send.json"

    form_json = {
            "key": mandrill_key,
            "message": {
                "html": html,
                "text": text,
                "subject": subject,
                "from_email": from_email,
                "from_name": "Podradio",
                "to": [{
                    "email": to_email,
                    "name": "",
                    "type": "to"
                }],
                "headers": {
                    "Reply-To": from_email
                },
                "important": False,
                "track_opens": True,
                "track_clicks": True,
                "auto_text": None,
                "auto_html": None,
                "inline_css": inline_css,
                "url_strip_qs": None,
                "preserve_recipients": False,
                "view_content_link": None,
                "bcc_address": None,
                "tracking_domain": None,
                "signing_domain": None,
            },
            "async": False,
            "ip_pool": "Main Pool",
            "send_at": None
        }
    

    result = urlfetch.fetch(url=url, payload=json.dumps(form_json), method=urlfetch.POST)

    logging.error("MANDRILL RESULT")

    logging.error(dir(result))
    logging.error(result.status_code)
    logging.error(json.loads(result.content))
    
def notify_user(message, userObj):
    if message == "application-accepted":
        subject = "Nonterritorial - application"
        email_message = "Congratulations your application has been accepted. Your status is now: %s" % userObj.userType
        email_html = "<p>%s</p>" % email_message
    elif message == "application-rejected":
        subject = "Nonterritorial - application"
        email_message = "We regret to inform you that your application has been rejected. Your status will remain: %s" % userObj.userType
        email_html = "<p>%s</p>" % email_message
    elif message == "global-curator-accepted":
        subject = "Nonterritorial - congratulations"
        email_message = "Congratulations you've been chosen as a global curator for Nonterritorial."
        email_html = "<p>%s</p>" % email_message
    elif message == "global-curator-rejected":
        subject = "Nonterritorial - Global Curator Status"
        email_message = "Your global curator status for Nonterritorial has been removed."
        email_html = "<p>%s</p>" % email_message
    elif message == "landing-permission-offered":
        subject = "Nonterritorial - You've been offered permission to land."
        email_message = "You've been offered permission to land, log in and take a look at your landing permissions for more details."
        email_html = "<p>%s</p>" % email_message
    elif message == "landing-permission-accepted":
        subject = "Nonterritorial - Your landing permission was accepted."
        email_message = "Your landing permission was accepted, log in and take a look at your landing permissions for more details."
        email_html = "<p>%s</p>" % email_message
    elif message == "landing-permission-rejected":
        subject = "Nonterritorial - Your landing permission was rejected."
        email_message = "Your landing permission was rejected, log in and take a look at your landing permissions for more details."
        email_html = "<p>%s</p>" % email_message

    # need to verify domain before Mandrill can be used
    #send_mandrill_email(subject, email_html, email_message, userObj.email, from_email, False)

    #mail.send_mail(sender_address, user_address, subject, body)

    logging.info( "sending message to %s, %s" % (userObj.email, email_message) )

    mail.send_mail('nonterritorial@gmail.com', userObj.email, subject, email_message)
    
    

# =============================================================================
# Location
# =============================================================================

# search indices
# 'ideaIndex', 'flightIndex'

def fetch_search_results(self, search_index_name, entityKind):
    nearby = self.request.get("nearby")
    radius = self.request.get("radius")

    next_curs = None

    if nearby:
        if not radius:
            radius = 100000# radius around search point in metres

        #    41.577047,3.773608
        lat = nearby.split(",")[0]#'41.577047'
        lng = nearby.split(",")[1]#'3.773608'

        import search
        search_result = search.locationSearch(lat, lng, radius, search_index_name)
        results = search_result["results"]
        search_entities = []
        for result in results:
            if result["id"]:
                ndbModel = getModel(entityKind)
                search_entities.append( ndbModel.get_by_id( int( result["id"] ) ) )

    else:
        curs = Cursor(urlsafe=self.request.get('cursor'))
        ndbModel = getModel(entityKind)
        search_entities, next_curs, more = ndbModel.query(ndbModel.public == True).order(-ndbModel.created).fetch_page(20, start_cursor=curs)
        if more and next_curs:
            next_curs = next_curs.urlsafe()
        else:
            next_curs = None

    return {
        "results": search_entities,
        "next_curs": next_curs
    }




    
    
    
    
    