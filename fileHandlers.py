

import time
import logging

from google.appengine.api import images
from google.appengine.ext import blobstore
from google.appengine.api import app_identity

import cloudstorage as gcs

from main import MainHandler
import model
import utils

def save_empty_file():
    empty_file = model.File()
    empty_file.put()
    return empty_file

def delete_from_gcs(gcs_filename, image):
    try:
        if gcs_filename:
            if image:
                images.delete_serving_url(blobstore.create_gs_key(gcs_filename))
            gcs.delete(gcs_filename[3:])
    except:
        logging.error("something went wrong deleting a file from cloud storage")

def upload_file(file_req):
    def save_gcs_to_file(gcs_filename, content_type, filename, image_url, height, width, download_url):
        if image_url:
            fileType = "image"
        else:
            fileType = "file"

        file_obj = model.File(
            fileType=fileType,
            gcs_filename=gcs_filename, 
            content_type=content_type, 
            filename=filename, 
            image_url=image_url,
            height=height,
            width=width,
            download_url=download_url
            )
        file_obj.put()
        return file_obj

    def get_image_details(file_data, gcs_filename):
        height = images.Image(image_data=file_data).height
        width = images.Image(image_data=file_data).width
        serving_url = images.get_serving_url(blobstore.create_gs_key(gcs_filename))
        return [ serving_url, height, width ]

    #file_req = self.request.POST["file"]
    filename = file_req.filename
    # import slugify
    # filename = slugify.slugify(filename)
    filename = filename.replace(" ", "-")
    # filename = filename.encode('ascii',errors='ignore')

    # the below converts filenames to pure ascii, and removes all invalid characters...sorry chinese people
    import re
    filename = re.sub(r'[^\x00-\x7f]',r'', filename)

    content_type = file_req.type
    file_data = file_req.value
    time_stamp = int(time.time())
    app_id = app_identity.get_application_id()
    fname = '/%s.appspot.com/file_%s%s' % (app_id, time_stamp, filename)
    # fname = '/%s/file_%s%s' % (app_id, time_stamp, filename)
    if content_type:
        gcs_file = gcs.open(fname, 'w', content_type=content_type)
        # gcs_file = cloudstorage.open(filename, 'w', content_type='text/plain', options={'x-goog-acl': 'private','x-goog-meta-foo': 'foo', 'x-goog-meta-bar': 'bar'})
    else:
        gcs_file = gcs.open(fname, 'w')# reverts to default "binary/octet-stream"

    gcs_file.write(file_data)

    gcs_file.close()

    gcs_filename = "/gs%s" % fname
    
    # some initial conditions
    image_url = None
    height = None
    width = None
    download_url = None

    if content_type == 'image/gif':
        imageDetails = get_image_details(file_data, gcs_filename)
        image_url = imageDetails[0]
        height = imageDetails[1]
        width = imageDetails[2]
    elif content_type == 'image/jpeg':
        imageDetails = get_image_details(file_data, gcs_filename)
        image_url = imageDetails[0]
        height = imageDetails[1]
        width = imageDetails[2]
    elif content_type == 'image/pjpeg':
        imageDetails = get_image_details(file_data, gcs_filename)
        image_url = imageDetails[0]
        height = imageDetails[1]
        width = imageDetails[2]
    elif content_type == 'image/png':
        imageDetails = get_image_details(file_data, gcs_filename)
        image_url = imageDetails[0]
        height = imageDetails[1]
        width = imageDetails[2]
    else:
        download_url = "http://storage.googleapis.com%s" % gcs_filename[3:]

    file_obj = save_gcs_to_file(gcs_filename, content_type, filename, image_url, height, width, download_url)

    return file_obj

class FileUpload(MainHandler):
    def post(self):
        file_req = self.request.POST["file"]
        entityKind = self.request.get("entityKind")
        entityID = self.request.get("entityID")

        userObj = self.user
        errors = []

        if userObj:

            if userObj.flightCode:

                file_obj = upload_file(file_req)

                entity = None

                if not entityID:
                    if file_obj:
                        entity = utils.getModel(entityKind)
                        entity = entity()
                        entity.artist = userObj.key
                        entity.files = [file_obj.key]

                        if file_obj.fileType == "image":
                            entity.images = [file_obj.image_url]

                        entity.put()
                        # inefficient 2 puts in order to get unique slug name if not already set.
                        temp_name = "draft-%s" % str( entity.key.id() )
                        entity.name = temp_name
                        entity.slug = temp_name
                        entity.put()
                    else:
                        errors.append("Something went wrong and we couldn't upload your file.")
                        logging.error("failed to upload file")
                else:
                    Model = utils.getModel(entityKind)
                    entity = Model.get_by_id(int(entityID))
                    entity.files.append(file_obj.key)

                    if file_obj.fileType == "image":
                        entity.images.append( file_obj.image_url )

                    entity.put()

                if entity:
                    entityID = entity.key.id()
                    fileType = file_obj.fileType
                    image_url = file_obj.image_url
                    download_url = file_obj.download_url
                    preview_html = utils.get_file_preview_html(file_obj)
                else:
                    entityID = None
                    fileType = None
                    image_url = None
                    download_url = None
                    preview_html = None

                msg = {
                    "message": "success",
                    "entityID": entityID,
                    "fileType": fileType,
                    "image_url": image_url,
                    "download_url": download_url,
                    "preview_html": preview_html,
                    "errors": errors
                }

                self.render_json(msg)
            else:
                errors.append("You need to set a flight code.")
                self.render_json({
                    "message": "fail",
                    "long_message": "You need to set a flight code.",
                    "errors": errors
                })
        else:
            errors.append("You're not logged in.")
            self.render_json({
                "message": "fail",
                "long_message": "You're not logged in.",
                "errors": errors
                })








