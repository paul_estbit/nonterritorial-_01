import jinja2
import os

template_dir = os.path.join(os.path.dirname(__file__), 'templates')
jinja_env = jinja2.Environment(loader = jinja2.FileSystemLoader(template_dir), autoescape = True)

def hasFlightCode(userObj):
    if userObj.flightCode:
        return ''
    else:
        return 'hidden'
jinja_env.filters['hasFlightCode'] = hasFlightCode

def blog_date(value):
    return value.strftime("%-d %b %y")
jinja_env.filters['blog_date'] = blog_date

def crop_tweet(tweet_string):
    # less 21 chars for url and less 6 for @emiru and less 2 for spaces
    crop_to = 140 - 29
    tweet = tweet_string[:crop_to]
    return tweet
jinja_env.filters['crop_tweet'] = crop_tweet

def tags(tags):
    # cool for/else statement
    tag_string = ""
    if tags:
        for tag in tags[:-1]:
            tag_string += tag + ", "
        else:
            tag_string += tags[-1]
    return tag_string
jinja_env.filters['tags'] = tags

def check_none(value):
    if value:
        return value
    else:
        return ""
jinja_env.filters['check_none'] = check_none