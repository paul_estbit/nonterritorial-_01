from main import MainHandler

from google.appengine.datastore.datastore_query import Cursor

import logging

from google.appengine.ext import ndb

import utils
import model

class Ideas(MainHandler):
    def get(self):
        curs = Cursor(urlsafe=self.request.get('cursor'))
        ideas, next_curs, more = model.Idea.query(model.Idea.public == True).order(-model.Idea.created).fetch_page(20, start_cursor=curs)
        if more and next_curs:
            next_curs = next_curs.urlsafe()
        else:
            next_curs = None

        self.render("users/ideas.html", ideas=ideas, next_curs=next_curs)

class Idea(MainHandler):
    def get(self, idea_slug):
        idea = model.Idea.query(model.Idea.slug == idea_slug).get()

        files = []
        no_media_files = True
        for file_key in idea.files:
            fileObj = file_key.get()
            files.append( fileObj )
            if fileObj.fileType != "image" and fileObj.fileType != "video":
                no_media_files = False

        self.render("idea/idea-page.html", idea=idea, files=files, no_media_files=no_media_files)

class AdminIdeas(MainHandler):
    def get(self):
        curs = utils.getCursor(self)
        ideas, next_curs, more = model.Idea.query().order(-model.Idea.created).fetch_page(20, start_cursor=curs)
        if more and next_curs:
            next_curs = next_curs.urlsafe()
        else:
            next_curs = None

        self.render("idea/admin-ideas.html", ideas=ideas, next_curs=next_curs)














