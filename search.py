from google.appengine.api import search
import logging

doc_limit = 25

# Queries

def textSearch(query, INDEX_NAME, offsetval=0):
    #query = "stories"

    results = {"results": []}
    results["returned_count"] = None
    results["number_found"] = None

    try:
        index = search.Index(INDEX_NAME)
        #search_results = index.search(query)

        search_query = search.Query(
            query_string=query.strip(),
            options=search.QueryOptions(
                limit=doc_limit, 
                offset=offsetval
                ))

        search_results = index.search(search_query)

        returned_count = len(search_results.results)
        number_found = search_results.number_found

        results["returned_count"] = returned_count
        results["number_found"] = number_found

        logging.info(".............................")
        logging.info("Returned count: %s, Number Found: %s" % (returned_count, number_found))
        for doc in search_results:
            resultObj = {}
            resultObj["title"] = doc.field('title').value
            resultObj["description"] = doc.field('description').value
            resultObj["id"] = doc.doc_id
            results["results"].append(resultObj)
            logging.info(doc.field('title').value)
            logging.info(doc.field('description').value)
    except search.Error:
        logging.error(search.Error)

    return results

def locationSearch(lat, lng, radius, INDEX_NAME, offsetval=0):
    query_string = "distance(location, geopoint(%s, %s)) < %s" % (lat, lng, radius)
    results = {"results": []}
    results["returned_count"] = None
    results["number_found"] = None

    try:
        index = search.Index(INDEX_NAME)
        #search_results = index.search(query)

        # search_query = search.Query(
        #     query_string=query_string,
        #     options=search.QueryOptions(
        #         limit=doc_limit, 
        #         offset=offsetval
        #         ))

        # search_results = index.search(search_query)

        logging.info("Query string . . . . . %s" % query_string)

        search_results = index.search(query_string)

        returned_count = len(search_results.results)
        number_found = search_results.number_found

        results["returned_count"] = returned_count
        results["number_found"] = number_found

        logging.info(".............................")
        logging.info("Returned count: %s, Number Found: %s" % (returned_count, number_found))
        for doc in search_results:
            resultObj = {}
            resultObj["name"] = doc.field('name').value
            resultObj["description"] = doc.field('description').value
            resultObj["location"] = doc.field('location').value
            resultObj["id"] = doc.doc_id
            results["results"].append(resultObj)
            logging.info(doc.field('name').value)
            logging.info(doc.field('description').value)
    except search.Error:
        logging.error(search.Error)

    return results



# Helpers

def stringToSearchGeoPt(latLng):
    latitude = float( latLng.split(",")[0] )
    longitude = float( latLng.split(",")[1] )
    if latitude and longitude:
        geopoint = search.GeoPoint( latitude, longitude )
    else:
        geopoint = None

    return geopoint

# Indexes

def removeFromIndex(doc_id, index_name):
    doc_index = search.Index(name=index_name)
    doc_index.delete([doc_id])


def indexSlide(slideObj):

    INDEX_NAME = 'slideShowIndex'

    index = search.Index(name=INDEX_NAME)

    slideshow = slideObj.slideShow.get()


    geopoint = stringToSearchGeoPt(slideObj.slideShow.get().location)
    fields = [
            search.TextField(name="title", value=slideObj.title),
            search.TextField(name="slideshow", value=str(slideObj.slideShow.id())),
            search.TextField(name="locality", value=slideshow.locality),
            search.TextField(name="country", value=slideshow.country),
            search.HtmlField(name="description", value=slideObj.description),
            search.GeoField(name="location", value=geopoint),
            search.DateField(name="created", value=slideshow.created),
        ]

    d = search.Document(doc_id=str( slideObj.key.id() ), fields=fields)

    try:
        add_result = search.Index(name=INDEX_NAME).put(d)
    except search.Error:
        logging.error(" . . . . . .  . something went wrong with the search")
        logging.error(search.Error)



def indexIdea(ideaObj):

    INDEX_NAME = 'ideaIndex'

    index = search.Index(name=INDEX_NAME)


    geopoint = stringToSearchGeoPt(ideaObj.location)
    fields = [
            search.TextField(name="name", value=ideaObj.name),
            search.HtmlField(name="description", value=ideaObj.description),
            search.GeoField(name="location", value=geopoint)  
        ]

    # fields = [
    #           search.TextField(name=docs.Product.PID, value=pid), # the product id
    #           # The 'updated' field is set to the current date.
    #           search.DateField(name=docs.Product.UPDATED,
    #                        value=datetime.datetime.now().date()),
    #           search.TextField(name=docs.Product.PRODUCT_NAME, value=name),
    #           search.TextField(name=docs.Product.DESCRIPTION, value=description),
    #           # The category names are atomic
    #           search.AtomField(name=docs.Product.CATEGORY, value=category),
    #           # The average rating starts at 0 for a new product.
    #           search.NumberField(name=docs.Product.AVG_RATING, value=0.0),
    #           search.NumberField(name=docs.Product.PRICE, value=price) 
    #       ]

    d = search.Document(doc_id=str( ideaObj.key.id() ), fields=fields)

    try:
        add_result = search.Index(name=INDEX_NAME).put(d)
        #doc_id = add_result[0].id
    except search.Error:
        logging.error(" . . . . . .  . something went wrong with the search")
        logging.error(search.Error)

def indexFlight(landingPermissionObj):

    INDEX_NAME = 'flightIndex'

    index = search.Index(name=INDEX_NAME)


    geopoint = stringToSearchGeoPt(landingPermissionObj.location)
    fields = [
            search.GeoField(name="location", value=geopoint)  
        ]

    d = search.Document(doc_id=str( landingPermissionObj.key.id() ), fields=fields)

    try:
        add_result = search.Index(name=INDEX_NAME).put(d)
    except search.Error:
        logging.error(" . . . . . .  . something went wrong with the search")
        logging.error(search.Error)









