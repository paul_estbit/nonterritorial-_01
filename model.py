from google.appengine.ext import ndb

import utils

# def users_key(group='default'):
#     return ndb.Key('users', group)
    
class User(ndb.Model):
    username = ndb.StringProperty()
    slug = ndb.StringProperty()
    name = ndb.StringProperty()
    surname = ndb.StringProperty()
    about = ndb.TextProperty()
    profile_image = ndb.StringProperty()
    file_obj = ndb.KeyProperty(kind="File")
    flightCode = ndb.StringProperty()
    ideaCount = ndb.IntegerProperty(default=0)
    location = ndb.StringProperty()
    geopoint = ndb.GeoPtProperty()
    address = ndb.StringProperty()
    country = ndb.StringProperty()
    locality = ndb.StringProperty()
    userType = ndb.StringProperty(default="casual")#casual, artist, curator, freeport, airport, gallery
    editedSpace = ndb.BooleanProperty(default=False)
    slideShow = ndb.KeyProperty(kind="SlideShow")
    isGlobalCurator = ndb.BooleanProperty(default=False)
    isGalleryArtist = ndb.BooleanProperty(default=False)# these are artists added by galleries or potentialy galleries added by artists
    isGallery = ndb.BooleanProperty(default=False)
    upgraded = ndb.BooleanProperty(default=False)
    email = ndb.StringProperty()
    pw_hash = ndb.StringProperty()
    newsletter = ndb.StringProperty(default="weekly-newsletter")
    # chosen_user_type = ndb.BooleanProperty(default=False)
    # applicationProgress = ndb.BooleanProperty(default=False)
    portfolio = ndb.StringProperty()
    approved = ndb.BooleanProperty(default=False)
    isSonic = ndb.BooleanProperty(default=False)
    created = ndb.DateTimeProperty(auto_now_add=True)
    
    @classmethod
    def by_id(cls, uid):
        return cls.get_by_id(uid)
        # return cls.get_by_id(uid, parent = users_key())
    
    @classmethod
    def login(cls, emailOrUsername, pw):
        u = cls.by_email(emailOrUsername)
        if u:
            username = u.username
        else:
            u = cls.by_username(emailOrUsername)
            if u:
                username = u.username
        if u and utils.valid_pw(username, pw, u.pw_hash):
            return u
            
    @classmethod
    def by_email(cls, email):
        u = User.query(User.email == email).get()
        return u
    @classmethod
    def by_username(cls, username):
        u = User.query(User.username == username).get()
        return u
        
class Counter(ndb.Model):
    allUsers = ndb.IntegerProperty(default=0)
    applications = ndb.IntegerProperty(default=0)
    artists = ndb.IntegerProperty(default=0)
    curators = ndb.IntegerProperty(default=0)
    globalCurators = ndb.IntegerProperty(default=0)
    casualUsers = ndb.IntegerProperty(default=0)
    ideas = ndb.IntegerProperty(default=0)
    flights = ndb.IntegerProperty(default=0)
    landingRequests = ndb.IntegerProperty(default=0)
    inFlight = ndb.IntegerProperty(default=0)
    landingPermissions = ndb.IntegerProperty(default=0)
    landingPermissionsAccepted = ndb.IntegerProperty(default=0)
       
class Application(ndb.Model):
    application = ndb.StringProperty()
    accept = ndb.BooleanProperty(default=False)
    reject = ndb.BooleanProperty(default=False)
    username = ndb.StringProperty()
    email = ndb.StringProperty()
    userType = ndb.StringProperty()
    user = ndb.KeyProperty(kind="User")
    created = ndb.DateTimeProperty(auto_now_add=True)

class SlideShow(ndb.Model):
    title = ndb.StringProperty(default="Title")
    slug = ndb.StringProperty()
    user = ndb.KeyProperty(kind='User')
    name = ndb.StringProperty()
    surname = ndb.StringProperty()
    flightCode = ndb.StringProperty()
    flightNumber = ndb.StringProperty()
    userType = ndb.StringProperty()
    description = ndb.TextProperty(default="A description")
    location = ndb.StringProperty()
    geopoint = ndb.GeoPtProperty()
    address = ndb.StringProperty()
    country = ndb.StringProperty()
    locality = ndb.StringProperty()
    audio = ndb.StringProperty()
    audio_name = ndb.StringProperty()
    inFlight = ndb.BooleanProperty(default=False)
    isGallerySlideShow = ndb.BooleanProperty(default=False)
    booking_link = ndb.StringProperty()
    public = ndb.BooleanProperty(default=True)
    approved = ndb.BooleanProperty(default=False)
    created = ndb.DateTimeProperty(auto_now_add=True)

class Slide(ndb.Model):
    user = ndb.KeyProperty(kind='User')
    slideShow = ndb.KeyProperty(kind='SlideShow')
    slideType = ndb.StringProperty()
    title = ndb.StringProperty()
    description = ndb.TextProperty()
    file_obj = ndb.KeyProperty(kind='File')
    image_url = ndb.StringProperty()
    video_url = ndb.StringProperty()
    video_url_mp4 = ndb.StringProperty()
    video_url_webm = ndb.StringProperty()
    video_url_ogv = ndb.StringProperty()
    video_embed = ndb.TextProperty()
    slideOrder = ndb.IntegerProperty()
    created = ndb.DateTimeProperty(auto_now_add=True)

class UserSlideShow(ndb.Model):
    user = ndb.KeyProperty(kind="User")
    userType = ndb.StringProperty()
    userFlightCode = ndb.StringProperty()
    slideShow = ndb.KeyProperty(kind="SlideShow")
    title = ndb.StringProperty()
    surname = ndb.StringProperty()
    flightCode = ndb.StringProperty()
    slug = ndb.StringProperty()
    flightNumber = ndb.StringProperty()
    takeOffDate = ndb.DateTimeProperty()
    landingDate = ndb.DateTimeProperty()
    departureDate = ndb.DateTimeProperty()
    takeOffLocation = ndb.StringProperty()# just including this for convenience
    takeOffCountry = ndb.StringProperty()
    takeOffLocality = ndb.StringProperty()
    landingCountry = ndb.StringProperty()
    landingLocality = ndb.StringProperty()
    location = ndb.StringProperty()
    geopoint = ndb.GeoPtProperty()
    accepted = ndb.BooleanProperty(default=None)
    public = ndb.BooleanProperty(default=False)
    approved = ndb.BooleanProperty(default=False)
    created = ndb.DateTimeProperty(auto_now_add=True)

class SavedSlideShow(ndb.Model):
    user = ndb.KeyProperty(kind="User")
    slideShow = ndb.KeyProperty(kind="SlideShow")
    created = ndb.DateTimeProperty(auto_now_add=True)

class Inquiry(ndb.Model):
    name = ndb.StringProperty()
    email = ndb.StringProperty()
    message = ndb.TextProperty()
    slideShowID = ndb.StringProperty()
    slideshow_url = ndb.StringProperty()
    created = ndb.DateTimeProperty(auto_now_add=True)

# maps galleries to their exhibitions
class GallerySlideShow(ndb.Model):
    gallery = ndb.KeyProperty(kind="User")
    userType = ndb.StringProperty()
    userFlightCode = ndb.StringProperty()
    slideShow = ndb.KeyProperty(kind="SlideShow")
    title = ndb.StringProperty()
    surname = ndb.StringProperty()
    flightCode = ndb.StringProperty()
    slug = ndb.StringProperty()
    flightNumber = ndb.StringProperty()
    takeOffDate = ndb.DateTimeProperty()
    landingDate = ndb.DateTimeProperty()
    departureDate = ndb.DateTimeProperty()
    takeOffLocation = ndb.StringProperty()# just including this for convenience
    takeOffCountry = ndb.StringProperty()
    takeOffLocality = ndb.StringProperty()
    landingCountry = ndb.StringProperty()
    landingLocality = ndb.StringProperty()
    location = ndb.StringProperty()
    geopoint = ndb.GeoPtProperty()
    accepted = ndb.BooleanProperty(default=None)
    public = ndb.BooleanProperty(default=False)
    approved = ndb.BooleanProperty(default=False)
    created = ndb.DateTimeProperty(auto_now_add=True)

#  this is used to get artists for a gallery or to check if a gallery owns a slideshow
class ExhibitionArtist(ndb.Model):
    gallery = ndb.KeyProperty(kind="User")
    exhibition = ndb.KeyProperty(kind="SlideShow")
    artist = ndb.KeyProperty(kind="User")
    created = ndb.DateTimeProperty(auto_now_add=True)

class StaticPage(ndb.Model):
    title = ndb.StringProperty()
    subtitle = ndb.StringProperty()
    text = ndb.TextProperty()
    page_name =  ndb.StringProperty()
    order = ndb.IntegerProperty()
    created = ndb.DateTimeProperty(auto_now_add=True)


class Idea(ndb.Model):
    slug = ndb.StringProperty()
    artist = ndb.KeyProperty(kind="User")
    number = ndb.StringProperty()
    name = ndb.StringProperty()
    description = ndb.TextProperty()
    notes = ndb.TextProperty()
    status = ndb.StringProperty()
    originName = ndb.StringProperty()
    location = ndb.StringProperty()
    geopoint = ndb.GeoPtProperty()
    address = ndb.StringProperty()
    country = ndb.StringProperty()
    locality = ndb.StringProperty()
    origin = ndb.KeyProperty(kind="Origin")
    landingRequests = ndb.IntegerProperty(default=0)
    cover_image = ndb.StringProperty()#one main cover
    covers = ndb.StringProperty(repeated=True)#potentially one cover to match each video in video array
    videos = ndb.StringProperty(repeated=True)
    images = ndb.StringProperty(repeated=True)
    files = ndb.KeyProperty(kind="File", repeated=True)
    public = ndb.BooleanProperty(default=False)
    tags = ndb.StringProperty(repeated=True)
    created = ndb.DateTimeProperty(auto_now_add=True)

class Origin(ndb.Model):
    name = ndb.StringProperty()
    owner = ndb.KeyProperty(kind="User")
    location = ndb.StringProperty()
    geopoint = ndb.GeoPtProperty()

class Country(ndb.Model):
    country = ndb.StringProperty()
    location = ndb.StringProperty()
    geopoint = ndb.GeoPtProperty()
    created = ndb.DateTimeProperty(auto_now_add=True)

class Locality(ndb.Model):
    locality = ndb.StringProperty()
    location = ndb.StringProperty()
    geopoint = ndb.GeoPtProperty()
    created = ndb.DateTimeProperty(auto_now_add=True)

class TagIdea(ndb.Model):
    User = ndb.KeyProperty(kind="User")
    Tag =ndb.KeyProperty(kind="Tag")
    Idea = ndb.KeyProperty(kind="Idea")

class LandingPermission(ndb.Model):
    curator = ndb.KeyProperty(kind="User")
    artist = ndb.KeyProperty(kind="User")
    idea = ndb.KeyProperty(kind="Idea")
    takeOffDate = ndb.DateTimeProperty()
    landingDate = ndb.DateTimeProperty()
    m = ndb.FloatProperty()# y = mx + c
    c = ndb.FloatProperty()# y = mx + c
    duration = ndb.IntegerProperty()# time in seconds between takeoff and landing
    takeOffLocation = ndb.StringProperty()# just including this for convenience
    location = ndb.StringProperty()
    geopoint = ndb.GeoPtProperty()
    accepted = ndb.BooleanProperty(default=None)
    public = ndb.BooleanProperty(default=False)
    #? not sure if we need a landing location? or is it that the landing location is the location of the local curator
    created = ndb.DateTimeProperty(auto_now_add=True)

class ExhibitionSpace(ndb.Model):
    user = ndb.KeyProperty()
    name = ndb.StringProperty()
    description = ndb.TextProperty()
    location = ndb.StringProperty()
    geopoint = ndb.GeoPtProperty()
    awaitingFlight = ndb.BooleanProperty(default=False)
    created = ndb.DateTimeProperty(auto_now_add=True)

class File(ndb.Model):
    user = ndb.KeyProperty(kind='User')
    file_reference = ndb.StringProperty()
    fileType = ndb.StringProperty()
    isAudio = ndb.BooleanProperty()
    isVideo = ndb.BooleanProperty()
    image_url = ndb.StringProperty()
    download_url = ndb.StringProperty()
    gcs_filename = ndb.StringProperty()
    content_type = ndb.StringProperty()
    filename = ndb.StringProperty()
    height = ndb.IntegerProperty()
    width = ndb.IntegerProperty()
    created = ndb.DateTimeProperty(auto_now_add=True)

class Tag(ndb.Model):
    tag = ndb.StringProperty()
    

    