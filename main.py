#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import webapp2
import logging
import os
from string import letters
import json
from datetime import datetime, timedelta
import datetime
import time
import urllib
import base64

from google.appengine.ext import ndb
from google.appengine.api import images
from google.appengine.api import memcache
from google.appengine.ext import blobstore
from google.appengine.ext.webapp import blobstore_handlers
from google.appengine.api import mail
from google.appengine.datastore.datastore_query import Cursor

from google.appengine.api import users
from google.appengine.api import app_identity

import model
import utils

import cloudstorage as gcs

cookie_name = "net.nonterritorial.www"


import jinja2
template_dir = os.path.join(os.path.dirname(__file__), 'templates')
jinja_env = jinja2.Environment(loader = jinja2.FileSystemLoader(template_dir), autoescape = True)

def lat_from_loc(loc):
    return loc.split(",")[0]
jinja_env.filters['lat_from_loc'] = lat_from_loc

def lng_from_loc(loc):
    return loc.split(",")[1]
jinja_env.filters['lng_from_loc'] = lng_from_loc

def abbrev_country(country):
    if country == "United States":
        return "USA"
    elif country == "United Kingdom":
        return "UK"
    elif country == "United Arab Emirates":
        return "UAE"
    else:
        return country

    return html
jinja_env.filters['abbrev_country'] = abbrev_country

def renderFileItem(fileKey):
    fileEntity = fileKey.get()
    html =""
    if fileEntity.fileType == "image":
        html = "<img src='%s'>" % fileEntity.image_url
    else:
        html = "<a href='%s'>Download</a>" % fileEntity.download_url

    return html

jinja_env.filters['renderFileItem'] = renderFileItem

def locationLatLng(latLng):
    if latLng:
        lat = latLng.split(",")[0]
        lng = latLng.split(",")[1]

        dict_latLng = {
            'lat': float(lat),
            'lng': float(lng)
        }
        json_latLng = json.dumps( dict_latLng )
        return json_latLng
    else:
        return ''
jinja_env.filters['locationLatLng'] = locationLatLng

def hideIfHasProperty(obj, property):
    if getattr(obj, property):
        return ''
    else:
        return 'hidden'
jinja_env.filters['hideIfHasProperty'] = hideIfHasProperty

def hideIfHasNotProperty(obj, property):
    if getattr(obj, property):
        return 'hidden'
    else:
        return ''
jinja_env.filters['hideIfHasNotProperty'] = hideIfHasNotProperty

def hasFlightCode(userObj):
    if userObj.flightCode:
        return ''
    else:
        return 'hidden'
jinja_env.filters['hasFlightCode'] = hasFlightCode

def blog_date(value):
    if value:
        return value.strftime("%-d %b %y")
jinja_env.filters['blog_date'] = blog_date

def dash_date(value):
    if value:
        return value.strftime("%d/%m/%y")
jinja_env.filters['dash_date'] = dash_date

def picker_date(value):
    if value:
        return value.strftime("%Y/%m/%d")
jinja_env.filters['picker_date'] = picker_date

def tags(tags):
    # cool for/else statement
    tag_string = ""
    if tags:
        for tag in tags[:-1]:
            tag_string += tag + ", "
        else:
            tag_string += tags[-1]
    return tag_string
jinja_env.filters['tags'] = tags

def check_none(value):
    if value:
        return value
    else:
        return ""
jinja_env.filters['check_none'] = check_none

class MainHandler(webapp2.RequestHandler):

#TEMPLATE FUNCTIONS
    def write(self, *a, **kw):
        self.response.headers['Host'] = 'localhost'
        self.response.headers['Content-Type'] = 'text/html'
        self.response.out.write(*a, **kw)

    def render_str(self, template, **params):
        params['user'] = self.user
        #params['buyer'] = self.buyer
        t = jinja_env.get_template(template)
        return t.render(params)

    def render(self, template, **kw):
        userObj = self.user
        self.write(self.render_str(template, userObj=userObj, **kw))
    def render_auth(self, template, **kw):
        userObj = self.user
        if not userObj:
            self.redirect("/login")
        else:
            self.write(self.render_str(template, userObj=userObj, **kw))

    #JSON rendering
    def render_json(self, obj):
        self.response.headers['Content-Type'] = 'application/json'
        self.response.headers['Host'] = 'localhost'
        self.response.headers['Access-Control-Allow-Origin'] = '*'
        self.response.out.write(json.dumps(obj))

    #COOKIE FUNCTIONS
    # sets a cookie in the header with name, val , Set-Cookie and the Path---not blog
    def set_secure_cookie(self, name, val):
        cookie_val = utils.make_secure_val(val)
        self.response.headers.add_header('Set-Cookie', '%s=%s; Path=/' % (name, cookie_val))# consider imcluding an expire time in cookie(now it closes with browser), see docs
    # reads the cookie from the request and then checks to see if its true/secure(fits our hmac)
    def read_secure_cookie(self, name):
        cookie_val = self.request.cookies.get(name)
        if cookie_val:
            cookie_val = urllib.unquote(cookie_val)
        return cookie_val and utils.check_secure_val(cookie_val)

    def login(self, user):
        self.set_secure_cookie(cookie_name, str(user.key.id()))

    def logout(self):
        self.response.headers.add_header('Set-Cookie', '%s=; Path=/' % cookie_name)

    def initialize(self, *a, **kw):
        webapp2.RequestHandler.initialize(self, *a, **kw)
        uid = self.read_secure_cookie(cookie_name)

        self.user = uid and model.User.by_id(int(uid))

import userHandlers
import fileHandlers
import ideaHandlers

def get_general_content():
    in_flight = model.UserSlideShow.query().order(-model.UserSlideShow.created).fetch(4)
    waiting_to_take_off = model.SlideShow.query(model.SlideShow.userType == 'artist').order(-model.SlideShow.created).fetch(4)

def get_front_page_flight_list():
    in_flight = model.UserSlideShow.query(model.UserSlideShow.approved == True).order(-model.UserSlideShow.created).fetch(6)
    in_flight_ids = []
    for flight in in_flight:
        in_flight_ids.append(int(flight.slideShow.id()))

    # logging.error("------------------------")
    # logging.error(in_flight_ids)

    waiting_to_take_off = model.SlideShow.query(model.SlideShow.userType == 'artist', model.SlideShow.approved == True ).order(-model.SlideShow.created).fetch(60)
    waiting_to_take_off_dd = []
    for wtto in waiting_to_take_off:
        # logging.error(wtto.key.id())
        if int(wtto.key.id()) not in in_flight_ids:
            waiting_to_take_off_dd.append(wtto)

    waiting_to_take_off_dd = waiting_to_take_off_dd[:6]

    return { "waiting_to_take_off_dd": waiting_to_take_off_dd, "in_flight":in_flight }

def get_gallery_flight_list(gallery):
    in_flight = model.UserSlideShow.query(model.UserSlideShow.user == gallery.key).order(-model.UserSlideShow.created).fetch(15)
    in_flight_ids = []
    for flight in in_flight:
        in_flight_ids.append(int(flight.slideShow.id()))

    return { "in_flight":in_flight }


class Home(MainHandler):
    def get(self):
        year = datetime.datetime.now().year
        counter = utils.get_counter()

        no_register = self.request.get("no_register")

        # in_flight = model.UserSlideShow.query().order(-model.UserSlideShow.created).fetch(6)
        # in_flight_ids = []
        # for flight in in_flight:
        #     in_flight_ids.append(flight.slideShow.id())

        # logging.error("------------------------")
        # logging.error(in_flight_ids)

        # waiting_to_take_off = model.SlideShow.query(model.SlideShow.userType == 'artist' ).order(-model.SlideShow.created).fetch(60)
        # waiting_to_take_off_dd = []
        # for wtto in waiting_to_take_off:
        #     logging.error(wtto.key.id())
        #     if wtto.key.id() not in in_flight_ids:
        #         waiting_to_take_off_dd.append(wtto)

        # waiting_to_take_off_dd = waiting_to_take_off_dd[:6]


        # self.render('index-prototype.html', year=year, counter=counter)
        self.render('slideshow/fp-map.html', year=year, counter=counter, in_flight=get_front_page_flight_list()['in_flight'], waiting_to_take_off=get_front_page_flight_list()['waiting_to_take_off_dd'], no_register=no_register)

class UserPage(MainHandler):
    def get(self, flightCode):
        userPage = model.User.query(model.User.flightCode == flightCode).get()
        if userPage:
            self.render("slideshow/user-page.html", userPage=userPage)

class Demo(MainHandler):
    def get(self):
        self.render("slideshow/demo.html")

class Galleries(MainHandler):
    def get(self):

        import urllib2

        country = urllib2.unquote( self.request.get("country") )
        locality = urllib2.unquote( self.request.get("locality") )
        order = self.request.get("order")

        curs = Cursor(urlsafe=self.request.get('slideshow_cursor'))
        if country:
            # ideas, next_curs, more = model.SlideShow.query(model.SlideShow.userType == "artist", model.SlideShow.approved == True, model.SlideShow.country == country).order(-model.SlideShow.created).fetch_page(20, start_cursor=curs)
            galleries, next_curs, more = model.User.query(model.User.userType == "gallery", model.User.approved == True, model.User.isGallery == True , model.User.country == country).order(-model.User.created).fetch_page(20, start_cursor=curs)
        elif locality:
            # ideas, next_curs, more = model.SlideShow.query(model.SlideShow.userType == "artist", model.SlideShow.approved == True, model.SlideShow.locality == locality).order(-model.SlideShow.created).fetch_page(20, start_cursor=curs)
            galleries, next_curs, more = model.User.query(model.User.userType == "gallery", model.User.approved == True, model.User.isGallery == True , model.User.locality == locality).order(-model.User.created).fetch_page(20, start_cursor=curs)
        else:
            galleries, next_curs, more = model.User.query(model.User.userType == "gallery", model.User.approved == True, model.User.isGallery == True).order(-model.User.created).fetch_page(20, start_cursor=curs)
        if more and next_curs:
            next_curs = next_curs.urlsafe()
        else:
            next_curs = None

        countries = model.Country.query().order(model.Country.country).fetch()
        localities = model.Locality.query().order(model.Locality.locality).fetch()

        # curs = Cursor(urlsafe=self.request.get('slideshow_cursor'))
        # galleries, next_curs, more = model.User.query(model.User.userType == "gallery", model.User.approved == True).order(-model.User.created).fetch_page(20, start_cursor=curs)
        # if more and next_curs:
        #     next_curs = next_curs.urlsafe()
        # else:
        #     next_curs = None

        gallery_exhibiting = []
        now = datetime.datetime.now()
        for gallery in galleries:
            is_exhibiting = model.UserSlideShow.query(model.UserSlideShow.user == gallery.key, model.UserSlideShow.departureDate > now).get()
            if is_exhibiting:
                gallery_exhibiting.append(True)
            else:
                gallery_exhibiting.append(False)

        self.render("slideshow/galleries.html", galleries=galleries, next_curs=next_curs, gallery_exhibiting=gallery_exhibiting, countries=countries, localities=localities)

class Artists(MainHandler):
    def get(self):

        import urllib2

        country = urllib2.unquote( self.request.get("country") )
        locality = urllib2.unquote( self.request.get("locality") )
        order = self.request.get("order")

        curs = Cursor(urlsafe=self.request.get('slideshow_cursor'))
        if country:
            # ideas, next_curs, more = model.SlideShow.query(model.SlideShow.userType == "artist", model.SlideShow.approved == True, model.SlideShow.country == country).order(-model.SlideShow.created).fetch_page(20, start_cursor=curs)
            galleries, next_curs, more = model.User.query(model.User.userType == "gallery", model.User.approved == True, model.User.isGallery == False , model.User.country == country).order(-model.User.created).fetch_page(20, start_cursor=curs)
        elif locality:
            # ideas, next_curs, more = model.SlideShow.query(model.SlideShow.userType == "artist", model.SlideShow.approved == True, model.SlideShow.locality == locality).order(-model.SlideShow.created).fetch_page(20, start_cursor=curs)
            galleries, next_curs, more = model.User.query(model.User.userType == "gallery", model.User.approved == True, model.User.isGallery == False , model.User.locality == locality).order(-model.User.created).fetch_page(20, start_cursor=curs)
        else:
            galleries, next_curs, more = model.User.query(model.User.userType == "gallery", model.User.approved == True, model.User.isGallery == False).order(-model.User.created).fetch_page(20, start_cursor=curs)
        if more and next_curs:
            next_curs = next_curs.urlsafe()
        else:
            next_curs = None

        countries = model.Country.query().order(model.Country.country).fetch()
        localities = model.Locality.query().order(model.Locality.locality).fetch()

        gallery_exhibiting = []
        now = datetime.datetime.now()
        for gallery in galleries:
            is_exhibiting = model.UserSlideShow.query(model.UserSlideShow.user == gallery.key, model.UserSlideShow.departureDate > now).get()
            if is_exhibiting:
                gallery_exhibiting.append(True)
            else:
                gallery_exhibiting.append(False)

        self.render("slideshow/galleries.html", galleries=galleries, next_curs=next_curs, artist_listing=True, gallery_exhibiting=gallery_exhibiting, countries=countries, localities=localities)



class GalleryPage(MainHandler):
    def get(self, gallery_slug):
        gallery = model.User.query(model.User.slug == gallery_slug).get()
        exhibitions = model.UserSlideShow.query(model.UserSlideShow.user == gallery.key).order(-model.UserSlideShow.created).fetch()

        self.render("slideshow/gallery-list.html", gallery=gallery, exhibitions=exhibitions)

class GalleryPageMap(MainHandler):
    def get(self, gallery_slug):
        gallery = model.User.query(model.User.slug == gallery_slug).get()
        if not gallery:
            gallery = model.User.query(model.User.flightCode == gallery_slug).get()
        exhibitions = model.UserSlideShow.query(model.UserSlideShow.user == gallery.key).order(-model.UserSlideShow.created).fetch()

        self.render("slideshow/gallery-map.html", gallery=gallery, exhibitions=exhibitions, in_flight=get_gallery_flight_list(gallery)['in_flight'])

class GalleryPageList(MainHandler):
    def get(self, gallery_slug):
        gallery = model.User.query(model.User.slug == gallery_slug).get()
        if not gallery:
            gallery = model.User.query(model.User.flightCode == gallery_slug).get()
        # exhibitions = model.UserSlideShow.query(model.UserSlideShow.user == gallery.key).order(-model.UserSlideShow.landingDate).fetch()

        list_title = "In Flight"
        user_slideshows = model.UserSlideShow.query(model.UserSlideShow.user == gallery.key).order(-model.UserSlideShow.landingDate).fetch(30)
        ideas = []

        for slideshow in user_slideshows:
            slideShowObj = slideshow.slideShow.get().to_dict()
            slideShowUser = slideshow.user.get()
            slideShowObj["destinationCountry"] = slideShowUser.country
            slideShowObj["destinationLocality"] = slideShowUser.locality
            slideShowObj["landingDate"] = slideshow.landingDate
            ideas.append(slideShowObj)

        self.render("slideshow/gallery-detail-list.html", gallery=gallery, ideas=ideas, list_title=list_title)

class GalleryContact(MainHandler):
    def get(self, gallery_flightCode):
        gallery = model.User.query(model.User.flightCode == gallery_flightCode).get()
        self.render("slideshow/gallery-contact.html", gallery=gallery)

    def post(self, gallery_flightCode):
        name = self.request.get("name")
        email = self.request.get("email")
        message = self.request.get("message")
        gallery_email = self.request.get("galleryEmail")

        try:
            utils.gallery_contact_email(gallery_email, email, message)
        except:
            logging.error("couldn't send gallery inquiry email")

        self.render_json({
            "message": "success"
            })

class APIIdeas(MainHandler):
    def get(self):
        search_results = utils.fetch_search_results(self, 'ideaIndex', 'Idea')

        ideas = search_results["results"]
        next_curs = search_results["next_curs"]

        counter = utils.get_counter()

        ideas_obj = {}
        ideas_obj["next_curs"] = next_curs
        ideas_obj["results"] = []
        ideas_obj["ideas"] = counter.ideas
        for idea in ideas:
            idea_obj = {}
            lat = None
            lng = None
            if idea.location:
                lat = float( idea.location.split(",")[0].strip() )
                lng = float( idea.location.split(",")[1].strip() )
            idea_obj["name"] = idea.name
            idea_obj["description"] = idea.description
            idea_obj["slug"] = idea.slug
            idea_obj["lat"] = lat
            idea_obj["lng"] = lng
            ideas_obj["results"].append(idea_obj)

        self.render_json(ideas_obj)

class APIFlights(MainHandler):
    def get(self):
        search_results = utils.fetch_search_results(self, 'flightIndex', 'LandingPermission')

        flights = search_results["results"]
        next_curs = search_results["next_curs"]

        counter = utils.get_counter()

        flights_obj = {}
        flights_obj["next_curs"] = next_curs
        flights_obj["results"] = []
        flights_obj["landingPermissionsAccepted"] = counter.landingPermissionsAccepted
        for flight in flights:
            if flight.accepted and flight.takeOffDate and flight.landingDate and flight.takeOffLocation and flight.location:
                flight_obj = {}
                lat = None
                lng = None
                if flight.location:
                    latTO = float( flight.takeOffLocation.split(",")[0].strip() )
                    lngTO = float( flight.takeOffLocation.split(",")[1].strip() )
                    latLD = float( flight.location.split(",")[0].strip() )
                    lngLD = float( flight.location.split(",")[1].strip() )
                flight_obj["latTO"] = latTO
                flight_obj["lngTO"] = lngTO
                flight_obj["latLD"] = latLD
                flight_obj["lngLD"] = lngLD
                flight_obj["takeOffDate"] = flight.takeOffDate.strftime('%B %d, %Y')
                flight_obj["landingDate"] = flight.landingDate.strftime('%B %d, %Y')

                idea = flight.idea.get()
                flight_obj["name"] = idea.name
                flight_obj["number"] = idea.number
                flight_obj["description"] = idea.description
                flight_obj["slug"] = idea.slug

                flights_obj["results"].append(flight_obj)

        self.render_json(flights_obj)

class APIPublicIdeas(MainHandler):
    def get(self):
        search_results_landing_permissions = utils.fetch_search_results(self, 'flightIndex', 'LandingPermission')
        search_results_ideas = utils.fetch_search_results(self, 'ideaIndex', 'Idea')

        flights = search_results_landing_permissions["results"]
        ideas = search_results_ideas["results"]

        # remove all ideas that are accounted for by a landing permission
        ideas_in_flight_permission = []
        for flight in flights:
            ideas_in_flight_permission.append( flight.idea )

        #ignore cursors for now, but if we expect to display more than a few hundred ideas will need markerclusterer anyway
        #next_curs = search_results["next_curs"]

        counter = utils.get_counter()

        flights_obj = {}
        #flights_obj["next_curs"] = next_curs
        flights_obj["results"] = []
        flights_obj["landingPermissionsAccepted"] = counter.landingPermissionsAccepted
        flights_obj["ideas"] = counter.ideas

        for flight in flights:
            if flight.accepted and flight.takeOffDate and flight.landingDate and flight.takeOffLocation and flight.location:
                flight_obj = {}
                lat = None
                lng = None
                if flight.location:
                    latTO = float( flight.takeOffLocation.split(",")[0].strip() )
                    lngTO = float( flight.takeOffLocation.split(",")[1].strip() )
                    latLD = float( flight.location.split(",")[0].strip() )
                    lngLD = float( flight.location.split(",")[1].strip() )
                flight_obj["latTO"] = latTO
                flight_obj["lngTO"] = lngTO
                flight_obj["latLD"] = latLD
                flight_obj["lngLD"] = lngLD
                flight_obj["takeOffDate"] = flight.takeOffDate.strftime('%B %d, %Y')
                flight_obj["landingDate"] = flight.landingDate.strftime('%B %d, %Y')

                idea = flight.idea.get()
                flight_obj["name"] = idea.name
                flight_obj["number"] = idea.number
                flight_obj["description"] = idea.description
                flight_obj["slug"] = idea.slug

                flights_obj["results"].append(flight_obj)

        for idea in ideas:
            if idea.public and idea.location and idea.key not in ideas_in_flight_permission:
                flight_obj = {}
                lat = None
                lng = None
                if idea.location:
                    latTO = float( idea.location.split(",")[0].strip() )
                    lngTO = float( idea.location.split(",")[1].strip() )
                flight_obj["latTO"] = latTO
                flight_obj["lngTO"] = lngTO
                flight_obj["latLD"] = None
                flight_obj["lngLD"] = None
                flight_obj["takeOffDate"] = None
                flight_obj["landingDate"] = None

                flight_obj["name"] = idea.name
                flight_obj["number"] = idea.number
                flight_obj["description"] = idea.description
                flight_obj["slug"] = idea.slug

                flights_obj["results"].append(flight_obj)

        self.render_json(flights_obj)


class Map(MainHandler):
    def get(self):
        self.render("map.html")


# ================================================================================
# Testing
# ================================================================================
# def gcs_upload(acl='public-read-write'):
#     """ jinja2 upload form context """

#     default_bucket = app_identity.get_default_gcs_bucket_name()
#     google_access_id = app_identity.get_service_account_name()
#     #succes_redirect = webapp2.uri_for('uploaded', _full=True)
#     succes_redirect = 'http://non-territorial.appspot.com/uploaded'
#     expiration_dt = datetime.datetime.now() + timedelta(seconds=3000)

#     policy_string = """
#     {"expiration": "%s",
#               "conditions": [
#                   ["starts-with", "$key", ""],
#                   {"acl": "%s"},
#                   {"bucket": "%s"},
#                   {"success_action_redirect": "%s"},
#                   {"success_action_status": "201"},
#               ]}""" % (expiration_dt.replace(microsecond=0).isoformat() + 'Z', acl, default_bucket, succes_redirect)

#     import base64
#     policy = base64.b64encode(policy_string)
#     _, signature_bytes = app_identity.sign_blob(policy)
#     signature = base64.b64encode(signature_bytes)

#     return dict(form_bucket=default_bucket, form_access_id=google_access_id, form_policy=policy, form_signature=signature,
#             form_succes_redirect=succes_redirect, form_acl=acl)


# class Test(MainHandler):
#     def get(self):
#         self.render('test.html', **gcs_upload())


# class Uploaded(MainHandler):
#     def get(self):
#         logging.error("uploaded - get")
#     def post(self):
#         logging.error("uploaded - post")

# class GetUploadUrl(MainHandler):
#     def get(self):
#         # do some user validation here, make sure its a registered user adn log it so that we can track any fraudulent posts...

#         default_bucket = app_identity.get_default_gcs_bucket_name()
#         upload_url = blobstore.create_upload_url('/gcsCallback', gs_bucket_name=default_bucket)
#         self.render_json({
#             "message": "success",
#             "url": upload_url
#             })

# class GCSCallback(MainHandler):
#     def get(self):
#         logging.error("................................................................")
#         logging.error("gcs_callback - get")
#     def post(self):
#         logging.error("................................................................")
#         logging.error("gcs_callback - post")
#         logging.error( dir(self.request.POST) )





















# ---------------------------------------------------------------------
# File Upload
# ---------------------------------------------------------------------

def gcs_upload(file_reference, acl='public-read'):
    """ return GCS upload form context
        more info : https://cloud.google.com/storage/docs/xml-api/post-object
    """

    app_id = app_identity.get_application_id()

    # appengine default bucket has free quota
    default_bucket = app_identity.get_default_gcs_bucket_name()
    # these folders can be used in the upload
    bucket_folders = ['test', 'uploads']

    #user_id = users.get_current_user().email().lower()
    google_access_id = app_identity.get_service_account_name()
    # success_redirect = webapp2.uri_for('gcs_upload_ok', _full=True)
    success_redirect = "http://%s.appspot.com/fileuploaded" % app_id
    # GCS signed upload url expires
    expiration_dt = datetime.datetime.now() + timedelta(seconds=12000)

    # The security json policy document that describes what can and cannot be uploaded in the form
    policy_string = """
    {"expiration": "%s",
              "conditions": [
                  ["starts-with", "$key", ""],
                  {"acl": "%s"},
                  {"success_action_redirect": "%s"},
                  {"success_action_status": "201"}
              ]}""" % (expiration_dt.replace(microsecond=0).isoformat() + 'Z', acl, success_redirect)

    # sign the policy document
    policy = base64.b64encode(policy_string)
    _, signature_bytes = app_identity.sign_blob(policy)
    signature = base64.b64encode(signature_bytes)

    logging.debug('GCS upload policy : ' + policy_string)
    return dict(
        form_bucket=default_bucket,
        form_access_id=google_access_id,
        form_policy=policy,
        form_signature=signature,
        form_succes_redirect=success_redirect,
        form_folders=bucket_folders,
        file_reference=file_reference)

class FileUploaded(MainHandler):
    def get(self):
        logging.error("returned to redirect successfully... yoyoyoyooo")
        key = self.request.get("key")
        bucket = self.request.get("bucket")
        logging.error("key....")
        logging.error(key)
        logging.error("bucket....")
        logging.error(bucket)
        sections = key.split("_")
        ideaID = sections[1]

        idea = model.Idea.get_by_id(int(ideaID))
        userObj = self.user

        if not userObj:
            uploadingUser = model.User.get_by_id(int(sections[2]))
        else:
            uploadingUser = userObj

        file_reference = "%s_%s_%s_" % ( sections[0], sections[1], sections[2] )
        logging.error("File reference: ")
        logging.error(sections)
        logging.error(file_reference)
        fileObj = model.File.query(model.File.file_reference == file_reference).get()

        fileType = "video"
        gcs_filename = "/gs/%s/%s" % ( bucket, key )
        download_url = "http://storage.googleapis.com%s" % gcs_filename[3:]

        # Update file entity
        fileObj.fileType = fileType
        fileObj.gcs_filename = gcs_filename
        fileObj.download_url = download_url
        fileObj.put()

        # Update idea entity's file and video list
        files = idea.files
        videos = idea.videos

        files.append(fileObj.key)
        videos.append(fileObj.download_url)

        idea.files = files
        idea.videos = videos
        idea.put()

        if userObj:
            self.redirect("/me/idea/edit/%s" % idea.slug)
        else:
            self.redirect("/login")

        # self.response.headers['Access-Control-Allow-Origin'] = '*'
        # self.render('fileupload/gcs_upload.html', title='Gcs Upload', **gcs_upload())

    def ok(self):
        """ GCS upload success callback """
        logging.error(" ========================= def ok()self: ====================")
        logging.error(self.request.get("key"))
        logging.debug('GCS upload result : %s' % self.request.query_string)
        bucket = self.request.get('bucket', default_value='')
        key = self.request.get('key', default_value='')
        key_parts = key.rsplit('/', 1)
        folder = key_parts[0] if len(key_parts) > 1 else None

        # show modal box with result
        self.response.headers['Access-Control-Allow-Origin'] = '*'
        self.render('fileupload/gcs_upload.html', title='Gcs Upload', modal_box="show",
                             bucket=bucket, key=key, form_folder=folder, **gcs_upload())

    def post(self):
        logging.error("weird post request to google cloud storage...")
        self.response.headers['Access-Control-Allow-Origin'] = '*'
        self.render_json({
            "message": "success"
            })

class FileUploaded2(MainHandler):
    def get(self):
        logging.error("returned to redirect successfully... yoyoyoyooo")
        key = self.request.get("key")
        bucket = self.request.get("bucket")
        logging.error("key....")
        logging.error(key)
        logging.error("bucket....")
        logging.error(bucket)
        sections = key.split("_")
        # slideShowID = sections[0]

        # slideShow = model.SlideShow.get_by_id(int(slideShowID))
        userObj = self.user

        if not userObj:
            uploadingUser = model.User.get_by_id(int(sections[1]))
        else:
            uploadingUser = userObj

        file_reference = "%s_%s_" % ( sections[0], sections[1] )
        filename = key.replace(file_reference, "")
        logging.error("File reference: ")
        logging.error(sections)
        logging.error(file_reference)
        fileObj = model.File.query(model.File.file_reference == file_reference).get()

        fileType = "video"
        gcs_filename = "/gs/%s/%s" % ( bucket, key )
        download_url = "http://storage.googleapis.com%s" % gcs_filename[3:]

        # Update file entity
        fileObj.fileType = fileType
        fileObj.filename = filename
        fileObj.user = uploadingUser.key
        fileObj.gcs_filename = gcs_filename
        fileObj.download_url = download_url
        fileObj.put()

        # Update idea entity's file and video list
        # files = idea.files
        # videos = idea.videos

        # files.append(fileObj.key)
        # videos.append(fileObj.download_url)

        # idea.files = files
        # idea.videos = videos
        # idea.put()

        if userObj:
            self.redirect("/me/my-videos")
        else:
            self.redirect("/login")

        # self.response.headers['Access-Control-Allow-Origin'] = '*'
        # self.render('fileupload/gcs_upload.html', title='Gcs Upload', **gcs_upload())

    def ok(self):
        """ GCS upload success callback """
        logging.error(" ========================= def ok()self: ====================")
        logging.error(self.request.get("key"))
        logging.debug('GCS upload result : %s' % self.request.query_string)
        bucket = self.request.get('bucket', default_value='')
        key = self.request.get('key', default_value='')
        key_parts = key.rsplit('/', 1)
        folder = key_parts[0] if len(key_parts) > 1 else None

        # show modal box with result
        self.response.headers['Access-Control-Allow-Origin'] = '*'
        self.render('fileupload/gcs_upload.html', title='Gcs Upload', modal_box="show",
                             bucket=bucket, key=key, form_folder=folder, **gcs_upload())

    def post(self):
        logging.error("weird post request to google cloud storage...")
        self.response.headers['Access-Control-Allow-Origin'] = '*'
        self.render_json({
            "message": "success"
            })


class IdeaVideoUpload(MainHandler):
    def get(self):

        ideaID = self.request.get("ideaID")
        userObj = self.user

        if userObj:
            if ideaID:
                empty_file = fileHandlers.save_empty_file()

                logging.error("....................file, idea and user objs")
                logging.error(empty_file)
                logging.error(ideaID)
                logging.error(userObj)

                file_reference = "%s_%s_%s_" % ( str(empty_file.key.id()), str(ideaID), str(userObj.key.id()) )
                empty_file.file_reference = file_reference
                empty_file.put()

                self.response.headers['Access-Control-Allow-Origin'] = '*'
                # self.render('users/user-idea-video-upload.html', title='Gcs Upload', **gcs_upload())
                self.render('fileupload/gcs_upload.html', title='Gcs Upload', **gcs_upload(file_reference))

        else:
            self.redirect("/login")



class OfferLandingPermission(MainHandler):
    def post(self):
        ideaID = self.request.get("ideaID")
        landingDate = self.request.get("landingDate")
        location = self.request.get("location")
        userObj = self.user
        errors = []

        if location:
            # dict_location = json.loads(location)
            # lat = dict_location["lat"]
            # lng = dict_location["lng"]
            # landingLocation = str(lat)+","+str(lng)

            landingLocation = utils.stringLocationFromJson(location)

        if userObj:
            if ideaID:
                idea = model.Idea.get_by_id(int(ideaID))
                if idea:
                    artist = idea.artist.get()

                    landingDate = datetime.datetime.strptime(landingDate, '%Y/%m/%d')

                    if not landingLocation:
                        landingLocation = userObj.location

                    landingPermission = model.LandingPermission(
                        curator = userObj.key,
                        artist = artist.key,
                        idea = idea.key,
                        landingDate = landingDate,
                        location = landingLocation,
                        geopoint = utils.stringToGeoPt(landingLocation) )
                    landingPermission.put()

                    idea.landingRequests += 1
                    idea.put()
                    utils.increment_counter("landingRequests")

                    utils.notify_user("landing-permission-offered", artist)

                    self.render_json({
                        "message": "success",
                        "landingPermissionID": landingPermission.key.id(),
                        "errors": errors
                    })

        else:
            errors.append("You're not logged in.")
            self.render_json({
                "message": "fail",
                "errors": errors
                })









# ===========================================================
# Data Visualisation
# ===========================================================

class DataViz(MainHandler):
    def get(self):

        self.render("experiment/data-viz.html")


class Query(MainHandler):
    def get(self):
        query = self.request.get("q")
        import search
        results = search.textSearch(query, 'ideaIndex')

        self.response.out.write(results)

class LocationQuery(MainHandler):
    def get(self):
        radius = self.request.get("r")

        lat = '41.577047'
        lng = '3.773608'

        import search
        results = search.locationSearch(lat, lng, radius, 'ideaIndex')

        self.response.out.write(results)

class Placeholder(MainHandler):
    def get(self):
        self.render("placeholder.html")

class TestVimeo(MainHandler):
    def get(self):
        import vimeo

        v = vimeo.VimeoClient(
            token='59791959a4bcdbad1c596d4490378195',
            key='381a63f82fe29713a065849ec5d3e0b2895a1bfe',
            secret='POw9RMwFAjYSC3IlaJK6lthXJwkOBb0Xt+ZeU68cygo5OQnyqeYhN4tgARhH1PtPDGDhSV1fbiPiwFYllDERTd8CL/TdlhhIYJk8ejt3RZwG7EGMpc/K5B2UADQP8auY')

        # Make the request to the server for the "/me" endpoint.
        #about_me = v.get('/me')
        #video_info = v.get('https://api.vimeo.com/videos/99483135')#https://vimeo.com/99483135

        # Works in the playground
        # https://developer.vimeo.com/api/playground/me/videos/99483135
        # a_video = v.get("https://api.vimeo.com/me/videos/99483135")

        my_videos = v.get('/me/videos')

        assert my_videos.status_code == 200  # Make sure we got back a successful response.
        logging.info(" - - - - - - - - - - -  vimeo api")
        # logging.info( about_me.json() )   # Load the body's JSON data.
        # logging.info( video_info.json() )
        logging.info( my_videos.json() )

class AddVideo(MainHandler):
    def get(self, ideaID):
        idea = model.Idea.get_by_id(int(ideaID))

        idea.videos = ["http://storage.googleapis.com/non-territorial.appspot.com/5730827476402176_5096168749006848_5682617542246400_emile-me-flappy.webm"]
        idea.put()



class Dashboard(MainHandler):
    def get(self):
        userObj = self.user
        if userObj:
            in_flight = model.UserSlideShow.query().order(-model.UserSlideShow.created).fetch(4)
            waiting_to_take_off = model.SlideShow.query(model.SlideShow.userType == 'artist').order(-model.SlideShow.created).fetch(4)
            slideshows = model.SlideShow.query(model.SlideShow.user == userObj.key).order(-model.SlideShow.created).fetch()

            userSlideShow = None
            if userObj.userType == "airport" or userObj.userType == "freeport" or userObj.userType == "gallery":
                if userObj.slideShow:
                    userSlideShow = userObj.slideShow.get()

            if userObj.userType == "gallery":
                userSlideShows = model.UserSlideShow.query(model.UserSlideShow.user == userObj.key).fetch()
                slideshows = []
                for userSlideShow in userSlideShows:
                    slideshows.append(userSlideShow.slideShow.get())

            savedSlideShows = model.SavedSlideShow.query(model.SavedSlideShow.user == userObj.key).fetch()
            bookmarked_slideshows = []
            for slideShow in savedSlideShows:
                bookmarked_slideshows.append(slideShow.slideShow.get())

            # logging.error("SLIDESHOWS . .  . . . . . . .")
            # logging.error(slideshows)
            self.render("slideshow/me.html", in_flight=in_flight, waiting_to_take_off=waiting_to_take_off, slideshows=slideshows, savedSlideShows=bookmarked_slideshows, userSlideShow=userSlideShow)
        else:
            self.redirect("/login")

class SaveUser(MainHandler):
    def post(self):
        name = self.request.get("name")
        surname = self.request.get("surname")
        about = self.request.get("about")
        newsletter = self.request.get("newsletter")
        address = self.request.get("address")
        country = self.request.get("country")
        locality = self.request.get("locality")
        ideaLocation = self.request.get("ideaLocation")
        location = utils.stringLocationFromJson(ideaLocation)

        userObj = self.user

        if userObj:
            # logging.info("hello?")
            userObj.name = name
            userObj.surname = surname
            userObj.about = about
            userObj.newsletter = newsletter
            userObj.location = location
            userObj.geopoint = utils.stringToGeoPt(location)
            userObj.address = address
            userObj.country = country
            userObj.locality = locality

            if not userObj.flightCode:
                flightCode = utils.get_flight_code(userObj)
                logging.error("result from getting flight code. . . . . : %s" % flightCode)
                if not flightCode:
                    logging.error("- - - - - - - - - -- - - -  need to set name and surname in order to get a flight code.")
                else:
                    userObj.flightCode = flightCode

            userObj.put()

            # Save the user location for later reference
            if userObj.userType == 'artist' or userObj.userType == 'airport' or userObj.userType == 'freeport' or userObj.userType == 'gallery':
                user_country = userObj.country
                user_locality = userObj.locality
                if user_country:
                    country = model.Country.query(model.Country.country == user_country).get()
                    if not country:
                        country = model.Country(country=user_country, location=userObj.location, geopoint=userObj.geopoint)
                        country.put()

                if user_locality:
                    locality = model.Locality.query(model.Locality.locality == user_locality).get()
                    if not locality:
                        locality = model.Locality(locality=user_locality, location=userObj.location, geopoint=userObj.geopoint)
                        locality.put()

            self.render_json(userObj.to_dict(exclude=["geopoint", "created", "file_obj", "slideShow"]))

        else:
            self.redirect("/login")

class ProfileImageUpload(MainHandler):
    def post(self):
        userObj = self.user

        if userObj:
            file_req = self.request.POST["file"]
            file_obj = fileHandlers.upload_file(file_req)

            # delete old file if its there
            if file_obj and userObj.file_obj:
                fileHandlers.delete_from_gcs(userObj.file_obj.get().gcs_filename, True)

            userObj.file_obj = file_obj.key
            userObj.profile_image = file_obj.image_url
            userObj.put()

            self.render_json(userObj.to_dict(exclude=["geopoint", "created", "file_obj"]))

        else:
            self.redirect("/login")

class UserVideos(MainHandler):
    def get(self):
        userObj = self.user
        videos = model.File.query(model.File.user == userObj.key, model.File.fileType == "video").fetch()
        logging.info("videos")
        logging.info(videos)
        self.render("slideshow/my-videos.html", videos=videos)

class APIUserVideos(MainHandler):
    def get(self):
        userObj = self.user
        if userObj:
            videos = model.File.query(model.File.user == userObj.key, model.File.fileType == "video").fetch()

            video_list = []
            for video in videos:
                videoObj = {}
                videoObj["filename"] = video.filename
                videoObj["download_url"] = video.download_url
                video_list.append(videoObj)

            self.render_json(video_list)

        else:
            self.redirect("/login")

class APIPortsTopoJson(MainHandler):
    def get(self):

        # Pseudocode
        #  - get all airports and freeports
        #  - get all userSlideShows
        #  - get all artists associated with userSlideshows --> UserSlideShow.slideshow.get().user.get()
        #  - get take Off location --> UserSlideShow.takeOffLoction
        #  - make a mapping of takeoff and landing locations
        #    -- > UserSlideShow.location = landing UserSlideShow.takeOffLocation = take off

        airports = model.User.query(model.User.userType == "airport", model.User.approved == True).fetch()
        freeports = model.User.query(model.User.userType == "freeport", model.User.approved == True).fetch()

        waiting_to_take_off_dd = get_front_page_flight_list()['waiting_to_take_off_dd']# get de-duplicated list of ideas waiting to take off
        waiting_to_take_off = []
        for idx, w in enumerate( waiting_to_take_off_dd ):
            w_user = w.user.get()
            if w_user.isSonic:
                sonic = "sonic"
            else:
                sonic = ""
            portObj = {
                "type": "Feature",
                "id": "artist-%s" % ( w.user.id() ),
                "properties": {
                    "portType": "artist",
                    "sonic": sonic,
                    "portFlightNumber": "%s%s" % ( w.flightCode, w.flightNumber ),
                    "portTitle": w.title,
                    "portName": "%s %s" % ( w_user.name.capitalize(), w_user.surname.capitalize()),
                    "portLat": w_user.location.split(",")[0],
                    "portLng": w_user.location.split(",")[1],
                    "portLocality": w_user.locality,
                    "portCountry": w_user.country,
                    "portLink": "/idea/%s/%s" % (w.flightCode, w.slug)
                },
              "geometry": {
                "type": "Point",
                "coordinates": [float( w.location.split(",")[1] ), float( w.location.split(",")[0] )]
              }
            }

            waiting_to_take_off.append(portObj)


        # artists = model.User.query(model.User.userType == "artist", model.User.approved == True).fetch()

        userSlideShows = model.UserSlideShow.query(model.UserSlideShow.approved == True).fetch(100)


        ports = []
        connections = []

        # weirdly, the lat and long are swapped here hence airport.location.split(",")[1], airport.location.split(",")[1]
        for airport in airports:
            if airport.location:
                slideShow = model.SlideShow.query(model.SlideShow.user == airport.key).get()
                if slideShow:
                    title = slideShow.title
                    link = "/idea/%s/%s" % ( airport.flightCode, slideShow.slug )
                else:
                    title = airport.username
                    link = "/u/%s" % airport.flightCode

                if airport.isSonic:
                    sonic = "sonic"
                else:
                    sonic = ""

                portObj = {
                        "type": "Feature",
                        "id": "port-%s" % airport.key.id(),
                        "properties": {
                            "portType": "airport",
                            "sonic": sonic,
                            "portFlightNumber": "",
                            "portTitle": title,
                            "portName": airport.name.capitalize(),
                            "portLat": airport.location.split(",")[0],
                            "portLng": airport.location.split(",")[1],
                            "portLocality": airport.locality,
                            "portCountry": airport.country,
                            "portLink": link
                        },
                      "geometry": {
                        "type": "Point",
                        "coordinates": [float( airport.location.split(",")[1] ), float( airport.location.split(",")[0] )]
                      }
                    }
                ports.append(portObj)
        for freeport in freeports:
            if freeport.location:
                slideShow = model.SlideShow.query(model.SlideShow.user == freeport.key).get()
                if slideShow:
                    title = slideShow.title
                    link = "/idea/%s/%s" % ( freeport.flightCode, slideShow.slug )
                else:
                    title = freeport.username
                    link = "/u/%s" % freeport.flightCode

                if freeport.isSonic:
                    sonic = "sonic"
                else:
                    sonic = ""

                portObj = {
                        "type": "Feature",
                        "id": "port-%s" % freeport.key.id(),
                        "properties": {
                            "portType": "freeport",
                            "sonic": sonic,
                            "portFlightNumber": "",
                            "portTitle": title,
                            "portName": freeport.name.capitalize(),
                            "portLat": freeport.location.split(",")[0],
                            "portLng": freeport.location.split(",")[1],
                            "portLocality": freeport.locality,
                            "portCountry": freeport.country,
                            "portLink": link
                        },
                      "geometry": {
                        "type": "Point",
                        "coordinates": [float( freeport.location.split(",")[1] ), float( freeport.location.split(",")[0] )]
                      }
                    }
                ports.append(portObj)

        for idx, flight in enumerate(userSlideShows):
            slideshow = flight.slideShow.get()
            artist = slideshow.user.get()
            if artist.location and not artist.isGalleryArtist:
                slideShow = model.SlideShow.query(model.SlideShow.user == artist.key).get()
                if slideShow:
                    title = slideShow.title
                    portFlightNumber = "%s%s" % ( slideShow.flightCode, slideShow.flightNumber )
                    link = "/idea/%s/%s" % ( artist.flightCode, slideShow.slug )
                else:
                    title = artist.username
                    portFlightNumber = ""
                    link = "/u/%s" % artist.flightCode

                if artist.isSonic:
                    sonic = "sonic"
                else:
                    sonic = ""

                portObj = {
                        "type": "Feature",
                        "id": "artist-%s" % ( artist.key.id() ),
                        "properties": {
                            "portType": "artist",
                            "sonic": sonic,
                            "portFlightNumber": portFlightNumber,
                            "portTitle": title,
                            "portName": "%s %s" % ( artist.name.capitalize(), artist.surname.capitalize()),
                            "portLat": artist.location.split(",")[0],
                            "portLng": artist.location.split(",")[1],
                            "portLocality": artist.locality,
                            "portCountry": artist.country,
                            "portLink": link
                        },
                      "geometry": {
                        "type": "Point",
                        "coordinates": [float( artist.location.split(",")[1] ), float( artist.location.split(",")[0] )]
                      }
                    }
                ports.append(portObj)

            import math
            duration = math.floor( (flight.landingDate - flight.takeOffDate).total_seconds() )
            connectionObj = [
                "artist-%s" % artist.key.id(),
                "port-%s" % flight.user.id(),
                "#%s%s" % (flight.flightNumber, flight.flightCode),
                flight.surname,
                duration,
                {
                    "slug": slideshow.slug,
                    "flightCode": flight.flightCode,
                    "title": slideshow.title,
                    "slideShowID": slideshow.key.id(),
                    "ideaID": flight.slideShow.id(),
                    "destinationID": flight.user.id(),
                    "flightID": flight.key.id(),
                    "flightNumber": "%s%s" % (flight.flightCode, flight.flightNumber),
                    "surname": flight.surname,
                    "duration": duration,
                    "landingDate": flight.landingDate.strftime('%d-%m-%y'),
                    "from": flight.takeOffLocality,
                    "to":  flight.landingLocality,
                    "featured":  False
                }
            ]
            connections.append(connectionObj)

            #-- not sure if this line was a bug waiting to happen?# ports.append(portObj)

        # from topojson import topojson

        # geojson_ports = { "type": "FeatureCollection", "features": ports }

        # #give it a path in and out
        # ports = topojson(geojson_ports, quantization=1e6, simplify=0.0001)

        ports = ports + waiting_to_take_off

        # mark the first vector as featured... can dynamically set this later

        if len(connections):
            connections[0][5]["featured"] = True

        self.render_json(
            {
                "ports": ports,
                "connections": connections
            }
        )

class APIGalleryTopoJson(MainHandler):
    def get(self, gallery_slug):

        # Pseudocode
        #  - get the gallery
        #  - get all GallerySlideShows
        #  - get all artists associated with userSlideshows --> UserSlideShow.slideshow.get().user.get()
        #  - get take Off location --> UserSlideShow.takeOffLoction
        #  - make a mapping of takeoff and landing locations
        #    -- > UserSlideShow.location = landing UserSlideShow.takeOffLocation = take off

        # NBNBNB may need to change the exhibition link to /exhibition/gallery-slug/exhibition-slug

        gallery = model.User.query(model.User.slug == gallery_slug, model.User.approved == True).get()
        if not gallery:
            gallery = model.User.query(model.User.flightCode == gallery_slug, model.User.approved == True).get()

        gallerySlideShows = model.UserSlideShow.query(model.UserSlideShow.user == gallery.key).fetch()

        ports = []
        connections = []

        for idx, flight in enumerate(gallerySlideShows):
            slideshow = flight.slideShow.get()
            artist = slideshow.user.get()
            if artist.location:
                slideShow = model.SlideShow.query(model.SlideShow.user == artist.key).get()
                if slideShow:
                    title = slideShow.title
                    portFlightNumber = "%s%s" % ( slideShow.flightCode, slideShow.flightNumber )
                    link = "/exhibition/%s/%s" % ( gallery.flightCode, slideShow.slug )# <-- may need to change this
                else:
                    title = artist.username
                    portFlightNumber = ""
                    link = "/u/%s" % artist.flightCode

                if artist.isSonic:
                    sonic = "sonic"
                else:
                    sonic = ""

                artist_port_type = "artist"
                if gallery.upgraded:
                    artist_port_type = "freeport"

                portObj = {
                        "type": "Feature",
                        "upgraded": gallery.upgraded,
                        "id": "artist-%s" % ( artist.key.id() ),
                        "properties": {
                            "portType": artist_port_type,
                            "sonic": sonic,
                            "portFlightNumber": portFlightNumber,
                            "portTitle": title,
                            "portName": "%s %s" % ( artist.name.capitalize(), artist.surname.capitalize()),
                            "portLat": artist.location.split(",")[0],
                            "portLng": artist.location.split(",")[1],
                            "portLocality": artist.locality,
                            "portCountry": artist.country,
                            "portLink": link
                        },
                      "geometry": {
                        "type": "Point",
                        "coordinates": [float( artist.location.split(",")[1] ), float( artist.location.split(",")[0] )]
                      }
                    }
                ports.append(portObj)

            # if gallery.slideShow:
            #     galleryLink = "/idea/%s/%s" % ( gallery.flightCode, gallery.slideShow.get().slug )
            # else:
            #     galleryLink = '/gallery/%s' % gallery.slug

            # portObjGallery = {
            #             "type": "Feature",
            #             "id": "port-%s" % ( gallery.key.id() ),
            #             "properties": {
            #                 "portType": "freeport",
            #                 "sonic": sonic,
            #                 "portFlightNumber": "",
            #                 "portTitle": "",
            #                 "portName": "%s" % ( gallery.username.capitalize() ),
            #                 "portLat": gallery.location.split(",")[0],
            #                 "portLng": gallery.location.split(",")[1],
            #                 "portLocality": gallery.locality,
            #                 "portCountry": gallery.country,
            #                 "portLink": galleryLink
            #             },
            #           "geometry": {
            #             "type": "Point",
            #             "coordinates": [float( gallery.location.split(",")[1] ), float( gallery.location.split(",")[0] )]
            #           }
            #         }

            # ports.append(portObjGallery)

            logging.error("----- connection objects .......")
            logging.error(flight.user.id())
            logging.error(gallery.key.id())

            import math
            duration = math.floor( (flight.landingDate - flight.takeOffDate).total_seconds() )
            connectionObj = [
                "artist-%s" % artist.key.id(),
                "port-%s" % flight.user.id(),
                "#%s" % (flight.flightCode),
                flight.surname,
                duration,
                {
                    "slug": slideshow.slug,
                    "flightCode": flight.flightCode,
                    "title": slideshow.title,
                    "slideShowID": slideshow.key.id(),
                    "ideaID": flight.slideShow.id(),
                    "destinationID": flight.user.id(),
                    "flightID": flight.key.id(),
                    "flightNumber": "%s%s" % (flight.flightCode, flight.flightNumber),
                    "surname": flight.surname,
                    "duration": duration,
                    "landingDate": flight.landingDate.strftime('%d-%m-%y'),
                    "from": flight.takeOffLocality,
                    "to":  flight.landingLocality,
                    "featured":  False,
                    "galleryLink": '/exhibition/%s/%s' % (gallery.flightCode, flight.slug),
                    "upgraded": gallery.upgraded
                }
            ]
            connections.append(connectionObj)

        # ports = ports + waiting_to_take_off


        if gallery.slideShow:
            galleryLink = "/idea/%s/%s" % ( gallery.flightCode, gallery.slideShow.get().slug )
        else:
            galleryLink = '/gallery/%s' % gallery.slug

        freeport_port_type = "freeport"
        if gallery.upgraded:
            freeport_port_type = "artist"

        portObjGallery = {
                    "type": "Feature",
                    "upgraded": gallery.upgraded,
                    "id": "port-%s" % ( gallery.key.id() ),
                    "properties": {
                        "portType": freeport_port_type,
                        "sonic": "",
                        "portFlightNumber": "",
                        "portTitle": "",
                        "portName": "%s" % ( gallery.username.capitalize() ),
                        "portLat": gallery.location.split(",")[0],
                        "portLng": gallery.location.split(",")[1],
                        "portLocality": gallery.locality,
                        "portCountry": gallery.country,
                        "portLink": galleryLink
                    },
                  "geometry": {
                    "type": "Point",
                    "coordinates": [float( gallery.location.split(",")[1] ), float( gallery.location.split(",")[0] )]
                  }
                }

        ports.append(portObjGallery)


        # mark the first vector as featured... can dynamically set this later
        if len(connections):
            connections[0][5]["featured"] = True

        self.render_json(
            {
                "ports": ports,
                "connections": connections
            }
        )

class APISaveSlideShow(MainHandler):
    def post(self):
        slideShowID = self.request.get("slideShowID")

        userObj = self.user

        if userObj:
            slideShow = model.SlideShow.get_by_id(int(slideShowID))
            if slideShow:
                bookmark = model.SavedSlideShow.query(model.SavedSlideShow.user == userObj.key, model.SavedSlideShow.slideShow == slideShow.key).get()
                if not bookmark:
                    bookmark = model.SavedSlideShow(user=userObj.key, slideShow=slideShow.key)
                    bookmark.put()

                self.render_json({
                    "message": "success",
                    "savedSlideShowID": bookmark.key.id()
                    })

        else:
            self.render_json({
                    "message": "success",
                    "savedSlideShowID": None
                    })


class UserVideoUpload(MainHandler):
    def get(self):
        userObj = self.user
        if userObj:
            empty_file = fileHandlers.save_empty_file()
            logging.error("....................file and user objs")
            logging.error(empty_file)
            logging.error(userObj)

            file_reference = "%s_%s_" % ( str(empty_file.key.id()), str(userObj.key.id()) )
            empty_file.file_reference = file_reference
            empty_file.put()

            self.response.headers['Access-Control-Allow-Origin'] = '*'
            # self.render('users/user-idea-video-upload.html', title='Gcs Upload', **gcs_upload())
            self.render('slideshow/video-upload-2.html', title='Gcs Upload', **gcs_upload(file_reference))

        else:
            self.redirect("/login")


class NewSlideShow(MainHandler):
    def get(self):

        slideShowID = self.request.get("slideShowID")
        slideShow = None
        if slideShowID:
            slideShow = model.SlideShow.get_by_id(int(slideShowID))

        # self.render("slideshow/new-slideshow.html")
        # self.render("slideshow/new-slide-show-single-column.html")
        self.render("slideshow/new-slideshow-abs.html", slideShow=slideShow)

class EditSlideShow(MainHandler):
    def get(self, slideShowID):
        userObj = self.user
        if userObj:
            slideShow = None
            if slideShowID:
                slideShow = model.SlideShow.get_by_id(int(slideShowID))

            belongs_to_gallery = None
            if userObj.userType == "gallery" and slideShow:
                belongs_to_gallery = model.ExhibitionArtist.query(
                    model.ExhibitionArtist.exhibition == slideShow.key,
                    model.ExhibitionArtist.gallery == userObj.key).get()

            slides = model.Slide.query(model.Slide.slideShow == slideShow.key, model.Slide.user == userObj.key).order(model.Slide.slideOrder).fetch()
            videos = model.File.query(model.File.user == userObj.key, model.File.fileType == "video").fetch()
            # videos = [
            #     {
            #         'download_url': "http://storage.googleapis.com/nonterritorial-dev.appspot.com/6288801441775616_5162901534932992_Making of Babel by Cildo Meireles.mp4",
            #         'filename': "meireles.mp4"
            #     }
            # ]

            # self.render("slideshow/edit-slideshow.html", slideShow=slideShow, slides=slides, videos=videos, belongs_to_gallery=belongs_to_gallery)
            self.render("slideshow/edit-slideshow-alt.html", slideShow=slideShow, slides=slides, videos=videos, belongs_to_gallery=belongs_to_gallery)
        else:
            self.redirect("/login")

class EditExhibitionDetails(MainHandler):
    def get(self, slideShowID):

        userObj = self.user

        year = datetime.datetime.now().year
        year15 = int(year) + 16
        exhibition = model.SlideShow.get_by_id(int(slideShowID))
        exhibition_artist = exhibition.user.get()

        userSlideShow = model.UserSlideShow.query(model.UserSlideShow.user == userObj.key, model.UserSlideShow.slideShow == exhibition.key).get()

        self.render("slideshow/edit-exhibition.html", exhibition=exhibition, exhibition_artist=exhibition_artist, year=year, year15=year15, userSlideShow=userSlideShow)

class NewExhibitionSlideShow(MainHandler):
    def get(self):
        year = datetime.datetime.now().year
        year15 = int(year) + 16
        self.render("slideshow/new-exhibition.html", year15=year15, year=year)

    def post(self):
        title = self.request.get("title")
        select_artist = self.request.get("select_artist")
        artist_name = self.request.get("artist_name")
        about_artist = self.request.get("about_artist")

        if select_artist:
            artist = model.User.get_by_id(int(select_artist))

class AudioSlideShow(MainHandler):
    def get(self, slideShowID):
        slideShow = model.SlideShow.get_by_id(int(slideShowID))
        self.render("slideshow/slideshow-audio.html", slideShow=slideShow)

    def post(self, slideShowID):
        slideShow = model.SlideShow.get_by_id(int(slideShowID))
        userObj = self.user

        if userObj and slideShow:
            import fileHandlers
            file_req = self.request.POST["file"]
            file_obj = fileHandlers.upload_file(file_req)
            file_obj.user = userObj.key
            file_obj.isAudio = True
            file_obj.put()
            if slideShow.audio:
                old_file = model.File.query(model.File.user == userObj.key, model.File.download_url == slideShow.audio).get()
                fileHandlers.delete_from_gcs(old_file.gcs_filename, False)
                old_file.key.delete()

            slideShow.audio = file_obj.download_url
            slideShow.audio_name = file_obj.filename

            slideShow.put()

            self.render_json({
                "success": True,
                "message": "uploaded fileID: %s" % file_obj.key.id()
            })

        else:
            self.render_json({
                "success": True,
                "message": "couldn't upload file"
            })

class SlideReorder(MainHandler):
    def post(self):
        new_order = json.loads( self.request.get("order") )
        for o in new_order:
            slide = model.Slide.get_by_id(int(o["id"]))
            slide.slideOrder = o["index"]
            slide.put()

        self.render_json({
            "message": "success"
        })

class SlideShowDelete(MainHandler):
    def post(self, slideShowID):
        # slideShowID = self.request.get("slideShowID")

        slideShow = model.SlideShow.get_by_id(int(slideShowID))

        slides = model.Slide.query(model.Slide.slideShow == slideShow.key).fetch()

        userSlideShows = model.UserSlideShow.query(model.UserSlideShow.slideShow == slideShow.key).fetch()

        for slide in slides:
            if slide.image_url:
                if slide.file_obj:
                    fileObj = slide.file_obj.get()
                    gcs_filename = fileObj.gcs_filename
                    fileHandlers.delete_from_gcs(gcs_filename, True)
                    fileObj.key.delete()

            slide.key.delete()

        for userSlideShow in userSlideShows:
            userSlideShow.key.delete()

        slideShow.key.delete()

        self.render_json({
            "message": "success",
            "slideShowID": slideShowID
        })

class SlideDelete(MainHandler):
    def post(self):
        slideID = self.request.get("slideID")

        slide = model.Slide.get_by_id(int(slideID))

        slideOrder = slide.slideOrder

        logging.info("slide.image_url")
        logging.info(slide.image_url)
        logging.info("slide.file_obj")
        logging.info(slide.file_obj)

        # not deleting videos on purpose in case they need to be used later... its a large uplaod, so convenient to not have to uplaod

        if slide.image_url:
            if slide.file_obj:
                fileObj = slide.file_obj.get()
                gcs_filename = fileObj.gcs_filename
                fileHandlers.delete_from_gcs(gcs_filename, True)
                fileObj.key.delete()

        slide.key.delete()

        self.render_json({
            "message": "success",
            "slideID": slideID,
            "slideOrder": slideOrder
        })

class SaveSlide(MainHandler):
    def post(self):
        logging.info(" - - - - - - - saving slide - - -- - - -  - -")
        # logging.info(" . . . .. . . . . . . ..  .. . . . . . . . . .")
        title = self.request.get("title")
        description = self.request.get("description")
        slideTitle = self.request.get("slideTitle")
        slideDescription = self.request.get("slideDescription")

        slideVideoUrl = self.request.get("slideVideoUrl")

        slideShowID = self.request.get("slideShowID")
        slideID = self.request.get("slideID")
        slideType = self.request.get("slideType")
        slideOrder = self.request.get("slideOrder")

        # for galleries, if they add their own artist
        booking_link = self.request.get("booking_link")
        select_artist = self.request.get("select_artist")
        artist_first_name = self.request.get("artist_first_name")
        artist_last_name = self.request.get("artist_last_name")
        about_artist = self.request.get("about_artist")
        address = self.request.get("address")
        country = self.request.get("country")
        locality = self.request.get("locality")
        ideaLocation = self.request.get("ideaLocation")
        date = self.request.get("date")
        exhibitionEndDate = self.request.get("exhibitionEndDate")
        isGallery = self.request.get("isGallery")
        location = None
        geopoint = None
        if ideaLocation:
            location = utils.stringLocationFromJson(ideaLocation)
            geopoint = utils.stringToGeoPt(location)

        logging.info(" - - - - - - - artist data - - -- - - -  - -")
        # logging.info(location)
        # logging.info(country)
        # logging.info(locality)
        # logging.info(isGallery)

        gallery_artist = None
        if select_artist:
            gallery_artist = model.User.get_by_id(int(select_artist))
        elif artist_first_name or artist_last_name or about_artist:
            if slideShowID:
                existing_exhibition = model.SlideShow.get_by_id(int(slideShowID))
                existing_exhibition.user.delete()

            gallery_artist = model.User(
                name=artist_first_name,
                surname=artist_last_name,
                about=about_artist,
                isGalleryArtist=True,
                address=address,
                country=country,
                locality=locality,
                location=location,
                geopoint=geopoint,
                newsletter="no-newsletter")
            gallery_artist.put()
        else:
            gallery_artist = None



        # check if there's an existing artist with the same name and surname, that belongs to the gallery
        # if so, find the user and add that, ?? 2 people with the same name???
        # add a flag on a user to indicate that they belong to a gallery and don't have all the necessary info, like an email address

        # logging.info(" slideID: %s" % slideID)

        userObj = self.user

        if userObj:
            if userObj.approved:
                if not slideShowID:
                    if gallery_artist:
                        slideShowObj = utils.create_slideshow(gallery_artist, title, description)
                    else:
                        slideShowObj = utils.create_slideshow(userObj, title, description)
                    slideShowID = slideShowObj.key.id()
                else:
                    logging.error("slide show ID %s" % slideShowID)
                    slideShowObj = model.SlideShow.get_by_id(int(slideShowID))
                    if title:
                        slug = slugify.slugify(title)
                        slideShowObj.title = title
                        slideShowObj.slug = slug
                    if description:
                        slideShowObj.description = description

                    slideShowObj.put()

                #  FOR THE CASE OF A GALLERY
                # logging.error("-------------GA--------------")
                # logging.error(gallery_artist)

                # if slideShowObj and isGallery == "yes":
                #     userObj.editedSpace = True
                #     userObj.slideShow = slideShowObj.key
                #     userObj.put()

                if slideShowObj and gallery_artist and userObj:
                    gallery_slideshow = model.ExhibitionArtist.query(
                        model.ExhibitionArtist.gallery==userObj.key,
                        model.ExhibitionArtist.exhibition==slideShowObj.key).get()
                    if not gallery_slideshow:
                        gallery_slideshow = model.ExhibitionArtist()

                    gallery_slideshow.exhibition = slideShowObj.key
                    gallery_slideshow.gallery = userObj.key
                    gallery_slideshow.artist = gallery_artist.key
                    gallery_slideshow.put()

                    slideShowObj.user = gallery_artist.key
                    slideShowObj.put()

                    # save the UserSlideShow to link gallery with exhibition/slideshow
                    takeOffDate = datetime.datetime.now()
                    landingDate = datetime.datetime.strptime(date, '%Y/%m/%d')

                    if exhibitionEndDate:
                        exhibitionEndDate = datetime.datetime.strptime(exhibitionEndDate, '%Y/%m/%d')

                    UserSlideShow = model.UserSlideShow.query(model.UserSlideShow.user == userObj.key, model.UserSlideShow.slideShow==slideShowObj.key).get()

                    if not UserSlideShow:
                        UserSlideShow = model.UserSlideShow(
                            user = userObj.key,
                            userType = userObj.userType,
                            userFlightCode = userObj.flightCode,
                            slideShow = slideShowObj.key,
                            title = slideShowObj.title,
                            flightCode = userObj.flightCode,
                            slug = slideShowObj.slug,
                            flightNumber = slideShowObj.flightNumber,
                            surname = slideShowObj.surname,
                            takeOffDate = takeOffDate,
                            landingDate = landingDate,
                            departureDate = exhibitionEndDate,
                            takeOffLocation = slideShowObj.user.get().location,
                            takeOffCountry = slideShowObj.country,
                            takeOffLocality = slideShowObj.locality,
                            location = userObj.location,
                            landingCountry = userObj.country,
                            landingLocality = userObj.locality,
                            geopoint = userObj.geopoint,
                            accepted = True,
                            public = True
                        )
                        UserSlideShow.put()
                        utils.increment_counter("flights")
                    else:
                        UserSlideShow.landingDate = landingDate
                        UserSlideShow.departureDate = exhibitionEndDate
                        UserSlideShow.takeOffLocation = slideShowObj.user.get().location
                        UserSlideShow.takeOffCountry = slideShowObj.country
                        UserSlideShow.takeOffLocality = slideShowObj.locality
                        UserSlideShow.put()

                    slideShowObj.inFlight = True
                    slideShowObj.booking_link = booking_link
                    slideShowObj.flightCode = userObj.flightCode
                    slideShowObj.isGallerySlideShow = True
                    slideShowObj.put()

                logging.error("slideID %s" % slideID)
                if not slideID:
                    logging.error("no slideID.... so create a new one")
                    slideObj = utils.create_slide(self, slideShowObj, slideType)
                    slideID = slideObj.key.id()
                else:
                    slideObj = model.Slide.get_by_id(int(slideID))
                    utils.update_slide(self, slideShowObj, slideObj)

                # if slideType == "text":
                #     slideObj.title = title
                #     slideObj.description = description
                #     slideObj.slideOrder = int( slideOrder )
                #     slideObj.put()

                dictSlideObj = slideObj.to_dict(exclude=["geopoint", "created", "user", "slideShow", "file_obj"])
                dictSlideObj["slideShowID"] = slideShowID
                dictSlideObj["slideID"] = slideID
                dictSlideObj["slideOrder"] = slideOrder
                dictSlideObj["slideType"] = slideType
                dictSlideObj["slideTitle"] = slideTitle
                dictSlideObj["slideDescription"] = slideDescription
                dictSlideObj["slideVideoUrl"] = slideVideoUrl
                dictSlideObj["slideType"] = slideType

                # preview_slide_html = '<div id="slide-reorder-" class="slide-preview slide-preview-sortable" data-index="'+slide_order+'" data-id="">'+
                #         '<p>NEW SLIDE</p>'+
                #         '<button type="button" class="btn-flat delete-slide slide-thumb-btn hidden" data-slide-id="">DELETE SLIDE</button>'+
                #         '<button type="button" class="btn-flat slide-thumb-btn hidden" data-slide-id="">REORDER</button>'+
                #         '<button type="button" class="btn-flat go-to-slide slide-thumb-btn hidden" data-slide-id="" data-slide-order="'+slide_order+'">GO TO SLIDE</button>'+
                #     '</div>'

                # logging.info(" - - - - - - - - - - - -- - - - - - - - ")
                # logging.info(dictSlideObj)

                if slideOrder == 0 or slideOrder == '0':
                    if isGallery == "yes":
                        self.render_json({
                            "url": "/me/idea/edit/%s" % slideShowID
                        })
                    else:
                        self.redirect("/me/idea/edit/%s" % slideShowID)
                else:
                    self.render_json(dictSlideObj)
            else:
                self.redirect("/me")

        else:
            self.redirect('/login')

class ListIdeas(MainHandler):
    def get(self):
        import urllib2
        country = urllib2.unquote( self.request.get("country") )
        locality = urllib2.unquote( self.request.get("locality") )
        order = self.request.get("order")

        year = datetime.datetime.now().year
        year15 = int(year) + 16

        in_flight = model.UserSlideShow.query().order(-model.UserSlideShow.created).fetch(4)
        waiting_to_take_off = model.SlideShow.query(model.SlideShow.userType == 'artist').order(-model.SlideShow.created).fetch(4)

        # ideas = model.SlideShow.query(model.SlideShow.userType == "artist").order(-model.SlideShow.created).fetch(20)

        curs = Cursor(urlsafe=self.request.get('cursor'))
        if country:
            ideas, next_curs, more = model.SlideShow.query(model.SlideShow.userType == "artist", model.SlideShow.approved == True, model.SlideShow.country == country).order(-model.SlideShow.created).fetch_page(20, start_cursor=curs)
        elif order:
            if order == "new":
                ideas, next_curs, more = model.SlideShow.query(model.SlideShow.userType == "artist", model.SlideShow.approved == True).order(-model.SlideShow.created).fetch_page(20, start_cursor=curs)
            elif order == "old":
                ideas, next_curs, more = model.SlideShow.query(model.SlideShow.userType == "artist", model.SlideShow.approved == True).order(model.SlideShow.created).fetch_page(20, start_cursor=curs)
        elif locality:
            ideas, next_curs, more = model.SlideShow.query(model.SlideShow.userType == "artist", model.SlideShow.approved == True, model.SlideShow.locality == locality).order(-model.SlideShow.created).fetch_page(20, start_cursor=curs)
        else:
            ideas, next_curs, more = model.SlideShow.query(model.SlideShow.userType == "artist", model.SlideShow.approved == True).order(-model.SlideShow.created).fetch_page(20, start_cursor=curs)
        if more and next_curs:
            next_curs = next_curs.urlsafe()
        else:
            next_curs = None

        countries = model.Country.query().order(model.Country.country).fetch()
        localities = model.Locality.query().order(model.Locality.locality).fetch()

        userObj = self.user
        checked_invitations = []
        if userObj:
            invitations = model.UserSlideShow.query(model.UserSlideShow.user == userObj.key).fetch()
            for invite in invitations:
                checked_invitations.append( int(invite.slideShow.id()) )


        # self.render("slideshow/idea_list.html", ideas=ideas, checked_invitations=checked_invitations, in_flight=in_flight, waiting_to_take_off=waiting_to_take_off, year=year, year15=year15, next_curs=next_curs)
        self.render("slideshow/idea_list.html", ideas=ideas, checked_invitations=checked_invitations, year=year, year15=year15, next_curs=next_curs, countries=countries, localities=localities)

class WaitingToTakeoff(MainHandler):
    def get(self):
        import urllib2
        country = urllib2.unquote( self.request.get("country") )
        locality = urllib2.unquote( self.request.get("locality") )
        order = self.request.get("order")

        list_title = "Waiting To Take Off"

        curs = Cursor(urlsafe=self.request.get('cursor'))
        if country:
            ideas, next_curs, more = model.SlideShow.query(model.SlideShow.userType == "artist", model.SlideShow.inFlight == False, model.SlideShow.approved == True, model.SlideShow.country == country).order(-model.SlideShow.created).fetch_page(20, start_cursor=curs)
        elif order:
            if order == "new":
                ideas, next_curs, more = model.SlideShow.query(model.SlideShow.userType == "artist", model.SlideShow.inFlight == False, model.SlideShow.approved == True).order(-model.SlideShow.created).fetch_page(20, start_cursor=curs)
            elif order == "old":
                ideas, next_curs, more = model.SlideShow.query(model.SlideShow.userType == "artist", model.SlideShow.inFlight == False, model.SlideShow.approved == True).order(model.SlideShow.created).fetch_page(20, start_cursor=curs)
        elif locality:
            ideas, next_curs, more = model.SlideShow.query(model.SlideShow.userType == "artist", model.SlideShow.inFlight == False, model.SlideShow.approved == True, model.SlideShow.locality == locality).order(-model.SlideShow.created).fetch_page(20, start_cursor=curs)
        else:
            ideas = model.SlideShow.query(model.SlideShow.userType == 'artist', model.SlideShow.inFlight == False, model.SlideShow.approved == True).order(-model.SlideShow.created).fetch(30)

        countries = model.Country.query().order(model.Country.country).fetch()
        localities = model.Locality.query().order(model.Locality.locality).fetch()

        self.render("slideshow/idea_detail_list.html", ideas=ideas, list_title=list_title, inFlight=False, countries=countries, localities=localities)

class InFlight(MainHandler):
    def get(self):
        import urllib2

        country = urllib2.unquote( self.request.get("country") )
        locality = urllib2.unquote( self.request.get("locality") )

        # logging.error("!!!!!!!!!!!!")
        # logging.error(country)

        order = self.request.get("order")

        list_title = "In Flight"

        if country:
            user_slideshows = model.UserSlideShow.query(model.UserSlideShow.approved == True, model.UserSlideShow.landingCountry == country).order(-model.UserSlideShow.created).fetch(30)
        elif order:
            if order == "new":
                user_slideshows = model.UserSlideShow.query(model.UserSlideShow.approved == True).order(-model.UserSlideShow.created).fetch(30)
            elif order == "old":
                user_slideshows = model.UserSlideShow.query(model.UserSlideShow.approved == True).order(model.UserSlideShow.created).fetch(30)
        elif locality:
            user_slideshows = model.UserSlideShow.query(model.UserSlideShow.approved == True, model.UserSlideShow.landingLocality == locality).order(-model.UserSlideShow.created).fetch(30)
        else:
            user_slideshows = model.UserSlideShow.query(model.UserSlideShow.approved == True).order(-model.UserSlideShow.created).fetch(30)
        # user_slideshows = model.UserSlideShow.query().order(-model.UserSlideShow.created).fetch(30)
        ideas = []

        for slideshow in user_slideshows:
            slideShowObj = slideshow.slideShow.get().to_dict()
            slideShowUser = slideshow.user.get()
            slideShowObj["destinationCountry"] = slideShowUser.country
            slideShowObj["destinationLocality"] = slideShowUser.locality
            slideShowObj["landingDate"] = slideshow.landingDate
            slideShowObj["departureDate"] = slideshow.departureDate
            ideas.append(slideShowObj)

        countries = model.Country.query().order(model.Country.country).fetch()
        localities = model.Locality.query().order(model.Locality.locality).fetch()

        self.render("slideshow/idea_detail_list.html", ideas=ideas, list_title=list_title, inFlight=True, countries=countries, localities=localities)

class Flight(MainHandler):
    def get(self, flightCode, slideShow_slug):
        artist = model.User.query(model.User.flightCode == flightCode).get()

        logging.error("...................................")
        logging.error(flightCode)
        logging.error(slideShow_slug)

        slideShow = model.SlideShow.query(model.SlideShow.slug == slideShow_slug, model.SlideShow.flightCode == flightCode).get()
        slides = model.Slide.query(model.Slide.slideShow == slideShow.key).order(model.Slide.slideOrder).fetch()
        bookmarked = False

        flight = model.UserSlideShow.query(model.UserSlideShow.slideShow == slideShow.key).order(-model.UserSlideShow.created).get()

        userObj = self.user
        if userObj:
            savedSlideShow = model.SavedSlideShow.query(model.SavedSlideShow.user == userObj.key, model.SavedSlideShow.slideShow == slideShow.key).get()
            if savedSlideShow:
                bookmarked = True
        if slideShow.approved:
            self.render("slideshow/flight-test.html", artist=artist, slides=slides, slideShow=slideShow, bookmarked=bookmarked, flight=flight)
        else:
            from google.appengine.api import users
            user = users.get_current_user()
            if user and users.is_current_user_admin():
                self.render("slideshow/flight-test.html", artist=artist, slides=slides, slideShow=slideShow, bookmarked=bookmarked, flight=flight)
            else:
                self.redirect("/u/%s" % slideShow.flightCode)

class Exhibition(MainHandler):
    def get(self, flightCode, slideShow_slug):
        gallery = model.User.query(model.User.flightCode == flightCode).get()

        logging.error("...................................")
        logging.error(flightCode)
        logging.error(slideShow_slug)

        userSlideShow = model.UserSlideShow.query(model.UserSlideShow.slug == slideShow_slug, model.SlideShow.user == gallery.key).get()
        slideShow = userSlideShow.slideShow.get()
        artist = slideShow.user.get()
        slides = model.Slide.query(model.Slide.slideShow == slideShow.key).order(model.Slide.slideOrder).fetch()
        bookmarked = False

        flight = userSlideShow

        userObj = self.user
        if userObj:
            savedSlideShow = model.SavedSlideShow.query(model.SavedSlideShow.user == userObj.key, model.SavedSlideShow.slideShow == slideShow.key).get()
            if savedSlideShow:
                bookmarked = True

        self.render("slideshow/flight-test.html", artist=artist, slides=slides, slideShow=slideShow, bookmarked=bookmarked, flight=flight, galleryUser=gallery)


class ToggleInvitation(MainHandler):
    def post(self):
        slideShowID = self.request.get("slideShowID")
        invited = self.request.get("invited")# <= this is a javascript boolean, so it becomes a string == 'true' or == 'false'
        date = self.request.get("date")
        exhibitionEndDate = self.request.get("exhibitionEndDate")

        if not exhibitionEndDate:
            exhibitionEndDate = None

        userObj = self.user

        if userObj:
            if userObj.userType == "airport" or userObj.userType == "freeport" or userObj.userType == "gallery" and userObj.location:

                slideShow = model.SlideShow.get_by_id(int(slideShowID))
                existing = model.UserSlideShow.query(model.UserSlideShow.user == userObj.key, model.UserSlideShow.slideShow == slideShow.key).get()

                if invited == 'true' and not existing:
                    takeOffDate = datetime.datetime.now()
                    landingDate = datetime.datetime.strptime(date, '%Y/%m/%d')

                    if exhibitionEndDate:
                        exhibitionEndDate = datetime.datetime.strptime(exhibitionEndDate, '%Y/%m/%d')

                    UserSlideShow = model.UserSlideShow(
                        user = userObj.key,
                        userType = userObj.userType,
                        userFlightCode = userObj.flightCode,
                        slideShow = slideShow.key,
                        title = slideShow.title,
                        flightCode = slideShow.flightCode,
                        slug = slideShow.slug,
                        flightNumber = slideShow.flightNumber,
                        surname = slideShow.surname,
                        takeOffDate = takeOffDate,
                        landingDate = landingDate,
                        departureDate = exhibitionEndDate,
                        takeOffLocation = slideShow.user.get().location,
                        takeOffCountry = slideShow.country,
                        takeOffLocality = slideShow.locality,
                        location = userObj.location,
                        landingCountry = userObj.country,
                        landingLocality = userObj.locality,
                        geopoint = userObj.geopoint,
                        accepted = True,
                        public = True
                    )
                    UserSlideShow.put()

                    utils.increment_counter("flights")

                    slideShow.inFlight = True
                    slideShow.put()

                    self.render_json({
                        "success": True,
                        "message": "success",
                        "UserSlideShowID": UserSlideShow.key.id()
                    })

                elif invited == 'false':
                    delete_id = existing.key.id()
                    existing.key.delete()
                    logging.info("deleted user < - > slide show")

                    still_in_flight = model.UserSlideShow.query(model.UserSlideShow.slideShow == slideShow.key).get()
                    if not still_in_flight:
                        slideShow.inFlight = False
                        slideShow.put()

                    utils.decrement_counter("flights")

                    self.render_json({
                        "success": True,
                        "message": "success",
                        "UserSlideShowDeleted": delete_id
                    })


            else:
                if not userObj.location:
                    self.render_json({
                        "success": False,
                        "message": 'You need to provide your location, please go to your <a href="/me">settings</a> page.'
                    })
                else:
                    self.redirect("/list/ideas")
        else:
            self.redirect("/login")


class APIGetIdeas(MainHandler):
    def get(self):

        curs = Cursor(urlsafe=self.request.get('slideshow_cursor'))
        ideas, next_curs, more = model.SlideShow.query().order(-model.SlideShow.created).fetch_page(20, start_cursor=curs)
        if more and next_curs:
            slideshow_next_curs = next_curs.urlsafe()
        else:
            slideshow_next_curs = None

        curs = Cursor(urlsafe=self.request.get('user_slideshow_cursor'))
        flights, next_curs, more = model.UserSlideShow.query().order(-model.UserSlideShow.created).fetch_page(20, start_cursor=curs)
        if more and next_curs:
            user_slideshow_next_curs = next_curs.urlsafe()
        else:
            user_slideshow_next_curs = None

        counter = utils.get_counter()

        flights_obj = {}
        #flights_obj["next_curs"] = next_curs
        flights_obj["results"] = []
        flights_obj["landingPermissionsAccepted"] = counter.landingPermissionsAccepted
        flights_obj["ideas"] = counter.ideas

        for flight in flights:
            # if flight.accepted and flight.takeOffDate and flight.landingDate and flight.takeOffLocation and flight.location:
            flight_obj = {}
            lat = None
            lng = None
            if flight.location:
                latTO = float( flight.takeOffLocation.split(",")[0].strip() )
                lngTO = float( flight.takeOffLocation.split(",")[1].strip() )
                latLD = float( flight.location.split(",")[0].strip() )
                lngLD = float( flight.location.split(",")[1].strip() )
            flight_obj["latTO"] = latTO
            flight_obj["lngTO"] = lngTO
            flight_obj["latLD"] = latLD
            flight_obj["lngLD"] = lngLD
            flight_obj["takeOffDate"] = flight.takeOffDate.strftime('%B %d, %Y')
            flight_obj["landingDate"] = flight.landingDate.strftime('%B %d, %Y')

            idea = flight.slideShow.get()
            flight_obj["name"] = idea.title
            flight_obj["number"] = idea.flightCode
            flight_obj["flightNumber"] = idea.flightNumber
            flight_obj["description"] = idea.description
            flight_obj["slug"] = idea.slug

            user = idea.user.get()
            flight_obj["category"] = user.userType
            flight_obj["artist"] = user.surname

            flight_obj["from"] = user.locality
            flight_obj["to"] = flight.user.get().locality

            flights_obj["results"].append(flight_obj)

        for idea in ideas:
            # if idea.public and idea.location and idea.key not in ideas_in_flight_permission:
            flight_obj = {}
            lat = None
            lng = None
            if idea.location:
                latTO = float( idea.location.split(",")[0].strip() )
                lngTO = float( idea.location.split(",")[1].strip() )
            flight_obj["latTO"] = latTO
            flight_obj["lngTO"] = lngTO
            flight_obj["latLD"] = None
            flight_obj["lngLD"] = None
            flight_obj["takeOffDate"] = None
            flight_obj["landingDate"] = None

            flight_obj["name"] = idea.title
            flight_obj["number"] = idea.flightCode
            flight_obj["flightNumber"] = idea.flightNumber
            flight_obj["description"] = idea.description
            flight_obj["slug"] = idea.slug

            user = idea.user.get()
            flight_obj["category"] = user.userType
            flight_obj["artist"] = user.surname

            flight_obj["from"] = user.locality
            flight_obj["to"] = "-"

            flights_obj["results"].append(flight_obj)

        self.render_json(flights_obj)


class Radar(MainHandler):
    def get(self):
        self.render("slideshow/radar.html")


class Page(MainHandler):
    def get(self, page_name):

        content = False
        static_page_name = page_name.lower()
        pages = model.StaticPage.query(model.StaticPage.page_name == static_page_name).fetch()

        if len(pages) > 0:
            content = True

        terminology = """
        <ul class="terminology-ul" style="text-align:center;line-height:200%;">
            <li>
                <span class="terminology-word">"Nonterritorial"</span> is a complex of paths and exhibition zones for ideas to take-off and land.
            </li>
            <li>
                <span class="terminology-word">"idea"</span>a work of art, object, installation, site specific, sonic or performance art.
            </li>
            <li>
                <span class="terminology-word" style="padding-right:3rem;">"an airspace of ideas"</span> a live movement of ideas in an airspace, by which works are exposed
            in multiple locations and contexts. Realtime coordinates of ideas suggests variety of places and exhibition
            formats of engagement between artist and spectator.
            </li>
            <li>
                <span class="terminology-word">"waiting to depart"</span> waiting mode, once idea is prepared and ready to take off.
            </li>
            <li>
                <span class="terminology-word">"landing"</span> arrival mode once idea is scheduled to land at specific destination.
            </li>
            <li>
                <span class="terminology-word">"an airport"</span> an aerodrome with facilities for flights to take off and land.
            </li>
            <li>
                <span class="terminology-word">"freeport"</span> a dedicated area, space, zone or territory to expose ideas through the format of exhibition.
            </li>
        </ul>"""

        airports = """
            <p>Nonterritorial collaborates with multiple airports around the world
            while creating 'departure and landing paths' for ideas to be introduced to unexpected audiences.</p>
        """

        freeports = """
            <p>Nonterritorial collaborates with variety of spaces and initiates new
            contextual places around the world for ideas to be exposed at any scale.<p>
        """

        spaces = "%s %s" % (airports, freeports)

        artists = """
            <p>Artists are welcome to introduce ideas and works for Nonterritorial airspace.<p>
        """

        curators = """
            <p>Curators are welcome to collaborate with Nonterritorial airspace.</p>
        """
        collectors = """
            <p>Collectors can collaborate with Nonterritorial and gain exclusive access to highly curated contemporary art and artists.</p>
        """

        submissions = """
            <p>Artists can join Nonterritorial by <a href="/register" target="_blank">registering</a> and then submitting new art work through the platform.</p>
        """

        about = """
            <p>Every idea has it's origin, departure point, route and destination.
            <p>Nonterritorial engages into a conversation between an active artists and audiences today.
            It's a gateway for a variety of geographies and its artists to the international space, also a
            bridge linking the different art scenes themselves.</p>
            <p>Nonterritorial is an experimental laboratory for artists whose work resists convention and
            transcends boundaries. We expose works rich with poignancy,
             intelligence, and risk.</p>
        """

        logging.error("CONTENT -----------------------")
        logging.error(content)

        self.render("slideshow/page.html",
            page_name=page_name,
            terminology=terminology,
            spaces=spaces,
            artists=artists,
            curators=curators,
            collectors=collectors,
            submissions=submissions,
            about=about,
            pages=pages,
            content=content
            )

class Inquiry(MainHandler):
    def get(self):

        in_flight = model.UserSlideShow.query().order(-model.UserSlideShow.created).fetch(4)
        waiting_to_take_off = model.SlideShow.query(model.SlideShow.userType == 'artist').order(-model.SlideShow.created).fetch(4)

        flightCode = self.request.get("flightCode")
        slug = self.request.get("slug")

        artist = None
        slideShow = None

        if flightCode and slug:
            artist = model.User.query(model.User.flightCode == flightCode).get()
            slideShow = model.SlideShow.query(model.SlideShow.user == artist.key, model.SlideShow.slug == slug).get()

            gallery = None
            if artist.userType == "gallery":
                gallery = artist
                slideShow = model.SlideShow.query(model.SlideShow.flightCode == flightCode, model.SlideShow.slug == slug).get()

        self.render("slideshow/inquiry.html", flightCode=flightCode, slug=slug, artist=artist, slideShow=slideShow, in_flight=in_flight, waiting_to_take_off=waiting_to_take_off, gallery=gallery)

    def post(self):
        name = self.request.get("name")
        email = self.request.get("email")
        message = self.request.get("message")
        slideShowID = self.request.get("slideShowID")

        slideshow_url = ""

        try:
            slideShow = model.SlideShow.get_by_id(int(slideShowID))
            slideshow_url = "%s/%s" % (slideShow.flightCode, slideShow.slug)
        except:
            logging.error("couldn't get slideshow by ID, for an inquiry")

        inquiry = model.Inquiry(name=name, email=email, message=message, slideShowID=slideShowID, slideshow_url=slideshow_url)
        inquiry.put()

        self.render_json({
            "message": "success"
            })

class Contact(MainHandler):
    def get(self):

        in_flight = model.UserSlideShow.query().order(-model.UserSlideShow.created).fetch(4)
        waiting_to_take_off = model.SlideShow.query(model.SlideShow.userType == 'artist').order(-model.SlideShow.created).fetch(4)

        flightCode = self.request.get("flightCode")
        slug = self.request.get("slug")

        artist = None
        slideShow = None

        if flightCode and slug:
            artist = model.User.query(model.User.flightCode == flightCode).get()
            slideShow = model.SlideShow.query(model.SlideShow.user == artist.key, model.SlideShow.slug == slug).get()

        self.render("slideshow/contact.html", flightCode=flightCode, slug=slug, artist=artist, slideShow=slideShow, in_flight=in_flight, waiting_to_take_off=waiting_to_take_off)

    def post(self):
        name = self.request.get("name")
        email = self.request.get("email")
        message = self.request.get("message")
        slideShowID = self.request.get("slideShowID")

        inquiry = model.Inquiry(name=name, email=email, message=message, slideShowID=slideShowID)
        inquiry.put()

        self.render_json({
            "message": "success"
            })

class Search(MainHandler):
    def get(self):
        results = {}
        q = self.request.get("q")
        initial = True

        import search
        if len(q) > 0:
            initial = False
            search_results = search.textSearch(q, "slideShowIndex", offsetval=0)# for standard moments
            results["number_found"] = search_results["number_found"]
            results["results"] = []
            slideShow_ids = []
            for result in search_results["results"]:
                logging.info(result)
                slide = model.Slide.get_by_id( int(result["id"]))
                logging.info(slide)
                if slide:
                    logging.info(slideShow_ids)
                    if slide.slideShow.id() not in slideShow_ids:
                        slideShow = slide.slideShow.get()
                        logging.info(slideShow)
                        if slideShow.public:
                            logging.info(" . . . . .  here")
                            slideShow_ids.append(slideShow.key.id())
                            results["results"].append(slideShow.to_dict())

            sorted_list = sorted(results["results"], key=lambda k: k['created'], reverse=True)
            results["results"] = sorted_list
            results["number_found"] = len(results["results"])
            logging.info(" . . . . . . .  search results")
            logging.info(results)

        self.render("slideshow/search.html", results=results, q=q, initial=initial)

class Exhibitions(MainHandler):
    def get(self):

        curs = Cursor(urlsafe=self.request.get('slideshow_cursor'))
        exhibitions, next_curs, more = model.UserSlideShow.query(model.UserSlideShow.approved == True).order(-model.SlideShow.landingDate).fetch_page(20, start_cursor=curs)
        if more and next_curs:
            next_curs = next_curs.urlsafe()
        else:
            next_curs = None

        self.render("slideshow/exhibitions.html", exhibitions=exhibitions, next_curs=next_curs)

# class Map(MainHandler):
#     def get(self):
#         self.render("map/map.html")

class Unity(MainHandler):
    def get(self):
        self.render("unity/integrated-unity.html")

class IndexSlides(MainHandler):
    def get(self):
        slides = model.Slide.query().fetch()
        import search
        for slide in slides:
            search.indexSlide(slide)

class Explainer(MainHandler):
    def get(self):

        self.render("slideshow/explainer.html")

class EditLocations(MainHandler):
    def get(self):
        countries = model.Country.query().fetch()
        localities = model.Locality.query().fetch()
        users = model.User.query().fetch()
        slideshows = model.SlideShow.query().fetch()
        userslideshows = model.UserSlideShow.query().fetch()

        for country in countries:
            if location_dictionary.has_key(country.country):
                country.country = location_dictionary[country.country]
                country.put()

        for locality in localities:
            if location_dictionary.has_key(locality.locality):
                locality.locality = location_dictionary[locality.locality]
                locality.put()

        for user in users:
            if location_dictionary.has_key(user.country):
                user.country = location_dictionary[user.country]
                user.put()
            if location_dictionary.has_key(user.locality):
                user.locality = location_dictionary[user.locality]
                user.put()

        for slideshow in slideshows:
            if location_dictionary.has_key(slideshow.country):
                slideshow.country = location_dictionary[slideshow.country]
                slideshow.put()
            if location_dictionary.has_key(slideshow.locality):
                slideshow.locality = location_dictionary[slideshow.locality]
                slideshow.put()

        for userslideshow in userslideshows:
            if location_dictionary.has_key(userslideshow.landingCountry):
                userslideshow.landingCountry = location_dictionary[userslideshow.landingCountry]
                userslideshow.put()
            if location_dictionary.has_key(userslideshow.landingLocality):
                userslideshow.landingLocality = location_dictionary[userslideshow.landingLocality]
                userslideshow.put()

            if location_dictionary.has_key(userslideshow.takeOffCountry):
                userslideshow.takeOffCountry = location_dictionary[userslideshow.takeOffCountry]
                userslideshow.put()
            if location_dictionary.has_key(userslideshow.takeOffLocality):
                userslideshow.takeOffLocality = location_dictionary[userslideshow.takeOffLocality]
                userslideshow.put()

        self.response.out.write("done.")

class PopulateLocations(MainHandler):
    def get(self):
        users = model.User.query().fetch()

        for user in users:
            user_country = user.country
            user_locality = user.locality

            if user_country:
                country = model.Country.query(model.Country.country == user_country).get()
                if not country:
                    country = model.Country(country=user_country, location=user.location, geopoint=user.geopoint)
                    country.put()

            if user_locality:
                locality = model.Locality.query(model.Locality.locality == user_locality).get()
                if not locality:
                    locality = model.Locality(locality=user_locality, location=user.location, geopoint=user.geopoint)
                    locality.put()


app = webapp2.WSGIApplication([
    ('/', Home),
    ('/u/([-\w]+)', UserPage),
    ('/demo', Demo),

    # gallery
    ('/galleries', Galleries),
    ('/artists', Artists),
    ('/gallery/([-\w]+)', GalleryPageList),
    ('/gallery/list/([-\w]+)', GalleryPageList),
    ('/gallery/map/([-\w]+)', GalleryPageMap),
    ('/gallery/([-\w]+)/contact', GalleryContact),

    ('/me', Dashboard),
    ('/me/save-user', SaveUser),
    ('/me/profile-image-upload', ProfileImageUpload),
    ('/me/my-videos', UserVideos),
    ('/me/upload-video', UserVideoUpload),
    ('/me/new', NewSlideShow),
    ('/me/exhibition/new', NewExhibitionSlideShow),
    ('/me/airport/new', NewSlideShow),
    ('/me/idea/edit/(\w+)', EditSlideShow),
    ('/me/exhibition/edit/(\w+)', EditExhibitionDetails),
    ('/me/idea/audio/(\w+)', AudioSlideShow),
    ('/me/slide-order', SlideReorder),
    ('/me/delete-slide', SlideDelete),
    ('/me/save-slide', SaveSlide),
    ('/me/slideShow/delete/(\w+)', SlideShowDelete),
    ('/me/change-email', userHandlers.ChangeEmail),
    ('/me/change-password', userHandlers.ChangePassword),

    ('/list/ideas', ListIdeas),
    ('/list/waiting-to-takeoff', WaitingToTakeoff),
    ('/list/in-flight', InFlight),

    ('/idea/([-\w]+)/([-\w]+)', Flight),
    ('/exhibition/([-\w]+)/([-\w]+)', Exhibition),
    ('/toggle_invitation', ToggleInvitation),

    ('/api/getIdeas', APIGetIdeas),
    ('/api/userVideos', APIUserVideos),
    ('/api/ports/topojson', APIPortsTopoJson),
    ('/api/gallery/([-\w]+)/topojson', APIGalleryTopoJson),
    ('/api/save-slideshow', APISaveSlideShow),

    ('/radar', Radar),


    ('/page/(\w+)', Page),
    ('/inquiry', Inquiry),
    ('/contact', Contact),
    ('/search', Search),
    ('/exhibitions', Exhibitions),

    ('/admin', userHandlers.Admin),
    ('/admin/users', userHandlers.AdminUsers),
    ('/admin/approve-user', userHandlers.AdminApproveUser),
    ('/admin/unapprove-user', userHandlers.AdminUnApproveUser),
    ('/admin/upgrade-user', userHandlers.AdminUpgradeUser),
    ('/admin/degrade-user', userHandlers.AdminDegradeUser),
    ('/admin/make-sonic', userHandlers.AdminMakeSonic),
    ('/admin/unmake-sonic', userHandlers.AdminUnMakeSonic),
    ('/admin/ideas', userHandlers.AdminIdeas),
    ('/admin/approve-idea', userHandlers.AdminApproveIdea),
    ('/admin/unapprove-idea', userHandlers.AdminUnApproveIdea),
    ('/admin/approve-flight', userHandlers.AdminApproveFlight),
    ('/admin/unapprove-flight', userHandlers.AdminUnApproveFlight),
    ('/admin/flights', userHandlers.AdminFlights),
    ('/admin/messages', userHandlers.AdminMessages),
    ('/admin/message/delete/(\w+)', userHandlers.AdminDeleteMessage),
    ('/admin/delete-user/(\w+)', userHandlers.AdminDeleteUser),

    ('/concept', Unity),




    # ----old


    # Users
    ('/login', userHandlers.Login),
    ('/logout', userHandlers.Logout),
    ('/register', userHandlers.Register),
    ('/forgot-password', userHandlers.ForgotPassword),
    ('/user/saveCode', userHandlers.SaveCode),
    ('/user/application', userHandlers.UserApplication),

    # users
    # ('/me', userHandlers.UserPage),
    ('/me/saveSettings', userHandlers.UserSettings),
    ('/me/change-username', userHandlers.ChangeUsername),
    # ('/me/change-email', userHandlers.ChangeEmail),
    # ('/me/change-password', userHandlers.ChangePassword),
    ('/me/ideas', userHandlers.UserIdeas),
    ('/me/new-idea', userHandlers.UserIdea),
    # ('/me/idea/edit/([-\w]+)', userHandlers.UserIdeaEdit),
    ('/user/([-\w]+)', userHandlers.PublicUserPage),
    ('/me/landing-permissions', userHandlers.UserLandingPermissions),
    ('/me/landing-permissions-respond/([-\w]+)', userHandlers.UserLandingPermissionRespond),
    ('/me/landing-permissions/offered', userHandlers.UserLandingPermissionOffered),
    ('/me/make-idea-public', userHandlers.MakeIdeaPublic),
    ('/me/idea/video-upload', IdeaVideoUpload),

    # curators, ideas, artists
    ('/curators', userHandlers.Curators),
    #('/ideas', userHandlers.Ideas),
    ('/artists', userHandlers.Artists),

    # Ideas
    ('/api/ideas', APIIdeas),
    ('/api/flights', APIFlights),
    ('/api/publicIdeas', APIPublicIdeas),
    ('/ideas', ideaHandlers.Ideas),
    ('/idea/([-\w]+)', ideaHandlers.Idea),

    # Landing Permission etc.
    ('/offerLandingPermission', OfferLandingPermission),

    # Admin
    # ('/admin', userHandlers.Admin),
    # ('/admin/users', userHandlers.AdminUsers),


    ('/admin/applications', userHandlers.AdminApplications),
    ('/admin/globalCurator', userHandlers.AdminGlobalCurator),
    # ('/admin/ideas', ideaHandlers.AdminIdeas),
    ('/admin/save-tags', userHandlers.AdminSaveTags),
    ('/admin/remove-tag', userHandlers.AdminRemoveTag),
    ('/admin/static-pages', userHandlers.AdminStaticPage),
    ('/admin/static-page/(\w+)', userHandlers.AdminStaticPageEdit),

    # Files
    ('/file/upload', fileHandlers.FileUpload),
    # ('/fileuploaded', FileUploaded),
    ('/fileuploaded', FileUploaded2),

    # --- testing
    ('/explainer', Explainer),
    ('/prototype', Map),
    ('/unity', Unity),
    ('/add-video/(\w+)', AddVideo),
    ('/data-viz', DataViz),
    ('/query', Query),
    ('/locationQuery', LocationQuery),
    #('/upload', fileHandlers.Upload),
    ('/placeholder', Placeholder),
    ('/testVimeo', TestVimeo),
    ('/indexslides', IndexSlides),


    ('/edit-locations', EditLocations),
    ('/populate_locations', PopulateLocations)
], debug=False)
