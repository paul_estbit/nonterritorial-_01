from main import MainHandler

from google.appengine.datastore.datastore_query import Cursor
from datetime import datetime, timedelta
import math

import logging

from google.appengine.ext import ndb

import utils
import model
import slugify
import jinjaFilters

class Register(MainHandler):
    def get(self):
        user_obj = self.user
        register_type = self.request.get("register_type")
        if user_obj:
            self.redirect("/me")
        else:
            data = None
            self.render("slideshow/register-radio.html", register_type=register_type)

    def post(self):
        username = self.request.get('username')
        email = self.request.get('email')
        password = self.request.get('password')
        verify_password = self.request.get('verify_password')
        userType = self.request.get("userType")
        
        # For artists
        name = self.request.get("name")
        surname = self.request.get("surname")
        portfolio = self.request.get("portfolio")

        slug = slugify.slugify(username)
        
        error = False
        errors = []
        error_name = ""
        error_password = ""
        error_email = ""
        error_verify = ""
        
        # if not utils.valid_username(username):
        #     error_name="Your username can only contain alphanumeric characters and needs to be at least 3 characters long."
        #     errors.append( error_name )
        #     error = True
        if not utils.unique_username(username):
            error_name="Sorry, name's been taken"
            errors.append( error_name )
            error = True
            
        if not utils.valid_password(password):
            error_password="Your password needs to be between 3 and 20 characters long"
            errors.append( error_password )
            error = True
            
        if not utils.valid_email(email):
            error_email="Please type in a valid email address"
            errors.append( error_email )
            error = True
        if not utils.unique_email(email):
            error_name="That email's already in use"
            errors.append( error_name )
            error = True
            
        if password != verify_password:
            error_verify="Please ensure your passwords match"
            errors.append( error_verify )
            error = True
        
        if not error:
            pw_hash = utils.make_pw_hash(username, password)
            if userType == "airport" or userType == "freeport" or userType == "gallery":
                isGallery = False
                if userType == "gallery":
                    isGallery = True
                user = model.User(username=username, email=email, pw_hash=pw_hash, slug=slug, userType=userType, portfolio=portfolio, name=name, surname=surname, flightCode=slug, isGallery=isGallery)
            elif userType == "casual":
                user = model.User(username=username, email=email, pw_hash=pw_hash, slug=slug, userType=userType, portfolio=portfolio, name=name, surname=surname, approved=True)
            else:
                user = model.User(username=username, email=email, pw_hash=pw_hash, slug=slug, userType=userType, portfolio=portfolio, name=name, surname=surname,)
            user.put()
            utils.increment_counter('allUsers')

            if user and user.userType== "artist" and user.name and user.surname:
                flightCode = utils.get_flight_code(user)
                user.flightCode = flightCode
                user.put()


            self.login(user)
            self.redirect('/me')
        else:
            data = {
                'username': username,
                'email': email,
                'portfolio': portfolio,
                'userType': userType,
                'errors':errors,
            }

            self.render('slideshow/register-radio.html', **data)
            
class LoginRegister(MainHandler):
    def get(self):
        data = None
        self.render("login.html", data=data)
        
class Login(MainHandler):
    def get(self):
        self.render("slideshow/login.html")
    def post(self):
        username = self.request.get('username')
        password = self.request.get('password')

        s = model.User.login(username, password)



        if s:
            self.login(s)
            self.redirect('/me')
        else:
            self.render("slideshow/login.html", error="Incorrect username/password.")
        
class Logout(MainHandler):
    def get(self):
        self.logout()
        self.redirect('/')

class ForgotPassword(MainHandler):
    def get(self):
        self.render("slideshow/forgot_password.html")
    def post(self):
        email = self.request.get("email")

        logging.info("email: %s" % email)

        user_email = model.User.query(model.User.email == email).get()
        user_username = model.User.query(model.User.username == email).get()

        if user_email or user_username:
            if user_email:
                user = user_email
            elif user_username:
                user = user_username

            new_pw = utils.generate_random_token(5)
            logging.error(" . . . new PW . . . %s" % new_pw)
            new_pw_hash = utils.make_pw_hash(user.username, new_pw)
            user.pw_hash = new_pw_hash
            user.put()

            utils.render_and_send_email_template("forgot_password", user.email, user.name, new_pw)

            self.render_json({
                "email": email,
                "invalidEmail": False,
                "message": "A temporary Password has been sent to %s" % user.email
            })
        else:
            self.render_json({
                "email": email,
                "invalidEmail": True,
                "message": "Sorry there is no user with the email address %s" % email
            })

class ChangeEmail(MainHandler):
    def post(self):
        email = self.request.get("email")
        password = self.request.get("password")

        logging.error("email ............................ %s" % email)

        userObj = self.user
        if userObj:
            errors = utils.changeEmail(email, userObj.username, password, userObj)
            if len(errors) <= 0:
                message = "success"
            else:
                message = "fail"
        else:
            errors = []
            message = "fail"
            errors.append("You're not logged in.")

        self.render_json({
            "message": message,
            "errors": errors
            })

class ChangePassword(MainHandler):
    def post(self):
        new_password = self.request.get("new_password")
        password = self.request.get("password")

        logging.error("..........password: %s" % password)

        userObj = self.user
        if userObj:
            errors = utils.changePassword(new_password, userObj.username, password, userObj)
            if len(errors) <= 0:
                message = "success"
            else:
                message = "fail"
        else:
            errors = []
            message = "fail"
            errors.append("You're not logged in.")

        self.render_json({
            "message": message,
            "errors": errors
            })


class UserPage(MainHandler):
    def get(self):
        if self.user:
            self.render("users/user-page.html")
        else:
            self.redirect('/login')

class UserSettings(MainHandler):
    def post(self):
        name = self.request.get("name")
        about = self.request.get("about")
        ideaLocation = self.request.get("ideaLocation")
        location = utils.stringLocationFromJson(ideaLocation)
        address = self.request.get("address")
        country = self.request.get("country")
        locality = self.request.get("locality")

        userObj = self.user

        errors = []

        if userObj:
            userObj.name = name
            userObj.about = about
            userObj.location = location
            userObj.geopoint = utils.stringToGeoPt(location)
            userObj.address = address
            userObj.country = country
            userObj.locality = locality
            userObj.put()

            self.render_json({
                "message": "success",    
            })
        else:
            errors.append("You're not logged in.")
            self.render_json({
                "message": "fail",    
            })

class ChangeUsername(MainHandler):
    def post(self):
        username = self.request.get("username")
        password = self.request.get("password")

        userObj = self.user
        if userObj:
            errors = utils.changeUsername(username, userObj.username, password, userObj)
            if len(errors) <= 0:
                message = "success"
            else:
                message = "fail"
        else:
            errors = []
            message = "fail"
            errors.append("You're not logged in.")

        self.render_json({
            "message": message,
            "errors": errors
            })

# class ChangeEmail(MainHandler):
#     def post(self):
#         email = self.request.get("email")
#         password = self.request.get("password")

#         userObj = self.user
#         if userObj:
#             errors = utils.changeEmail(email, userObj.username, password, userObj)
#             if len(errors) <= 0:
#                 message = "success"
#             else:
#                 message = "fail"
#         else:
#             errors = []
#             message = "fail"
#             errors.append("You're not logged in.")

#         self.render_json({
#             "message": message,
#             "errors": errors
#             })

# class ChangePassword(MainHandler):
#     def post(self):
#         new_password = self.request.get("new_password")
#         password = self.request.get("password")

#         logging.error("..........password: %s" % password)

#         userObj = self.user
#         if userObj:
#             errors = utils.changePassword(new_password, userObj.username, password, userObj)
#             if len(errors) <= 0:
#                 message = "success"
#             else:
#                 message = "fail"
#         else:
#             errors = []
#             message = "fail"
#             errors.append("You're not logged in.")

#         self.render_json({
#             "message": message,
#             "errors": errors
#             })






class UserIdeas(MainHandler):
    def get(self):
        userObj = self.user
        if userObj:
            curs = Cursor(urlsafe=self.request.get('cursor'))
            ideas, next_curs, more = model.Idea.query(model.Idea.artist == userObj.key).order(-model.Idea.created).fetch_page(20, start_cursor=curs)
            if more and next_curs:
                next_curs = next_curs.urlsafe()
            else:
                next_curs = None

        self.render("users/user-ideas.html", ideas=ideas, next_curs=next_curs)

class UserIdea(MainHandler):
    def get(self):
        userObj = self.user
        if userObj and userObj.userType == "artist" or userObj.userType == "curator":
            curs = Cursor(urlsafe=self.request.get('cursor'))
            ideas, next_curs, more = model.Idea.query(model.Idea.artist == userObj.key).order(-model.Idea.created).fetch_page(20, start_cursor=curs)
            if more and next_curs:
                next_curs = next_curs.urlsafe()
            else:
                next_curs = None

            self.render_auth("users/user-idea.html", ideas=ideas, next_curs=next_curs)
        else:
            self.redirect("/me")

    def post(self):
        idea_dict = utils.save_idea(self)
        idea = idea_dict["idea"]
        errors = idea_dict["errors"]
        deleted_file_ids = idea_dict["deleted_file_ids"]
        if idea:
            self.render_json({
                "message": "success",
                "ideaID": idea.key.id(),
                "ideaName": idea.name,
                "ideaDescription": idea.description,
                "ideaSlug": idea.slug,
                "deleted_file_ids": deleted_file_ids,
                "errors": errors
            })
        else:
            self.render_json({
                "message": "fail",
                "deleted_file_ids": deleted_file_ids,
                "errors": errors
            })

class UserIdeaEdit(MainHandler):
    def get(self, idea_slug):
        userObj = self.user
        if userObj and userObj.userType == "artist" or userObj.userType == "curator":
            idea = model.Idea.query(model.Idea.slug == idea_slug).get()
            self.render_auth("users/user-idea-edit.html", idea=idea)
        else:
            self.redirect("/me")

    def post(self, idea_slug):
        idea_dict = utils.save_idea(self)
        idea = idea_dict["idea"]
        errors = idea_dict["errors"]
        if idea:
            self.render_json({
                "message": "success",
                "ideaID": idea.key.id(),
                "ideaName": idea.name,
                "ideaDescription": idea.description,
                "ideaSlug": idea.slug,
                "errors": errors
            })
        else:
            self.render_json({
                "message": "fail",
                "errors": errors
            })
        self.redirect("/me/idea/edit/%s" % idea_slug)

class PublicUserPage(MainHandler):
    def get(self, username):
        userProfile = model.User.query(model.User.slug == username).get()
        self.render("users/user-page-public.html", userProfile=userProfile)

class UserLandingPermissions(MainHandler):
    def get(self):
        userObj = self.user
        if userObj:
            landingPermissions = model.LandingPermission.query(model.LandingPermission.artist == userObj.key)
            self.render("users/user-landing-permission.html", landingPermissions=landingPermissions)
        else:
            self.redirect("/login")

class UserLandingPermissionRespond(MainHandler):
    def post(self, landingPermissionID):
        userObj = self.user
        accepted = self.request.get("accept")
        rejected = self.request.get("reject")
        errors = []

        import search

        if userObj:
            landingPermission = model.LandingPermission.get_by_id(int(landingPermissionID))
            
            if accepted:
                
                landingPermission.accepted = True
                landingPermission.public = True
                landingPermission.takeOffDate = datetime.now()
                #landingPermission.duration = int( math.floor( (datetime.now()-landingPermission.created).total_seconds() ) )
                landingPermission.takeOffLocation = landingPermission.idea.get().location
                landingPermission.put()
                
                search.indexFlight(landingPermission)# index the landing permission for geo-search purposes
                
                utils.increment_counter("landingPermissionsAccepted")
                utils.notify_user("landing-permission-accepted", landingPermission.curator.get())
            
            if rejected:
                
                if landingPermission.accepted:
                    search.removeFromIndex(landingPermission.key.id(), 'flightIndex')
                    utils.decrement_counter("landingPermissionsAccepted")
                
                landingPermission.accepted = False
                landingPermission.put()
                utils.notify_user("landing-permission-accepted", landingPermission.curator.get())

            self.render_json({
                "message": "success",
                "accepted": landingPermission.accepted,
                "landingPermissionID": landingPermission.key.id(),
                "errors": errors
                })
        else:
            errors.append("You're not logged in.")
            self.render_json({
                "message": "fail",
                "accepted": None,
                "errors": errors
                })

class UserLandingPermissionOffered(MainHandler):
    def get(self):
        userObj = self.user
        if userObj:
            landingPermissions = model.LandingPermission.query(model.LandingPermission.curator == userObj.key)
            self.render("users/curator-landing-permissions.html", landingPermissions=landingPermissions)
        else:
            self.redirect("/login")

class MakeIdeaPublic(MainHandler):
    def post(self):
        userObj = self.user
        errors = []

        if userObj:
            ideaID = self.request.get("ideaID")
            public = self.request.get("public")
            if public == "yes":
                public = True
            elif public == "no":
                public = False
            idea = model.Idea.get_by_id(int(ideaID))
            idea.public = public
            idea.put()
        else:
            errors.append("You're not logged in.")
            self.render_json({
                "message": "fail",
                "errors": errors
                })










class SaveCode(MainHandler):
    def post(self):
        ideaCode = self.request.get('ideaCode')
        ideaCode_dict = utils.checkIdeaCode(ideaCode)
        ideaCode = ideaCode_dict["code"]
        errors = ideaCode_dict["errors"]

        if self.user and ideaCode:
            code = ideaCode

            if utils.existingEntity("User", "flightCode", ideaCode):
                message = "fail"
                long_message = "The flight code %s has already been taken." % code
                errors.append("The flight code %s has already been taken." % code)
            else:
                message = "success"
                long_message = "Your New Code is: %s" % code
                userObj = self.user
                userObj.flightCode = code
                userObj.put()
        else:
            message = "fail"
            long_message = "There was a problem with the code, %s, please try a new code." % ideaCode
            code = ideaCode

        self.render_json({
            "message": message,
            "long_message": long_message,
            "code": code,
            "errors": errors
            })

class UserApplication(MainHandler):
    def post(self):
        applicationType = self.request.get("applicationType")

        userObj = self.user
        if userObj:
            application = model.Application()
            application.application = applicationType
            application.username = userObj.username
            application.email = userObj.email
            application.userType = userObj.userType
            application.user = userObj.key
            application.put()
            userObj.applicationProgress = True
            userObj.put()
            utils.increment_counter('applications')
        else:
            self.redirect("/login")

# -----------------------------------------------------------------------------
# Curators, Ideas, Artists
# -----------------------------------------------------------------------------

class Curators(MainHandler):
    def get(self):
        curs = Cursor(urlsafe=self.request.get('cursor'))
        curators, next_curs, more = model.User.query(model.User.userType == "curator").order(-model.User.created).fetch_page(20, start_cursor=curs)
        if more and next_curs:
            next_curs = next_curs.urlsafe()
        else:
            next_curs = None

        self.render("users/curators.html", curators=curators, next_curs=next_curs)

class Artists(MainHandler):
    def get(self):
        curs = Cursor(urlsafe=self.request.get('cursor'))
        artists, next_curs, more = model.User.query( ndb.OR( model.User.userType == "artist", model.User.userType == "curator" ) ).order(-model.User._key, model.User.name).fetch_page(20, start_cursor=curs)
        if more and next_curs:
            next_curs = next_curs.urlsafe()
        else:
            next_curs = None

        self.render("users/artists.html", artists=artists, next_curs=next_curs)


# -----------------------------------------------------------------------------
# Admin
# -----------------------------------------------------------------------------

class Admin(MainHandler):
    def get(self):
        counter = utils.get_counter()
        tags = model.Tag.query().fetch()
        self.render("users/admin.html", counter=counter, tags=tags)

class AdminUsers(MainHandler):
    def get(self):

        filterType = self.request.get("filterType")
        filterSearch = self.request.get("filterSearch")

        counter = utils.get_counter()

        curs = Cursor(urlsafe=self.request.get('cursor'))
        if filterType:
            users, next_curs, more = model.User.query(model.User.userType == filterType).order(-model.User.created).fetch_page(20, start_cursor=curs)
        elif filterSearch:
            users, next_curs, more = model.User.query(
                ndb.OR(model.User.username == filterSearch,
                           model.User.email == filterSearch,
                           model.User.name == filterSearch)
                ).order(model.User._key).fetch_page(20, start_cursor=curs)
        else:
            users, next_curs, more = model.User.query().order(-model.User.created).fetch_page(20, start_cursor=curs)

        if more and next_curs:
            next_curs = next_curs.urlsafe()
        else:
            next_curs = None

        self.render("users/admin-users.html", counter=counter, users=users, next_curs=next_curs, filterType=filterType, filterSearch=filterSearch)

class AdminApproveUser(MainHandler):
    def post(self):
        userID = self.request.get("userID")
        approvedUser = model.User.get_by_id(int(userID))
        if approvedUser and approvedUser.location:
            approvedUser.approved = True
            approvedUser.put()

            #  No longer automatically approving all slideshows and flights once a user is approved
            # userSlideShows = model.UserSlideShow.query(model.UserSlideShow.user == approvedUser.key).fetch()
            # for us in userSlideShows:
            #     us.approved = True
            #     us.put()

            # slideShows = model.SlideShow.query(model.SlideShow.user == approvedUser.key).fetch()
            # for us in slideShows:
            #     us.approved = True
            #     us.put()

            utils.send_approve_user_mail(approvedUser)

            self.render_json({
                "success": True,
                "message": "success",
                "userID": approvedUser.key.id()
                })
        else:
            self.render_json({
                "success": False,
                "message": "no user location",
                "userID": approvedUser.key.id()
                })

class AdminUnApproveUser(MainHandler):
    def post(self):
        userID = self.request.get("userID")
        approvedUser = model.User.get_by_id(int(userID))
        approvedUser.approved = False
        approvedUser.put()

        userSlideShows = model.UserSlideShow.query(model.UserSlideShow.user == approvedUser.key).fetch()
        for us in userSlideShows:
            us.approved = False
            us.put()

        slideShows = model.SlideShow.query(model.SlideShow.user == approvedUser.key).fetch()
        for us in slideShows:
            us.approved = False
            us.put()

        self.render_json({
            "message": "success",
            "userID": approvedUser.key.id()
            })

class AdminUpgradeUser(MainHandler):
    def post(self):
        userID = self.request.get("userID")
        approvedUser = model.User.get_by_id(int(userID))
        if approvedUser and approvedUser.userType == "artist":
            approvedUser.userType = "gallery"
            approvedUser.isGallery = False
            approvedUser.upgraded = True
            approvedUser.put()

            self.render_json({
                "success": True,
                "message": "success",
                "userID": approvedUser.key.id()
                })
        else:
            self.render_json({
                "success": False,
                "message": "no user location",
                "userID": approvedUser.key.id()
                })

class AdminDegradeUser(MainHandler):
    def post(self):
        userID = self.request.get("userID")
        approvedUser = model.User.get_by_id(int(userID))
        approvedUser.upgraded = False
        approvedUser.userType = "artist"
        approvedUser.put()

        self.render_json({
            "message": "success",
            "userID": approvedUser.key.id()
            })

class AdminMakeSonic(MainHandler):
    def post(self):
        userID = self.request.get("userID")
        approvedUser = model.User.get_by_id(int(userID))
        if approvedUser:
            approvedUser.isSonic = True
            approvedUser.put()

            self.render_json({
                "success": True,
                "message": "success",
                "userID": approvedUser.key.id()
                })
        else:
            self.render_json({
                "success": False,
                "message": "no user location",
                "userID": approvedUser.key.id()
                })

class AdminUnMakeSonic(MainHandler):
    def post(self):
        userID = self.request.get("userID")
        approvedUser = model.User.get_by_id(int(userID))
        approvedUser.isSonic = False
        approvedUser.put()

        self.render_json({
            "message": "success",
            "userID": approvedUser.key.id()
            })

class AdminIdeas(MainHandler):
    def get(self):
        curs = utils.getCursor(self)
        ideas, next_curs, more = model.SlideShow.query().order(-model.SlideShow.created).fetch_page(20, start_cursor=curs)
        if more and next_curs:
            next_curs = next_curs.urlsafe()
        else:
            next_curs = None

        self.render("users/admin-ideas.html", ideas=ideas, next_curs=next_curs)


class AdminApproveIdea(MainHandler):
    def post(self):
        ideaID = self.request.get("ideaID")
        approvedIdea = model.SlideShow.get_by_id(int(ideaID))
        approvedIdea.approved = True
        approvedIdea.put()

        # approve associated userSlideShows
        userSlideShows = model.UserSlideShow.query(model.UserSlideShow.slideShow == approvedIdea.key).fetch()
        for us in userSlideShows:
            us.approved = True
            us.put()

        self.render_json({
            "message": "success",
            "ideaID": approvedIdea.key.id()
            })

class AdminUnApproveIdea(MainHandler):
    def post(self):
        ideaID = self.request.get("ideaID")
        approvedIdea = model.SlideShow.get_by_id(int(ideaID))
        approvedIdea.approved = False
        approvedIdea.put()

        # approve associated userSlideShows
        userSlideShows = model.UserSlideShow.query(model.UserSlideShow.slideShow == approvedIdea.key).fetch()
        for us in userSlideShows:
            us.approved = False
            us.put()

        self.render_json({
            "message": "success",
            "userID": approvedIdea.key.id()
            })

class AdminApproveFlight(MainHandler):
    def post(self):
        flightID = self.request.get("flightID")
        approvedFlight = model.UserSlideShow.get_by_id(int(flightID))
        approvedFlight.approved = True
        approvedFlight.put()

        self.render_json({
            "message": "success",
            "flightID": approvedFlight.key.id()
            })

class AdminUnApproveFlight(MainHandler):
    def post(self):
        flightID = self.request.get("flightID")
        approvedFlight = model.UserSlideShow.get_by_id(int(flightID))
        approvedFlight.approved = False
        approvedFlight.put()

        self.render_json({
            "message": "success",
            "flightID": approvedFlight.key.id()
            })

class AdminFlights(MainHandler):
    def get(self):
        curs = utils.getCursor(self)
        flights, next_curs, more = model.UserSlideShow.query().order(-model.UserSlideShow.created).fetch_page(20, start_cursor=curs)
        if more and next_curs:
            next_curs = next_curs.urlsafe()
        else:
            next_curs = None

        self.render("users/admin-flights.html", flights=flights, next_curs=next_curs)

class AdminMessages(MainHandler):
    def get(self):
        curs = utils.getCursor(self)
        messages, next_curs, more = model.Inquiry.query().order(-model.Inquiry.created).fetch_page(20, start_cursor=curs)
        if more and next_curs:
            next_curs = next_curs.urlsafe()
        else:
            next_curs = None

        self.render("users/admin-messages.html", messages=messages, next_curs=next_curs)

class AdminDeleteMessage(MainHandler):
    def post(self, messageID):
        message = model.Inquiry.get_by_id(int(messageID))
        if message:
            message.key.delete()

        self.render_json({
          "success": True,
          "message": "message no longer exists/deleted",
          "messageID": messageID  
        })

class AdminDeleteUser(MainHandler):
    def post(self, userID):

        user_obj = model.User.get_by_id(int(userID))

        if user_obj:

            slides = model.Slide.query(model.Slide.user == user_obj.key).fetch()
            for slide in slides:
                if slide.file_obj:
                    fileObj = slide.file_obj.get()
                    import fileHandlers
                    if slide.image_url:
                        fileHandlers.delete_from_gcs(fileObj.gcs_filename, True)
                    else:
                        fileHandlers.delete_from_gcs(fileObj.gcs_filename, False)
                slide.key.delete()

            savedSlideShows = model.SavedSlideShow.query(model.SavedSlideShow.user == user_obj.key).fetch()
            for savedSlideShow in savedSlideShows:
                savedSlideShow.key.delete()

            userSlideShows = model.UserSlideShow.query(model.UserSlideShow.user == user_obj.key).fetch()
            for userSlideShow  in userSlideShows:
                slideshow.key.delete()

            files = model.File.query(model.File.user == user_obj.key).fetch()
            for file in files:
                file.key.delete()

            slideshows = model.SlideShow.query(model.SlideShow.user == user_obj.key).fetch()
            for slideshow in slideshows:
                # delete all flights associated with slideshow
                slideshowflights = model.UserSlideShow.query(model.UserSlideShow.slideShow == slideshow.key).fetch()
                for slideshowflight  in slideshowflights:
                    slideshowflight.key.delete()

                slideshow.key.delete()

            user_obj.key.delete()

        self.render_json({
            "message": "deactivated",
            "success": True    
        })







class AdminApplications(MainHandler):
    def get(self):
        counter = utils.get_counter()
        curs = Cursor(urlsafe=self.request.get('cursor'))
        applications, next_curs, more = model.Application.query().order(-model.Application.created).fetch_page(20, start_cursor=curs)
        if more and next_curs:
            next_curs = next_curs.urlsafe()
        else:
            next_curs = None

        self.render("users/admin-applications.html", counter=counter, applications=applications, next_curs=next_curs)

    def post(self):
        verdict = self.request.get("verdict")
        applicationID = self.request.get("applicationID")

        application = model.Application.get_by_id(int(applicationID))

        applyingUser = application.user.get()

        if verdict == "accept":
            application.accept = True
            applyingUser.userType = application.application
            applyingUser.applicationProgress = False
            userType = application.application
            applyingUser.put()
            utils.increment_counter(application.application+"s")
            utils.notify_user("application-accepted", applyingUser)
        elif verdict == "reject":
            application.reject = True
            userType = applyingUser.userType
            if applyingUser.userType == application:
                utils.decrement_counter(application.application+"s")
            applyingUser.applicationProgress = False
            applyingUser.put()
            utils.notify_user("application-rejected", applyingUser)

        application.put()
        application.key.delete()
        utils.decrement_counter("applications")

        self.render_json({
            "message": "success",
            "verdict": verdict,
            "userType": userType,
            "applicationID": applicationID
            })

class AdminGlobalCurator(MainHandler):
    def post(self):
        verdict = self.request.get("verdict")
        userID = self.request.get("userID")

        applyingUser = model.User.get_by_id(int(userID))

        if verdict == "accept":
            applyingUser.isGlobalCurator = True
            applyingUser.put()
            utils.increment_counter("globalCurators")
            utils.notify_user("global-curator-accepted", applyingUser)
        elif verdict == "reject":
            applyingUser.isGlobalCurator = False
            applyingUser.put()
            utils.decrement_counter("globalCurators")
            utils.notify_user("global-curator-rejected", applyingUser)

        self.render_json({
            "message": "success",
            "verdict": verdict,
            "isGlobalCurator": applyingUser.isGlobalCurator,
            "userID": userID
            })

class AdminSaveTags(MainHandler):
    def post(self):
        tags = self.request.get("tags")
        tagObjs = []

        tags = tags.split(",")
        for tag in tags:
            tag = tag.lower()
            tag = tag.strip()
            if len(tag) > 0:
                existing_tag = model.Tag.query(model.Tag.tag == tag).get()
                if not existing_tag:
                    t = model.Tag(tag=tag)
                    t.put()
                    logging.error("put tag %s" % tag)
                    tagID = t.key.id()
                    tagObjs.append({"tag": t.tag, "tagID": t.key.id()})

        self.render_json({
            "message": "success",
            "tags": tagObjs
            })

class AdminRemoveTag(MainHandler):
    def post(self):
        tagID = self.request.get("tagID")
        tag = model.Tag.get_by_id(int(tagID))
        if tag:
            tag.key.delete()

        self.render_json({
            "message": "success",
            "tagID": tagID
            })

class AdminStaticPage(MainHandler):
    def get(self):
        self.render("users/admin-static-page-menu.html")


class AdminStaticPageEdit(MainHandler):
    def get(self, page_name):
        page = model.StaticPage.query(model.StaticPage.page_name == page_name).get()
        self.render("users/admin-static-page.html", page=page, page_name=page_name)

    def post(self, page_name):

        title = self.request.get("title")
        text = self.request.get("text")
        text =  text.strip()

        page = model.StaticPage.query(model.StaticPage.page_name == page_name).get()
        
        if not page:
            page = model.StaticPage()

        page.title = title
        page.text = text
        page.page_name = page_name

        page.put()

        self.redirect("/admin/static-page/%s" % page_name)










