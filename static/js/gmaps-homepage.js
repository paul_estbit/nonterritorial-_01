

    /*===========================================*/
    // Get User location
    /*===========================================*/

function get_user_location(){
    // Try HTML5 geolocation.
      if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            console.log("POS: ", pos);
            panTo(pos, map);
            populateMarkers(pos);
        }, function() {
          console.log("is this a callback?");
        });
      } else {
        populateMarkers(false);
      };
};

    /*===========================================*/
    // Fetching idea locations
    /*===========================================*/

function findCurrentPosition(ptA, ptB, takeOffTime, arrivalTime){

    console.log(" = = = = = = = = = = = = = = = = ");
    console.log(ptA);

    var now = Date.now();

    var distance_travelled_str = null;
    var heading_str = null;
    var time_to_arrival_hrs_str = null;
    var current_location = null;
    var lat = null;
    var lng = null;
    var status;// parked, in flight, waiting to land, landed

    // if 2 points
    if ( ptA["lat"] && ptB["lat"] ){

        var A = new google.maps.LatLng(ptA["lat"], ptA["lng"]);
        var B = new google.maps.LatLng(ptB["lat"], ptB["lng"]);
        var distanceBetween = google.maps.geometry.spherical.computeDistanceBetween(A, B);
        var heading = google.maps.geometry.spherical.computeHeading(A, B);

    };

    // if only 1 point
    if ( ptA["lat"] && !ptB["lat"] ){
        console.log("!!! = = = = = = = = = = = = = = = = !!!");
        status = "parked";
        current_location = ptA;
        lat = ptA.lat;
        lng = ptA.lng;
    };

    // if accepted landing permission
    if ( arrivalTime && takeOffTime ){

        var total_time = parseFloat( arrivalTime - takeOffTime );//parseFloat(arrivalTime) - parseFloat(takeOffTime);
        var time_elapsed = parseFloat( now - takeOffTime )//parseFloat(now) - parseFloat(takeOffTime);
        var fraction_time_elapsed = parseFloat( time_elapsed / total_time );//parseFloat(time_elapsed) / parseFloat(total_time);
        var time_to_arrival = parseFloat( total_time - time_elapsed );//parseFloat(total_time) - parseFloat(time_elapsed);

        var current_distance_travelled = parseFloat( distanceBetween*fraction_time_elapsed );//parseFloat(distanceBetween) * parseFloat(fraction_time_elapsed);

        var current_location = google.maps.geometry.spherical.computeOffset(A, current_distance_travelled, heading);

        console.log("fraction_time_elapsed: "+fraction_time_elapsed+" ms");
        console.log("distanceBetween: "+distanceBetween+" m");
        console.log("current_distance_travelled: "+current_distance_travelled+" m");
        console.log("current_location: ", current_location);

        var lat = current_location.lat();
        if ( lat > 0 ){
            lat =  "LAT: " + lat.toFixed(4).toString() + " N";
        }else{
            lat = "LAT: " + lat.toFixed(4).toString() + " S";
        };
        var lng = current_location.lng();
        if ( lat > 0 ){
            lng = "LNG: " + lng.toFixed(4).toString() + " E";
        }else{
            lng = "LNG: " + lng.toFixed(4).toString() + " W";
        };

        var time_elapsed_hrs = ( time_elapsed / ( 1000*60*60 ) ).toFixed(1);
        var time_to_arrival_hrs = ( time_to_arrival / ( 1000*60*60 ) ).toFixed(1);

        // curator has not marked idea as landed yet.
        if ( time_to_arrival_hrs <= 0 ){
            status = 'waiting to land';
            current_location = ptB;
        };
        // curator has not marked idea as landed yet.
        if ( time_elapsed_hrs <= 24 ){
            status = 'taking off';
        };
        // arriving soon
        if ( time_to_arrival_hrs < 24 ){
            status = "arriving soon";
        };

        if ( time_elapsed_hrs > 24 && time_to_arrival_hrs > 24 ){
            status = "in flight";
        };

        distance_travelled_str = toString( parseFloat( current_distance_travelled ).toFixed(1) )+" km";
        heading_str = heading.toFixed(2).toString() + " deg.";
        time_to_arrival_hrs_str = time_to_arrival_hrs.toString()+" hrs";

    };

    return_obj = {
        'distance_travelled': distance_travelled_str,
        'heading': heading_str,
        'time_to_arrival': time_to_arrival_hrs_str,
        'status': status,
        "current_location": current_location,
        "lat": lat,
        "lng": lng
    };

    return return_obj

};

function populateMarkers(userLocation){

        function gotMarkers(data){
            results = data["results"];
            console.log("API data: ", results);
            for (var i=0; i<results.length; i++){
                var latlng = results[i]//{ results[i]["lat"], results[i]["lng"] }
                var TO_pos = {"lat": results[i]['latTO'], "lng": results[i]['lngTO']};
                var LD_pos = {"lat": results[i]['latLD'], "lng": results[i]['lngLD']};
                
                var takeOffTime = Date.parse( results[i]["takeOffDate"] );
                var arrivalTime = Date.parse( results[i]["landingDate"] );
                
                var updated_pos = findCurrentPosition(TO_pos, LD_pos, takeOffTime, arrivalTime);
                var marker_data = {
                    'name': results[i]["name"],
                    'slug': results[i]["slug"],
                    'number': results[i]["number"],
                    'description': results[i]["description"],
                    'heading': updated_pos["heading"],
                    'time_to_arrival': updated_pos["time_to_arrival"],
                    'status': updated_pos["status"],
                    'distance_travelled': updated_pos["distance_travelled"],
                    'lat': updated_pos["lat"],
                    'lng': updated_pos["lng"],
                };

                console.log("marker_data............ ", marker_data);

                addMarker( updated_pos['current_location'], marker_data );
            };
        };
        // This get the in flight markers
        /*$.ajax({
            url: "/api/flights",
            type: "get",
            data: {"nearby": userLocation},
            success: gotMarkers
        }).fail(function(){
            console.log("couldn't reach idea api");
        });*/
        // get all public ideas & landing permissions
        $.ajax({
            url: "/api/publicIdeas",
            type: "get",
            data: {"nearby": userLocation},
            success: gotMarkers
        }).fail(function(){
            console.log("couldn't reach idea api");
        });


};

function panTo(latLng, map) {
        console.log("Place Marker");
        console.log(latLng);
        console.log(marker);
        map.panTo(latLng);
    };

    function placeMarker(latLng, map) {
            if ( !marker ){
                    console.log("placing 1st marker");
                    marker = new google.maps.Marker({
                    position: latLng,
                    map: map
                });
            }else{
                console.log("placing existing marker");
                marker.setPosition( latLng );
            };
        };






// =================================
// Front Page Maps
// =================================


    var styleArray = [{
        "stylers": [{
            "visibility": "simplified"
        }]
    }, {
        "stylers": [{
            "color": "#131314"
        }]
    }, {
        "featureType": "water",
        "stylers": [{
            "color": "#131313"
        }, {
            "lightness": 7
        }]
    }, {
        "elementType": "labels.text.fill",
        "stylers": [{
            "visibility": "on"
        }, {
            "lightness": 25
        }]
    },
    {
                "elementType": "labels.text",
                "stylers": [
                  { "visibility": "off" }
                ]
              }

    ];

    var $map = $("#fp-map");
    var map;
    var marker;
    var markers = [];

    var dummy_locations = [
        {lat: 41.902678, lng: 10.453403},
        {lat: -32.902678, lng: 18.56},
        {lat: 41.902, lng: 12.453403},
        {lat: 21.902678, lng: -12.45},
        {lat: 16.902678, lng: 8.453403}
    ];
    var dummy_names = [
        'Artwork A',
        'Artwork B',
        'Artwork C',
        'Artwork D',
        'Artwork E'
    ];

    /*function get_ideas(){
        function success(data){
            console.log("API data: ", data);
            results = data["results"];
        };

        $.ajax({
            url: "/api/ideas",
            type: "get",
            data: {},
            success: success
        }).fail(function(){
            console.log("couldn't reach idea api");
        });
    }*/


function initMap() {
        // fallback initial location
        /*if ( $("#userLocation").val() ){
            var initialLocation = JSON.parse( $("#userLocation").val() );
        }else{
            var initialLocation = null;
        };*/
        get_user_location();
        console.log("location from browser... ", initialLocation);
        if ( !initialLocation ){
            var initialLocation = null;
        };
        console.log("initialLocation", initialLocation);
        if ( !initialLocation ){
            initialLocation = {lat: 41.902678, lng: 12.453403};//41.902678, 12.453403
        };
        console.log("initialLocation...", initialLocation);
        map = new google.maps.Map(document.getElementById('fp-map'), {
            //center: {lat: -34.397, lng: 150.644},
            center: initialLocation,
            zoom: 3,
            minZoom: 3,
            styles: styleArray,
            backgroundColor: 'none',
            streetViewControl: false,
            mapTypeControlOptions: { mapTypeIds: [] },
            scrollwheel: false,
            zoomControl: true,
            zoomControlOptions: {
                position: google.maps.ControlPosition.LEFT_CENTER
            }
        });
        panTo(initialLocation, map);
        populateMarkers();
};

    // populates content side menu using some data from the marker, prob an ID
    $("body").on("click", "#closeInfo", function(){
        $("#statsContainer").removeClass("slide-out-right");
    });
    function populateContentMenu(Obj){
        console.log("Marker number: ", Obj['number']);
        //$('#menuContentBody').animateCss('slideOutRight');
        if ( $("#statsContainer").hasClass("slide-out-right") ){
            $("#statsContainer").removeClass("slide-out-right");
            setTimeout(function(){
                $("#ideaName").text(Obj['name']);
                $("#ideaStatus").text(Obj['title']);
                $("#ideaNumber").text(Obj['number']);
                $("#ideaHeading").text(Obj['heading']);
                $("#ideaLat").text(Obj['curr_lat']);
                $("#ideaLng").text(Obj['curr_lng']);
                $("#ideaETA").text(Obj['time_to_arrival']);
                $("#ideaLink").attr("href", "/idea/"+Obj['slug']);
                $("#statsContainer").addClass("slide-out-right");
            }, 1600);
        }else{
            $("#ideaName").text(Obj['name']);
            $("#ideaStatus").text(Obj['title']);
            $("#ideaNumber").text(Obj['number']);
            $("#ideaHeading").text(Obj['heading']);
            $("#ideaLat").text(Obj['curr_lat']);
            $("#ideaLng").text(Obj['curr_lng']);
            $("#ideaETA").text(Obj['time_to_arrival']);
            $("#ideaLink").attr("href", "/idea/"+Obj['slug']);
            $("#statsContainer").addClass("slide-out-right");
        };
    };

    // Adds a marker to the map and push to the array.
    function addMarker(location, marker_data) {
        console.log("location: ", location)

        var image = new google.maps.MarkerImage(
            '/static/img/circle.png',
            null, // size
            null, // origin
            new google.maps.Point( 8, 8 ), // anchor (move to center of marker)
            new google.maps.Size( 17, 17 ) // scaled size (required for Retina display icon)
        );

        var marker = new google.maps.Marker({
            flat: true,
            icon: image,
            map: map,
            optimized: false,
            title: marker_data["status"],
            visible: true,
            position: location,
            map: map,
            name: marker_data["name"],
            number: marker_data["number"],
            slug: marker_data["slug"],
            heading: marker_data["heading"],
            time_to_arrival: marker_data["time_to_arrival"],
            distance_travelled: marker_data["distance_travelled"],
            curr_lat: marker_data["lat"],
            curr_lng: marker_data["lng"]
        });
        markers.push(marker);
        marker.addListener('click', function() {
            map.setZoom(5);
            map.setCenter(marker.getPosition());
            populateContentMenu(this);
          });

    };

    // Sets the map on all markers in the array.
    function setMapOnAll(map) {
      for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(map);
      }
    }

    // Removes the markers from the map, but keeps them in the array.
    function clearMarkers() {
      setMapOnAll(null);
    }

    // Shows any markers currently in the array.
    function showMarkers() {
      setMapOnAll(map);
    }

    // Deletes all markers in the array by removing references to them.
    function deleteMarkers() {
      clearMarkers();
      markers = [];
    }

    function placeCustomMarker(latLng, map){

        /*var icon = {
            url: "/static/img/pulse.svg",
            anchor: new google.maps.Point(25,50),
            scaledSize: new google.maps.Size(50,50)
        };*/
        /*var icon = {
            //url: "http://www.primeracoop.com/assets/pin.svg",
            url: "http://localhost:8080/static/img/pulse.svg",
            anchor: new google.maps.Point(25,50),
            scaledSize: new google.maps.Size(50,50)
        }*/

        /*var marker = new google.maps.Marker({
            position: latLng,
            map: map,
            draggable: false,
            icon: icon,
            zIndex : -20
        });*/

        /*var marker = new MarkerWithLabel({
            position: myMap.getCenter(),
            icon: {
              path: google.maps.SymbolPath.CIRCLE,
              scale: 0, //tamaño 0
            },
            map: myMap,
            draggable: true,
            labelAnchor: new google.maps.Point(10, 10),
            labelClass: "label", // the CSS class for the label
          });*/

    };





