
    /*===========================================*/
    // Get User location
    /*===========================================*/

function get_user_location(){
    // Try HTML5 geolocation.
      if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            //console.log("POS: ", pos);
            panTo(pos, map);
            populateMarkers(pos);
        }, function() {
          console.log("get_user_location callback");
        });
      } else {
        populateMarkers(false);
      };
};

    /*===========================================*/
    // Fetching idea locations
    /*===========================================*/

function findCurrentPosition(ptA, ptB, takeOffTime, arrivalTime){

    //console.log(" = = = = = = = = = = = = = = = = ");
    //console.log(ptA);

    var now = Date.now();

    var distance_travelled_str = null;
    var heading_str = null;
    var time_to_arrival_hrs_str = null;
    var current_location = null;
    var lat = null;
    var lng = null;
    var status;// parked, in flight, waiting to land, landed

    // if 2 points
    if ( ptA["lat"] && ptB["lat"] ){

        var A = new google.maps.LatLng(ptA["lat"], ptA["lng"]);
        var B = new google.maps.LatLng(ptB["lat"], ptB["lng"]);
        var distanceBetween = google.maps.geometry.spherical.computeDistanceBetween(A, B);
        var heading = google.maps.geometry.spherical.computeHeading(A, B);

    };

    // if only 1 point
    if ( ptA["lat"] && !ptB["lat"] ){
        //console.log("!!! = = = = = = = = = = = = = = = = !!!");
        status = "parked";
        current_location = ptA;
        lat = ptA.lat;
        lng = ptA.lng;
    };

    // if accepted landing permission
    if ( arrivalTime && takeOffTime ){

        var total_time = parseFloat( arrivalTime - takeOffTime );//parseFloat(arrivalTime) - parseFloat(takeOffTime);
        var time_elapsed = parseFloat( now - takeOffTime )//parseFloat(now) - parseFloat(takeOffTime);
        var fraction_time_elapsed = parseFloat( time_elapsed / total_time );//parseFloat(time_elapsed) / parseFloat(total_time);
        var time_to_arrival = parseFloat( total_time - time_elapsed );//parseFloat(total_time) - parseFloat(time_elapsed);

        var current_distance_travelled = parseFloat( distanceBetween*fraction_time_elapsed );//parseFloat(distanceBetween) * parseFloat(fraction_time_elapsed);

        var current_location = google.maps.geometry.spherical.computeOffset(A, current_distance_travelled, heading);

        //console.log("fraction_time_elapsed: "+fraction_time_elapsed+" ms");
        //console.log("distanceBetween: "+distanceBetween+" m");
        //console.log("current_distance_travelled: "+current_distance_travelled+" m");
        //console.log("current_location: ", current_location);

        var lat = current_location.lat();
        if ( lat > 0 ){
            lat =  "LAT: " + lat.toFixed(4).toString() + " N";
        }else{
            lat = "LAT: " + lat.toFixed(4).toString() + " S";
        };
        var lng = current_location.lng();
        if ( lat > 0 ){
            lng = "LNG: " + lng.toFixed(4).toString() + " E";
        }else{
            lng = "LNG: " + lng.toFixed(4).toString() + " W";
        };

        var time_elapsed_hrs = ( time_elapsed / ( 1000*60*60 ) ).toFixed(1);
        var time_to_arrival_hrs = ( time_to_arrival / ( 1000*60*60 ) ).toFixed(1);

        // curator has not marked idea as landed yet.
        if ( time_to_arrival_hrs <= 0 ){
            status = 'waiting to land';
            current_location = ptB;
        };
        // curator has not marked idea as landed yet.
        if ( time_elapsed_hrs <= 24 ){
            status = 'taking off';
        };
        // arriving soon
        if ( time_to_arrival_hrs < 24 ){
            status = "arriving soon";
        };

        if ( time_elapsed_hrs > 24 && time_to_arrival_hrs > 24 ){
            status = "in flight";
        };

        distance_travelled_str = toString( parseFloat( current_distance_travelled ).toFixed(1) )+" km";
        heading_str = heading.toFixed(2).toString() + " deg.";
        time_to_arrival_hrs_str = time_to_arrival_hrs.toString()+" hrs";

    };

    return_obj = {
        'distance_travelled': distance_travelled_str,
        'heading': heading_str,
        'time_to_arrival': time_to_arrival_hrs_str,
        'status': status,
        "current_location": current_location,
        "lat": lat,
        "lng": lng
    };

    return return_obj

};

function populateMarkers(userLocation){

        function gotMarkers(data){
            results = data["results"];
            console.log("API data: ", results);
            for (var i=0; i<results.length; i++){
                var latlng = results[i]//{ results[i]["lat"], results[i]["lng"] }
                var TO_pos = {"lat": results[i]['latTO'], "lng": results[i]['lngTO']};
                var LD_pos = {"lat": results[i]['latLD'], "lng": results[i]['lngLD']};
                
                var takeOffTime = Date.parse( results[i]["takeOffDate"] );
                var arrivalTime = Date.parse( results[i]["landingDate"] );
                
                var updated_pos = findCurrentPosition(TO_pos, LD_pos, takeOffTime, arrivalTime);
                var marker_data = {
                    'name': results[i]["name"],
                    'category': results[i]["category"],
                    'slug': results[i]["slug"],
                    'number': results[i]["number"],
                    'flightNumber': results[i]["flightNumber"],
                    'description': results[i]["description"],
                    'heading': updated_pos["heading"],
                    'time_to_arrival': updated_pos["time_to_arrival"],
                    'status': updated_pos["status"],
                    'distance_travelled': updated_pos["distance_travelled"],
                    'from': results[i]["from"],
                    'to': results[i]["to"],
                    'lat': updated_pos["lat"],
                    'lng': updated_pos["lng"],
                    'landing': results[i]["landingDate"],
                    'surname': results[i]["artist"],
                };

                console.log("marker_data............ ", marker_data);

                addMarker( updated_pos['current_location'], marker_data );
            };
        };

        // get all public ideas & landing permissions
        $.ajax({
            url: "/api/getIdeas",
            type: "get",
            data: {"nearby": userLocation},
            success: gotMarkers
        }).fail(function(){
            console.log("couldn't reach idea api");
        });


};

function panTo(latLng, map) {
        //console.log("Place Marker");
        //console.log(latLng);
        //console.log(marker);
        map.panTo(latLng);
    };

    function placeMarker(latLng, map) {
            if ( !marker ){
                    //console.log("placing 1st marker");
                    marker = new google.maps.Marker({
                    position: latLng,
                    map: map
                });
            }else{
                //console.log("placing existing marker");
                marker.setPosition( latLng );
            };
        };






// =================================
// Front Page Maps
// =================================


    var styleArray = [{
        "stylers": [{
            "visibility": "simplified"
        }]
    }, {
        "stylers": [{
            "color": "#131314"
        }]
    }, {
        "featureType": "water",
        "stylers": [{
            "color": "#131314"
        }, {
            "lightness": 7
        }]
    }, {
        "elementType": "labels.text.fill",
        "stylers": [{
            "visibility": "on"
        }, {
            "lightness": 25
        }]
    },
    {
        "elementType": "labels.text",
        "stylers": [
          { "visibility": "off" }
        ]
      },

    {
    "featureType": "administrative.land_parcel",
    "elementType": "geometry.stroke",
    "stylers": [
      { "weight": 1 },
      { "visibility": "on" },
      { "color": "#232323" }
    ]
  },
  {
    "featureType": "administrative.country",
    "elementType": "geometry.stroke",
    "stylers": [
      { "visibility": "on" },
      { "color": "#232323" }
    ]
  }

    ];

/*[
  {
    "featureType": "administrative.land_parcel",
    "elementType": "geometry.stroke",
    "stylers": [
      { "weight": 1 },
      { "visibility": "on" },
      { "color": "#f3f4f7" }
    ]
  },{
    "featureType": "administrative.country",
    "elementType": "labels",
    "stylers": [
      { "visibility": "off" }
    ]
  },{
    "featureType": "water",
    "elementType": "labels",
    "stylers": [
      { "visibility": "off" }
    ]
  },{
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [
      { "visibility": "simplified" },
      { "color": "#000" }
    ]
  },{
    "featureType": "landscape",
    "stylers": [
      { "visibility": "off" }
    ]
  },{
    "featureType": "administrative.country",
    "elementType": "geometry.stroke",
    "stylers": [
      { "visibility": "on" },
      { "color": "#ffffff" }
    ]
  },{
    "featureType": "administrative.land_parcel",
    "elementType": "geometry.stroke",
    "stylers": [
      { "visibility": "on" },
      { "color": "#ffffff" }
    ]
  },{
    "featureType": "administrative.province",
    "stylers": [
      { "visibility": "off" }
    ]
  },{
    "featureType": "administrative.land_parcel",
    "elementType": "geometry.stroke",
    "stylers": [
      { "color": "#ffffff" },
      { "weight": 1 },
      { "visibility": "on" }
    ]
  },{
    "featureType": "administrative.country",
    "elementType": "geometry.stroke",
    "stylers": [
      { "visibility": "on" },
      { "color": "#ffffff" }
    ]
  },{
    "featureType": "administrative.land_parcel",
    "elementType": "geometry.fill",
    "stylers": [
      { "color": "#808080" },
      { "visibility": "on" }
    ]
  }
];*/

    var $map = $("#fp-map");
    var map;
    var marker;
    var markers = [];

    var overlay = null;
    USGSOverlay.prototype = new google.maps.OverlayView();

function initMap() {

        // TURN ON TO CENTER ON USERS LOCATION
        //get_user_location();


        //console.log("location from browser... ", initialLocation);
        if ( !initialLocation ){
            var initialLocation = null;
        };
        //console.log("initialLocation", initialLocation);
        if ( !initialLocation ){
            initialLocation = {lat: 41.902678, lng: 12.453403};//41.902678, 12.453403
        };
        console.log("initialLocation...", initialLocation);
        map = new google.maps.Map(document.getElementById('fp-map'), {
            //center: {lat: -34.397, lng: 150.644},
            center: initialLocation,
            zoom: 3,
            minZoom: 3,
            styles: styleArray,
            backgroundColor: 'none',
            streetViewControl: false,
            mapTypeControlOptions: { mapTypeIds: [] },
            scrollwheel: false,
            zoomControl: false,
            zoomControlOptions: {
                position: google.maps.ControlPosition.LEFT_CENTER
            }
        });
        panTo(initialLocation, map);
        populateMarkers();

        // The photograph is courtesy of the U.S. Geological Survey.
        /* ------------ Custom Overlay -------------- */
        /*var bounds = new google.maps.LatLngBounds(
            new google.maps.LatLng(62.281819, -150.287132),
            new google.maps.LatLng(62.400471, -150.005608));

        var srcImage = 'https://developers.google.com/maps/documentation/javascript/';
        srcImage += 'examples/full/images/talkeetna.png';
        overlay = new USGSOverlay(bounds, srcImage, map);*/

};

    // populates content side menu using some data from the marker, prob an ID
    $("body").on("click", "#closeInfo", function(){
        $("#statsContainer").removeClass("slide-out-right");
    });
    function populateContentMenu(Obj){
        console.log("Marker obj: ", Obj);
        //$('#menuContentBody').animateCss('slideOutRight');

        $("#statsContainer").addClass("flash");
        //$("#statsContainer").one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
        $("#statsContainer").one('webkitTransitionEnd TransitionEnd MSAnimationEnd oTransitionEnd transitionend', function(){
            $("#statsContainer").removeClass("flash");
            //console.log("...... transition end")
        });

        $("#statsContainer li").removeClass("hidden");
        $("#nothing_selected").addClass("hidden");

        function hide_irrelevant_tags(){
            $("#liLat").addClass("hidden");
            $("#liLng").addClass("hidden");
            $("#liFrom").addClass("hidden");
            $("#liTo").addClass("hidden");
            $("#liLanding").addClass("hidden");
            $("#liStatus").addClass("hidden");
        };

        if ( Obj['title'] == "parked" ){
            var status = "waiting to take off";
        }else{
            var status = Obj['title'];
        };

        var from_location = Obj['from'];

        var displayTitle = "FLIGHT";
        if ( Obj["category"] == "airport"){
            displayTitle = "AIRPORT";
            status = "-";
            from_location = "-";
            //hide_irrelevant_tags();
        }else if ( Obj["category"] == "freeport" ){
            displayTitle = "FREEPORT";
            status = "-"
            from_location = "-";
            //hide_irrelevant_tags();
        }/*else if ( Obj["category"] == "artist" ){
            displayTitle = "ARTIST";
            $("#liTitle").addClass("hidden");
            $("#liStatus").addClass("hidden");
            hide_irrelevant_tags();
        }*/else{
            displayTitle = "FLIGHT";
        };

        var ideaNumber =  "#"+Obj['number'] + Obj["flightNumber"];
        var landing = Obj['landing'];
        if ( !landing || landing == "null" || landing == null ){
            landing = "-";
        };

        if ( $("#statsContainer").hasClass("slide-out-right") ){
            //$("#statsContainer").removeClass("slide-out-right");
            setTimeout(function(){
                $("#ideaName").text(Obj['name']);
                $("#ideaSurname").text(Obj['surname']);
                $("#ideaStatus").text( status );
                $("#ideaNumber").text(ideaNumber);
                $("#ideaHeading").text(Obj['heading']);
                $("#ideaLat").text(Obj['curr_lat']);
                $("#ideaLng").text(Obj['curr_lng']);
                $("#ideaFrom").text(from_location);
                $("#ideaTo").text(Obj['to']);
                $("#ideaETA").text(Obj['time_to_arrival']);
                $("#ideaLanding").text(landing);
                $("#ideaLink").attr("href", "/idea/"+Obj['number']+"/"+Obj['slug']);
                $("#statsContainer").addClass("slide-out-right");
                $("#displayTitle").text(displayTitle);
            }, 1600);
        }else{
            $("#ideaName").text(Obj['name']);
            $("#ideaSurname").text(Obj['surname']);
            $("#ideaStatus").text( status );
            $("#ideaNumber").text(ideaNumber);
            $("#ideaHeading").text(Obj['heading']);
            $("#ideaLat").text(Obj['curr_lat']);
            $("#ideaLng").text(Obj['curr_lng']);
            $("#ideaFrom").text(from_location);
            $("#ideaTo").text(Obj['to']);
            $("#ideaETA").text(Obj['time_to_arrival']);
            $("#ideaLanding").text(landing);
            $("#ideaLink").attr("href", "/idea/"+Obj['number']+"/"+Obj['slug']);
            $("#statsContainer").addClass("slide-out-right");
            $("#displayTitle").text(displayTitle);
        };
    };

    // Adds a marker to the map and push to the array.
    function addMarker(location, marker_data) {
        //console.log("location: ", location)

        var image = new google.maps.MarkerImage(
            '/static/img/circle.png',
            //'/static/img/pulse.svg',
            null, // size
            null, // origin
            new google.maps.Point( 8, 8 ), // anchor (move to center of marker)
            new google.maps.Size( 17, 17 ) // scaled size (required for Retina display icon)
        );

        var marker = new google.maps.Marker({
            flat: true,
            icon: image,
            map: map,
            optimized: false,
            title: marker_data["status"],
            visible: true,
            position: location,
            map: map,
            name: marker_data["name"],
            category: marker_data["category"],
            number: marker_data["number"],
            flightNumber: marker_data["flightNumber"],
            slug: marker_data["slug"],
            heading: marker_data["heading"],
            time_to_arrival: marker_data["time_to_arrival"],
            distance_travelled: marker_data["distance_travelled"],
            from: marker_data["from"],
            to: marker_data["to"],
            curr_lat: marker_data["lat"],
            curr_lng: marker_data["lng"],
            landing: marker_data["landing"],
            surname: marker_data["surname"]
        });
        markers.push(marker);
        marker.addListener('click', function() {
            //USGSOverlay.prototype = new google.maps.OverlayView();

            var bounds = new google.maps.LatLngBounds(
            new google.maps.LatLng(62.281819, -150.287132),
            new google.maps.LatLng(62.400471, -150.005608)
            );

            var div_html = '<ul id="statsContainer" class="sidebarMenu">'+
                        '<li class="header-item">SELECTED</li>'+
                        '<li class="">'+
                            '<a id="ideaLink" href=""><span id="ideaNumber">'+marker_data["flightNumber"]+'</span> <span id="ideaSurname">'+marker_data["surname"]+'</span></a>'+
                        '</li>'+
                        '<li id="liTitle" class="">TITLE: <span id="ideaName">'+marker_data["name"]+'</span></li>'+
                        '<li id="liStatus" class="">STATUS: <span id="ideaStatus">'+marker_data["status"]+'</span></li>'+
                        '<li id="liLat" class="">LAT: <span id="ideaLat">'+marker_data["lat"]+'</span></li>'+
                        '<li id="liLng" class="">LNG: <span id="ideaLng">'+marker_data["lng"]+'</span></li>'+
                        '<li id="liFrom" class="">FROM: <span id="ideaFrom">'+marker_data["from"]+'</span></li>'+
                        '<li id="liTo" class="">TO: <span id="ideaTo">'+marker_data["to"]+'</span></li>'+
                        '<li id="liLanding" class="">LANDING: <span id="ideaLanding">'+marker_data["landing"]+'</span></li>'+
                    '</ul>';

            var srcImage = 'https://developers.google.com/maps/documentation/javascript/';
            srcImage += 'examples/full/images/talkeetna.png';
            if ( overlay ){
                overlay.setMap(null);
            };
            overlay = new USGSOverlay(bounds, srcImage, map, {lat: marker_data["lat"], lng: marker_data["lng"]}, div_html);

            map.setZoom(5);
            map.setCenter(marker.getPosition());
            populateContentMenu(this);
          });

    };

    // Sets the map on all markers in the array.
    function setMapOnAll(map) {
      for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(map);
      }
    }

    // Removes the markers from the map, but keeps them in the array.
    function clearMarkers() {
      setMapOnAll(null);
    }

    // Shows any markers currently in the array.
    function showMarkers() {
      setMapOnAll(map);
    }

    // Deletes all markers in the array by removing references to them.
    function deleteMarkers() {
      clearMarkers();
      markers = [];
    }

// Zoom controllers
function zoomIn(){
    console.log("Zoom in");
    var currentZoom = map.getZoom();
    currentZoom += 1;
    if (currentZoom > 16){ 
        map.setZoom(16); 
    }else{
        map.setZoom(currentZoom); 
    };
};
function zoomOut(){
    var currentZoom = map.getZoom();
    currentZoom -= 1;
    if (currentZoom < 3){ 
        map.setZoom(3); 
    }else{
        map.setZoom(currentZoom); 
    };
};



// =================================
// Custom Overlay
// =================================

      // This example adds hide() and show() methods to a custom overlay's prototype.
      // These methods toggle the visibility of the container <div>.
      // Additionally, we add a toggleDOM() method, which attaches or detaches the
      // overlay to or from the map.

      /*var overlay;
      USGSOverlay.prototype = new google.maps.OverlayView();*/

     /* function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 11,
          center: {lat: 62.323907, lng: -150.109291},
          mapTypeId: google.maps.MapTypeId.SATELLITE
        });

        var bounds = new google.maps.LatLngBounds(
            new google.maps.LatLng(62.281819, -150.287132),
            new google.maps.LatLng(62.400471, -150.005608));

        // The photograph is courtesy of the U.S. Geological Survey.
        var srcImage = 'https://developers.google.com/maps/documentation/javascript/';
        srcImage += 'examples/full/images/talkeetna.png';

        overlay = new USGSOverlay(bounds, srcImage, map);
      }*/

      /** @constructor */
      function USGSOverlay(bounds, image, map, topLeft, div_html) {

        // Now initialize all properties.
        this.bounds_ = bounds;
        this.image_ = image;
        this.map_ = map;
        var location = new google.maps.LatLng(topLeft["lat"], topLeft["lng"])
        this.topLeft_ = location;
        this.divHTML_ = div_html;

        // Define a property to hold the image's div. We'll
        // actually create this div upon receipt of the onAdd()
        // method so we'll leave it null for now.
        this.div_ = null;

        // Explicitly call setMap on this overlay
        this.setMap(map);
      }

      /**
       * onAdd is called when the map's panes are ready and the overlay has been
       * added to the map.
       */
      USGSOverlay.prototype.onAdd = function() {

        var div = document.createElement('div');
        div.style.border = 'none';
        div.style.borderWidth = '0px';
        div.style.position = 'absolute';

        // Create the img element and attach it to the div.
        /*var img = document.createElement('img');
        img.src = this.image_;
        img.style.width = '100%';
        img.style.height = '100%';
        div.appendChild(img);*/
        var html = "<div class='onMapMarkerDetailContainer'>"+this.divHTML_+"</div>"
        $(div).append(html);

        this.div_ = div;

        // Add the element to the "overlayImage" pane.
        var panes = this.getPanes();
        panes.overlayImage.appendChild(this.div_);
      };

      USGSOverlay.prototype.draw = function() {

        // We use the south-west and north-east
        // coordinates of the overlay to peg it to the correct position and size.
        // To do this, we need to retrieve the projection from the overlay.
        var overlayProjection = this.getProjection();

        // Retrieve the south-west and north-east coordinates of this overlay
        // in LatLngs and convert them to pixel coordinates.
        // We'll use these coordinates to resize the div.
        var sw = overlayProjection.fromLatLngToDivPixel(this.bounds_.getSouthWest());
        var ne = overlayProjection.fromLatLngToDivPixel(this.bounds_.getNorthEast());

        var topLeft = overlayProjection.fromLatLngToDivPixel(this.topLeft_);

        // Resize the image's div to fit the indicated dimensions.
        var div = this.div_;
        /*div.style.left = sw.x + 'px';
        div.style.top = ne.y + 'px';
        div.style.width = (ne.x - sw.x) + 'px';
        div.style.height = (sw.y - ne.y) + 'px';*/

        div.style.left = topLeft.x + 'px';
        div.style.top = topLeft.y + 'px';
        div.style.width = '100px';
        div.style.height = '100px';

      };

      USGSOverlay.prototype.onRemove = function() {
        this.div_.parentNode.removeChild(this.div_);
      };

      // Set the visibility to 'hidden' or 'visible'.
      USGSOverlay.prototype.hide = function() {
        if (this.div_) {
          // The visibility property must be a string enclosed in quotes.
          this.div_.style.visibility = 'hidden';
        }
      };

      USGSOverlay.prototype.show = function() {
        if (this.div_) {
          this.div_.style.visibility = 'visible';
        }
      };

      USGSOverlay.prototype.toggle = function() {
        if (this.div_) {
          if (this.div_.style.visibility === 'hidden') {
            this.show();
          } else {
            this.hide();
          }
        }
      };

      // Detach the map from the DOM via toggleDOM().
      // Note that if we later reattach the map, it will be visible again,
      // because the containing <div> is recreated in the overlay's onAdd() method.
      USGSOverlay.prototype.toggleDOM = function() {
        if (this.getMap()) {
          // Note: setMap(null) calls OverlayView.onRemove()
          this.setMap(null);
        } else {
          this.setMap(this.map_);
        }
      };

google.maps.event.addDomListener(window, 'load', initMap);

