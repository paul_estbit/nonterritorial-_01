$(function() {
  var OD_PAIRS = [
    /*[5733953138851840, 5171003185430528],
    [4889528208719872, 6296903092273152],
    [5171003185430528, 6296903092273152],
    [5171003185430528, 5733953138851840]*/
    ["FCO", "CPT"],
    ["NRT", "JFK"],
    ["SFO", "NRT"],
    ["LAX", "HNL"],
    ["HNL", "NRT"],
    ["CDG", "JFK"],
    ["NRT", "SYD"],
    ["FCO", "PEK"],
    ["LHR", "PVG"],
    ["NRT", "ARN"],
    ["LAX", "JFK"],
    ["NRT", "DEL"],
    ["DFW", "GRU"],
    ["MAD", "ATL"],
    ["ORD", "CAI"],
    ["HKG", "CDG"],
    ["LAS", "CDG"],
    ["NRT", "SVO"],
    ["DEN", "HNL"],
    ["ORD", "LAX"],
    ["SIN", "SEA"],
    ["SYD", "PEK"],
    ["CAI", "CPT"],
    ["CUN", "JFK"],
    ["ORD", "JFK"],
    ["LHR", "BOM"],
    ["LAX", "MEX"],
    ["LHR", "CPT"],
    ["PVG", "CGK"],
    ["SYD", "BOM"],
    ["JFK", "CPT"],
    ["MAD", "GRU"],
    ["EZE", "FCO"],
    ["DEL", "DXB"],
    ["DXB", "NRT"],
    ["GRU", "MIA"],
    ["SVO", "PEK"],
    ["YYZ", "ARN"],
    ["LHR", "YYC"],
    ["HNL", "SEA"],
    ["JFK", "EZE"],
    ["EZE", "LAX"],
    ["CAI", "HKG"],
    ["SVO", "SIN"],
    ["IST", "MCO"],
    ["MCO", "LAX"],
    ["FRA", "LAS"],
    ["ORD", "FRA"],
    ["MAD", "JFK"]
  ];

  // global variable 'ports' holds airport and freeport locations
  var ports;

  var currentWidth = $('#map').width();
  var width = 938;
  var height = 620;

  var projection = d3.geo
                     .stereographic()
                     .scale(150)
                     //.translate([width / 2, height / 1.41]);
                     .translate([width / 2.7, height / 1.41]);

  var path = d3.geo
               .path()
               .pointRadius(2)
               .projection(projection);
  
  var svg = d3.select("#map")
              .append("svg")
              .attr("preserveAspectRatio", "xMidYMid")
              .attr("viewBox", "0 0 " + width + " " + height)
              .attr("width", currentWidth)
              .attr("height", currentWidth * height / width);

  var airportMap = {};

// added by emile
/*var bezierLine = d3.svg.line()
    .x(function(d) { return d[0]; })
    .y(function(d) { return d[1]; })
    .interpolate("basis");*/

  function transition(plane, route, plane_tail) {
    var l = route.node().getTotalLength();
    var duration_adjust = 200;//50;
    plane.transition()
        .duration(l * duration_adjust)
        .attrTween("transform", delta(plane, route.node()))
        .each("end", function() { route.remove(); })
        .remove();

    for ( var i=0; i<plane_tail.length; i++ ){
      plane_tail[i].transition()
        .duration(l * duration_adjust)
        .attrTween("transform", delta(plane, route.node()))
        .each("end", function() { route.remove(); })
        .remove();
    };
    /*plane_tail.transition()
        .duration(l * 50)
        .attrTween("transform", delta(plane, route.node()))
        .each("end", function() { route.remove(); })
        .remove();*/
  }
  
  // returns a translate for the plane along a path, including scaling and rotation
  function delta(plane, path) {
    var l = path.getTotalLength();
    var plane = plane;
    var start = plane.attr("start");
    return function(i) {
      return function(t) {
        // t is the amount of time passed... this should be attached to each point and updated regularly
        // an initial point and a velocity could also be plotted in to each point and then animated dynamically
        //t = 0.2
        /* ----------------------------------------- */
        //var start_t = (start/100);
        //var p_init = path.getPointAtLength(start_t * l);
        /* ----------------------------------------- */

        var p = path.getPointAtLength(t * l);

        var t2 = Math.min(t + 0.05, 1);
        var p2 = path.getPointAtLength(t2 * l);

        var x = p2.x - p.x;
        var y = p2.y - p.y;

        //var r = 90 - Math.atan2(-y, x) * 180 / Math.PI;
        var r = 180 - Math.atan2(-y, x) * 180 / Math.PI;

        var s = Math.min(Math.sin(Math.PI * t) * 0.7, 0.3);

        return "translate(" + p.x + "," + p.y + ") scale(" + s + ") rotate(" + r + ")";
      }
    }
  }

/* ---------------------------- */
  // this is made up by me...
  function layoutPlanes(plane, route){

    var l = route.node().getTotalLength();
    var time = plane.attr("time");
    plane.transition()
        .duration(l * time)
        .attrTween("transform", delta(plane, route.node()))
        .each("end", function() { route.remove(); })
        .remove();

  };
/* ---------------------------- */

  function fly(origin, destination) {
    var route = svg.append("path")
                   .datum({type: "LineString", coordinates: [airportMap[origin], airportMap[destination]]})
                   .attr("class", "route")
                   .attr("d", path);

    var line = svg.append("path")
                   .datum({type: "LineString", coordinates: [airportMap[origin], airportMap[destination]]})
                   .attr("d", path)
                   .attr("stroke", "rgba(80,80,80,0.5)")
                   .attr("stroke-width", "0.1")
                   .attr("fill", "none");

    /* random starting point between 1 & 100... so % */
    var start = Math.floor((Math.random() * 100) + 1);

    /*var plane = svg.append("path")
                   .attr("class", "plane")
                   .attr("time", 500)
                   .attr("start", start)
                   .on('click', function(d,i) {
                        // handle events here
                        // d - datum
                        // i - identifier or index
                        // this - the `<rect>` that was clicked
                        $("#planeNote").html("Link to Artwork");
                        setTimeout(function(){
                          $("#planeNote").html("");
                        }, 1000)
                    })*/
    var plane = svg.append('circle')
        //circle
        .attr("r", "2px")
        .attr("fill", "#fff")
        // end circle
        .attr("class", "plane-head")
        .attr("time", 500)
        .attr("start", start)
        .on('click', function(d,i) {
            // handle events here
            // d - datum
            // i - identifier or index
            // this - the `<rect>` that was clicked
            $("#planeNote").html("Link to Artwork");
            setTimeout(function(){
              $("#planeNote").html("");
            }, 1000)
        })
      /*.attr('id','pointer')
      .attr('viewBox','0 0 10 10')
      .attr('refX','0')
      .attr('refY','5')
      .attr('markerUnits','strokeWidth')
      .attr('markerWidth','4')
      .attr('markerHeight','3')
      .attr('orient','auto')
      .append('path')*/
      //.attr('d', 'M5 20 l115 0')
      //.attr('d','M 0 0 L 10 5 L 0 10 z');
        //.attr("d", "m25.21488,3.93375c-0.44355,0 -0.84275,0.18332 -1.17933,0.51592c-0.33397,0.33267 -0.61055,0.80884 -0.84275,1.40377c-0.45922,1.18911 -0.74362,2.85964 -0.89755,4.86085c-0.15655,1.99729 -0.18263,4.32223 -0.11741,6.81118c-5.51835,2.26427 -16.7116,6.93857 -17.60916,7.98223c-1.19759,1.38937 -0.81143,2.98095 -0.32874,4.03902l18.39971,-3.74549c0.38616,4.88048 0.94192,9.7138 1.42461,13.50099c-1.80032,0.52703 -5.1609,1.56679 -5.85232,2.21255c-0.95496,0.88711 -0.95496,3.75718 -0.95496,3.75718l7.53,-0.61316c0.17743,1.23545 0.28701,1.95767 0.28701,1.95767l0.01304,0.06557l0.06002,0l0.13829,0l0.0574,0l0.01043,-0.06557c0,0 0.11218,-0.72222 0.28961,-1.95767l7.53164,0.61316c0,0 0,-2.87006 -0.95496,-3.75718c-0.69044,-0.64577 -4.05363,-1.68813 -5.85133,-2.21516c0.48009,-3.77545 1.03061,-8.58921 1.42198,-13.45404l18.18207,3.70115c0.48009,-1.05806 0.86881,-2.64965 -0.32617,-4.03902c-0.88969,-1.03062 -11.81147,-5.60054 -17.39409,-7.89352c0.06524,-2.52287 0.04175,-4.88024 -0.1148,-6.89989l0,-0.00476c-0.15655,-1.99844 -0.44094,-3.6683 -0.90277,-4.8561c-0.22699,-0.59493 -0.50356,-1.07111 -0.83754,-1.40377c-0.33658,-0.3326 -0.73578,-0.51592 -1.18194,-0.51592l0,0l-0.00001,0l0,0z");

    var plane_tail = svg.append('path')
                    .attr('d', 'M0 0 l50 0')
                    .attr("class", "plane-tail")
                    .attr("time", 500)
                    .attr("start", start)

    var plane_tail_1 = svg.append('path')
                    .attr('d', 'M0 0 l10 0')
                    .attr("class", "plane-tail-1")
                    .attr("time", 500)
                    .attr("start", start)

    var plane_tail_2 = svg.append('path')
                    .attr('d', 'M10 0 l20 0')
                    .attr("class", "plane-tail-2")
                    .attr("time", 500)
                    .attr("start", start)

    var plane_tail_3 = svg.append('path')
                    .attr('d', 'M20 0 l30 0')
                    .attr("class", "plane-tail-3")
                    .attr("time", 500)
                    .attr("start", start)

    var plane_tail_4 = svg.append('path')
                    .attr('d', 'M30 0 l40 0')
                    .attr("class", "plane-tail-4")
                    .attr("time", 500)
                    .attr("start", start)

    plane_tail = [ plane_tail_1,  plane_tail_2, plane_tail_3, plane_tail_4]

    /*var plane_tail = svg.append('path')
      .attr("d", bezierLine([[0, 40], [25, 70], [50, 100], [100, 50], [150, 20]]))
      .attr("stroke", "red")
      .attr("stroke-width", 1)
      .attr("fill", "none");*/


   /* This triggers the animation of the flying planes */
    transition(plane, route, plane_tail);
    /* ---------------------------- */
    //layoutPlanes(plane, route);
    /* ---------------------------- */
  }

  /*function drawLine (origin, destination){
    var path = svg.append("path")
    .data([points])
    .attr("d", d3.svg.line()
    .tension(0) // Catmull–Rom
    .interpolate("cardinal-closed"));
  };*/

  function drawPath (origin, destination){
    //console.log("points...", [points]);
    var path = svg.append("path")
    .data([points])
    .attr("d", d3.svg.line()
    .tension(0) // Catmull–Rom
    .interpolate("cardinal-closed"));
  };

  function loaded(error, countries, airports) {
    console.log("ports", ports);
    console.log('topojson.feature(airports, airports.objects.airports).features', topojson.feature(airports, airports.objects.airports).features );

    // drawing the countries
    svg.append("g")
       .attr("class", "countries")
       .selectAll("path")
       .data(topojson.feature(countries, countries.objects.countries).features)
       .enter()
       .append("path")
       .attr("d", path);

     // plotting the positions of each airport
    svg.append("g")
       .attr("class", "airports")
       .selectAll("path")
       .data(topojson.feature(airports, airports.objects.airports).features)
       //.data(ports)
       .enter()
       .append("path")
       .attr("id", function(d) {return d.id;})
       .attr("d", path);

    var geos = topojson.feature(airports, airports.objects.airports).features;
    for (i in geos) {
      airportMap[geos[i].id] = geos[i].geometry.coordinates;
      //console.log(airportMap);
    }

    /*for (i in ports) {
      console.log("ports[i].id", ports[i].id);
      console.log("ports[i].geometry.coordinates", ports[i].geometry.coordinates);
      airportMap[ports[i].id] = ports[i].geometry.coordinates;
      //console.log(airportMap);
    }*/

    var i = 0;
    setInterval(function() {
      /* --------------------------------------------- */
      if ( i < OD_PAIRS.length ){
        var od = OD_PAIRS[i];
        // call the planes to fly along their paths...
        fly(od[0], od[1]);
        i++;
      };
      /* ---------------------------------------------- */

      // ----- original below
      /*if (i > OD_PAIRS.length - 1) {
        i = 0;
      }
      var od = OD_PAIRS[i];
      // call the planes to fly along their paths...
      fly(od[0], od[1]);
      i++;*/
    }, 50);
  }

  /*function success(data){
    ports = data;
    for ( var i=0; i<ports.length; i++ ){
        var projected_coords =  projection( ports[i].geometry.coordinates );

        ports[i].geometry.coordinates[0] = projected_coords[0]
        ports[i].geometry.coordinates[1] = projected_coords[1]

        console.log("projected port: ", ports[i]);
    };

    queue().defer(d3.json, "/static/data/countries2.topo.json")
      .defer(d3.json, "/static/data/airports2.topo.json")
      //.defer(data)
      .await(loaded);
  };
  $.ajax({
    url: "/api/ports/topojson",
    data: {},
    success: success
  }).fail(function(){
    alert("failed to get port data")
  });*/

  queue().defer(d3.json, "/static/data/countries2.topo.json")
         .defer(d3.json, "/static/data/airports2.topo.json")
         .await(loaded);

  $(window).resize(function() {
    currentWidth = $("#map").width();
    svg.attr("width", currentWidth);
    svg.attr("height", currentWidth * height / width);
  });

});


// ====================================================================
// D3 example of data required forentering airport points
// ====================================================================

/*[{
  "type": "Feature",
  "id": "ATL",
  "properties": {},
  "geometry": {
    "type": "Point",
    "coordinates": [-84.4389921196544, 33.63676942979369]
  }
}, {
  "type": "Feature",
  "id": "PEK",
  "properties": {},
  "geometry": {
    "type": "Point",
    "coordinates": [116.58636727543299, 40.07586164417694]
  }
}, {
  "type": "Feature",
  "id": "LHR",
  "properties": {},
  "geometry": {
    "type": "Point",
    "coordinates": [-0.4632393123453369, 51.47479515882837]
  }
}, {
  "type": "Feature",
  "id": "NRT",
  "properties": {},
  "geometry": {
    "type": "Point",
    "coordinates": [140.39246737624134, 35.76241570133719]
  }
}, {
  "type": "Feature",
  "id": "ORD",
  "properties": {},
  "geometry": {
    "type": "Point",
    "coordinates": [-87.90555647311535, 41.9777556766106]
  }
}, {
  "type": "Feature",
  "id": "LAX",
  "properties": {},
  "geometry": {
    "type": "Point",
    "coordinates": [-118.39717354131267, 33.947536428557356]
  }
}, {
  "type": "Feature",
  "id": "CDG",
  "properties": {},
  "geometry": {
    "type": "Point",
    "coordinates": [2.5434746676973248, 49.013520528620106]
  }
}, {
  "type": "Feature",
  "id": "DFW",
  "properties": {},
  "geometry": {
    "type": "Point",
    "coordinates": [-97.031817730186, 32.903359312711416]
  }
}, {
  "type": "Feature",
  "id": "CGK",
  "properties": {},
  "geometry": {
    "type": "Point",
    "coordinates": [106.64652458846845, -6.1289757320056]
  }
}, {
  "type": "Feature",
  "id": "DXB",
  "properties": {},
  "geometry": {
    "type": "Point",
    "coordinates": [55.35552139950542, 25.258491143125127]
  }
}, {
  "type": "Feature",
  "id": "FRA",
  "properties": {},
  "geometry": {
    "type": "Point",
    "coordinates": [8.556902627782648, 50.04526696451549]
  }
}, {
  "type": "Feature",
  "id": "HKG",
  "properties": {},
  "geometry": {
    "type": "Point",
    "coordinates": [113.93338435186595, 22.312419994845527]
  }
}, {
  "type": "Feature",
  "id": "DEN",
  "properties": {},
  "geometry": {
    "type": "Point",
    "coordinates": [-104.67240855005912, 39.8521094050671]
  }
}, {
  "type": "Feature",
  "id": "SIN",
  "properties": {},
  "geometry": {
    "type": "Point",
    "coordinates": [103.99354166490136, 1.3542935982235846]
  }
}, {
  "type": "Feature",
  "id": "AMS",
  "properties": {},
  "geometry": {
    "type": "Point",
    "coordinates": [4.771980323493636, 52.30765071551501]
  }
}, {
  "type": "Feature",
  "id": "JFK",
  "properties": {},
  "geometry": {
    "type": "Point",
    "coordinates": [-73.79168731973864, 40.647672921902085]
  }
}, {
  "type": "Feature",
  "id": "MAD",
  "properties": {},
  "geometry": {
    "type": "Point",
    "coordinates": [-3.57607260933068, 40.47364340259443]
  }
}, {
  "type": "Feature",
  "id": "IST",
  "properties": {},
  "geometry": {
    "type": "Point",
    "coordinates": [28.82569216383493, 40.98330128056685]
  }
}, {
  "type": "Feature",
  "id": "PVG",
  "properties": {},
  "geometry": {
    "type": "Point",
    "coordinates": [121.78621380562439, 31.150633439684313]
  }
}, {
  "type": "Feature",
  "id": "SFO",
  "properties": {},
  "geometry": {
    "type": "Point",
    "coordinates": [-122.39433447948701, 37.61458701396866]
  }
}, {
  "type": "Feature",
  "id": "LAS",
  "properties": {},
  "geometry": {
    "type": "Point",
    "coordinates": [-115.14284782173708, 36.08561338005141]
  }
}, {
  "type": "Feature",
  "id": "MIA",
  "properties": {},
  "geometry": {
    "type": "Point",
    "coordinates": [-80.26496565324224, 25.793010380998638]
  }
}, {
  "type": "Feature",
  "id": "FCO",
  "properties": {},
  "geometry": {
    "type": "Point",
    "coordinates": [12.23570561512895, 41.79129547735239]
  }
}, {
  "type": "Feature",
  "id": "MCO",
  "properties": {},
  "geometry": {
    "type": "Point",
    "coordinates": [-81.2907857170215, 28.42831453051457]
  }
}, {
  "type": "Feature",
  "id": "SYD",
  "properties": {},
  "geometry": {
    "type": "Point",
    "coordinates": [151.18126459874733, -33.93640678137884]
  }
}, {
  "type": "Feature",
  "id": "YYZ",
  "properties": {},
  "geometry": {
    "type": "Point",
    "coordinates": [-79.62824975158615, 43.68075882983551]
  }
}, {
  "type": "Feature",
  "id": "SEA",
  "properties": {},
  "geometry": {
    "type": "Point",
    "coordinates": [-122.28821516254433, 47.447254854851195]
  }
}, {
  "type": "Feature",
  "id": "BOM",
  "properties": {},
  "geometry": {
    "type": "Point",
    "coordinates": [72.86520869504798, 19.092873887653894]
  }
}, {
  "type": "Feature",
  "id": "SVO",
  "properties": {},
  "geometry": {
    "type": "Point",
    "coordinates": [37.42135683619216, 55.96227062097577]
  }
}, {
  "type": "Feature",
  "id": "MEX",
  "properties": {},
  "geometry": {
    "type": "Point",
    "coordinates": [-99.08345785774453, 19.440932926269213]
  }
}, {
  "type": "Feature",
  "id": "CUN",
  "properties": {},
  "geometry": {
    "type": "Point",
    "coordinates": [-86.87973640933609, 21.04449063988976]
  }
}, {
  "type": "Feature",
  "id": "YYC",
  "properties": {},
  "geometry": {
    "type": "Point",
    "coordinates": [-114.01090844101515, 51.12673612021307]
  }
}, {
  "type": "Feature",
  "id": "EZE",
  "properties": {},
  "geometry": {
    "type": "Point",
    "coordinates": [-58.545878785639985, -34.818985057867664]
  }
}, {
  "type": "Feature",
  "id": "ARN",
  "properties": {},
  "geometry": {
    "type": "Point",
    "coordinates": [17.93077562438623, 59.654182566288185]
  }
}, {
  "type": "Feature",
  "id": "CPT",
  "properties": {},
  "geometry": {
    "type": "Point",
    "coordinates": [18.60286463168987, -33.973698821230485]
  }
}, {
  "type": "Feature",
  "id": "CAI",
  "properties": {},
  "geometry": {
    "type": "Point",
    "coordinates": [31.407928876106837, 30.106456323838387]
  }
}, {
  "type": "Feature",
  "id": "DEL",
  "properties": {},
  "geometry": {
    "type": "Point",
    "coordinates": [77.07460826710769, 28.565052009970586]
  }
}, {
  "type": "Feature",
  "id": "HNL",
  "properties": {},
  "geometry": {
    "type": "Point",
    "coordinates": [-157.90893254963794, 21.330396278752332]
  }
}, {
  "type": "Feature",
  "id": "GRU",
  "properties": {},
  "geometry": {
    "type": "Point",
    "coordinates": [-46.48364975982179, -23.42005154321623]
  }
}]*/











