$(function() {
  var OD_PAIRS = [];

  // global variable 'ports' holds airport and freeport locations
  var ports;

  var currentWidth = $('#map').width();
  var width = $(window).width();//938;
  var height = $(window).height();//620;

  var key_width = width;
  var key_height = height;

  /* some positioning relative to the navigation bar */
  var left_nav_item_pos = $("#search_nav_item").offset();

  var selected_vector_color = "#568eff";
  var label_font_size = '16px';
  var label_font_color = '#b7b7b7';
  var cta_font_color = "#fff";
  var initial_circle_r = 12;// this one stays the same
  var circle_r = 12;// this one updates
  var vector_arrow_scale = 0.4;
  var d3_scale_level = 1;
  var done_flight_counter = 0; // use this to calculate when to restart

  var countries_global;

  function start_flights(){
    //console.log("called start_flights...");
    var flight_counter = 0;
    setInterval(function() {
      //console.log("OD_PAIRS: ", OD_PAIRS[0]);
      if ( flight_counter < OD_PAIRS.length ){
        var od = OD_PAIRS[flight_counter];
        fly(od[0], od[1], od[2], od[4], od[5]);
        flight_counter++;
      };
    }, 50);
  };

  var projection = d3.geo
                     .stereographic()
                     .scale(400)//original 150, then 250
                     .translate([width / 3, height / 1.25]);

  var path = d3.geo
               .path()
               .pointRadius(2)
               .projection(projection);

  var svg = d3.select("#map")
              .append("svg")
              .attr("preserveAspectRatio", "xMidYMid")
              .attr("viewBox", "0 0 " + width + " " + height)
              .attr("width", currentWidth)
              .attr("height", currentWidth * height / width);



var g = svg.append("g")
          .attr("class", "map-g");

  // var artist_key = svg.append("g")
  //   .attr("class", "artist-key");
  // artist_key.append("circle")
  //   .attr("class", "artist-color")
  //   //.attr("cy", key_height - 122)
  //   .attr("cy", 70)
  //   .attr("cx", left_nav_item_pos.left+"px")
  //   .attr("r", 3)
  //   //.style("stroke", "blue")
  //   .style("stroke-width", "2");
  // artist_key.append("text")
  //   .attr("x", (left_nav_item_pos.left + 20) + "px")
  //   //.attr("y", key_height - 120)
  //   .attr("y", 73)
  //   .style("font-size", "12px")
  //   .style("color", label_font_color)
  //   .style("stroke", label_font_color)
  //   .style("fill", label_font_color)
  //   .style("stroke-width", "0.5")
  //   .text("Artist");
  //
  // var vector_key = svg.append("g")
  //   .attr("class", "vector-key")
  // vector_key.append("path")
  //   .attr("stroke", "#F1F100")
  //   .attr("stroke-width", "10px")
  //   .attr("fill", "rgba(0,0,0,0)")
  //   .attr("d", "M15.2,61.3l112,48 c0,0-20-30.7-20-48s20-48,20-48L15.2,61.3z")
  //   //.attr("transform", "translate("+ ( left_nav_item_pos.left - 5 ) +", "+ (key_height-107) +") scale("+ (0.08 / d3_scale_level) +")")
  //   .attr("transform", "translate("+ ( left_nav_item_pos.left - 5 ) +", "+ 87 +") scale("+ (0.08 / d3_scale_level) +")")
  // vector_key.append("text")
  //   .attr("x",  (left_nav_item_pos.left + 20) + "px")
  //   //.attr("y", key_height - 100)
  //   .attr("y", 95)
  //   .style("font-size", "12px")
  //   .style("color", label_font_color)
  //   .style("stroke", label_font_color)
  //   .style("fill", label_font_color)
  //   .style("stroke-width", "0.5")
  //   .text("Idea");
  //
  // var freeport_key = svg.append("g")
  //   .attr("class", "freeport-key");
  // freeport_key.append("circle")
  //   .attr("class", "freeport-color")
  //   .attr("cy", 116)
  //   .attr("cx", left_nav_item_pos.left+"px")
  //   .attr("r", 3)
  //   .style("stroke-width", "2");
  // freeport_key.append("text")
  //   .attr("x",  (left_nav_item_pos.left + 20) + "px")
  //   .attr("y", 119)
  //   .style("font-size", "12px")
  //   .style("color", label_font_color)
  //   .style("stroke", label_font_color)
  //   .style("fill", label_font_color)
  //   .style("stroke-width", "0.5")
  //   .text("Gallery");
  //
  //   var sonic_key = svg.append("g")
  //   .attr("class", "sonic-key");
  // sonic_key.append("circle")
  //   .attr("class", "freeport-color sonic")
  //   .attr("cy", 138)
  //   .attr("cx", left_nav_item_pos.left+"px")
  //   .attr("r", 3)
  //   .style("stroke-width", "2");
  // sonic_key.append("text")
  //   .attr("x",  (left_nav_item_pos.left + 20) + "px")
  //   .attr("y", 141)
  //   .style("font-size", "12px")
  //   .style("color", label_font_color)
  //   .style("stroke", label_font_color)
  //   .style("fill", label_font_color)
  //   .style("stroke-width", "0.5")
  //   .text("Sonic");

  /*var airport_key = svg.append("g")
    .attr("class", "airport-key");
  airport_key.append("circle")
    .attr("class", "airport-color")
    .attr("cy", 160)
    .attr("cx", left_nav_item_pos.left+"px")
    .attr("r", 3)
    .style("stroke-width", "2");
  airport_key.append("text")
    .attr("x",  (left_nav_item_pos.left + 20) + "px")
    .attr("y", 163)
    .style("font-size", "12px")
    .style("color", label_font_color)
    .style("stroke", label_font_color)
    .style("fill", label_font_color)
    .style("stroke-width", "0.5")
    .text("Airport");*/

function clear_selections(){
  // clear vectors
  d3.selectAll(".detail-container").style({'display':'none'});
  d3.selectAll(".flight-line").style({'stroke':'rgba(0,0,0,0)'});
  var all_vectors = d3.selectAll(".vector-arrow");
  all_vectors.style({'stroke':'#F1F100'});
  all_vectors.style({'fill':'rgba(0,0,0,0)'});
  // clear selected ports
  stop_pulse();
  d3.selectAll(".port-link").style("display", "none");
};

// global pulse function
function pulse() {
  var circle = g.selectAll(".port");
  //console.log(d3.select(this));
  (function repeat() {
    circle = circle.transition()
      .duration(2000)
      .attr('opacity', 1)
      //.attr("stroke-width", 20)
      .attr("r", circle_r)
      .transition()
      .duration(1000)
      .attr('opacity', 0.5)
      .attr("r", circle_r*2)
      .ease('sine')
      .each("end", repeat);
  })();
}

function stop_pulse(){
  var circle = g.selectAll(".port");
  circle = circle
    .transition()
    .duration(0)
    .attr('opacity', 1)
    .attr("r", circle_r)
};

function pulseEl(ID) {
  stop_pulse();
  var circle = g.selectAll(ID);
  (function repeat() {
    circle = circle.transition()
      .duration(500)
      .attr('opacity', 1)
      //.attr("stroke-width", 20)
      .attr("r", circle_r)
      .transition()
      .duration(1000)
      .attr('opacity', 0.5)
      .attr("r", circle_r*2)
      .ease('sine')
      .each("end", repeat);
  })();
}

function stop_color_pulse(){
  var cta = g.selectAll(".cta");
  cta = cta
    .transition()
    .duration(0)
    .attr('opacity', 0.8)
};
function pulseColor() {
  stop_color_pulse();
  var cta = g.selectAll(".cta");
  (function repeat() {
    //console.log("cta: ", cta);
      //if ( cta ){
        cta = cta.transition()
        .duration(500)
        .attr('opacity', 1)
        .transition()
        .duration(1000)
        .attr('opacity', 0.5)
        .ease('sine')
        .each("end", repeat);
      //};
  })();
};

function remove_transition(el) {
  el.transition().remove()
}


//========================================
// Zoom to center/point
//========================================

function interpolateZoom (translate, scale) {
    //console.log("in interpolate zoom");
    var self = this;
    return d3.transition().duration(350).tween("zoom", function () {
        var iTranslate = d3.interpolate(zoom.translate(), translate),
            iScale = d3.interpolate(zoom.scale(), scale);
        return function (t) {
            zoom
                .scale(iScale(t))
                .translate(iTranslate(t));
            zoomed();
        };
    });
}

function zoomClick() {
    //console.log("in zoom click");
    //var clicked = d3.event.target,
    var direction = 1,
        factor = 2,
        target_zoom = 1,
        center = [width / 2, height / 2],
        extent = zoom.scaleExtent(),
        translate = zoom.translate(),
        translate0 = [],
        l = [],
        view = {x: translate[0], y: translate[1], k: zoom.scale()};

   // d3.event.preventDefault();
    direction = (this.id === 'zoom_in') ? 1 : -1;
    target_zoom = zoom.scale() * (1 + factor * direction);

    if (target_zoom < extent[0] || target_zoom > extent[1]) { return false; }

    translate0 = [(center[0] - view.x) / view.k, (center[1] - view.y) / view.k];
    view.k = target_zoom;
    l = [translate0[0] * view.k + view.x, translate0[1] * view.k + view.y];

    view.x += center[0] - l[0];
    view.y += center[1] - l[1];

    interpolateZoom([view.x, view.y], view.k);
};

var global_translate = [0,0];
var global_scale = 1;




//========================================
// Mousewheel double click zoom
//========================================

var zoom = d3.behavior.zoom()
    .translate([0, 0])
    .scale(1)
    .scaleExtent([1, 8])
    .on("zoom", zoomed);


function zoomTo(location, scale) {
  var point = projection(location);
  return zoom
      .translate([width / 2 - point[0] * scale, height / 2 - point[1] * scale])
      .scale(scale);
}


function interpolateZoom (translate, scale) {
    var self = this;
    return d3.transition().duration(350).tween("zoom", function () {
        var iTranslate = d3.interpolate(zoom.translate(), translate),
            iScale = d3.interpolate(zoom.scale(), scale);
        return function (t) {
            global_translate = iTranslate(t);
            global_scale = iScale(t);
            zoom
                .scale(iScale(t))
                .translate(iTranslate(t));
            zoomed();
        };
    });
};

function zoomIn() {

  var clicked = d3.event.target,
        direction = 1,
        factor = 0.2,
        target_zoom = 1,
        center = [width / 2, height / 2],
        extent = zoom.scaleExtent(),
        translate = zoom.translate(),
        translate0 = [],
        l = [],
        view = {x: translate[0], y: translate[1], k: zoom.scale()};

    d3.event.preventDefault();
    direction = 1;// this is -1 for zoomOut
    target_zoom = zoom.scale() * (1 + factor * direction);

    if (target_zoom < extent[0] || target_zoom > extent[1]) { return false; }

    translate0 = [(center[0] - view.x) / view.k, (center[1] - view.y) / view.k];
    view.k = target_zoom;
    l = [translate0[0] * view.k + view.x, translate0[1] * view.k + view.y];

    view.x += center[0] - l[0];
    view.y += center[1] - l[1];

    interpolateZoom([view.x, view.y], view.k);

};

function zoomOut() {

  var clicked = d3.event.target,
        direction = 1,
        factor = 0.2,
        target_zoom = 1,
        center = [width / 2, height / 2],
        extent = zoom.scaleExtent(),
        translate = zoom.translate(),
        translate0 = [],
        l = [],
        view = {x: translate[0], y: translate[1], k: zoom.scale()};

    d3.event.preventDefault();
    direction = -1;// this is 1 for zoomIn
    target_zoom = zoom.scale() * (1 + factor * direction);

    if (target_zoom < extent[0] || target_zoom > extent[1]) { return false; }

    translate0 = [(center[0] - view.x) / view.k, (center[1] - view.y) / view.k];
    view.k = target_zoom;
    l = [translate0[0] * view.k + view.x, translate0[1] * view.k + view.y];

    view.x += center[0] - l[0];
    view.y += center[1] - l[1];

    interpolateZoom([view.x, view.y], view.k);

};
d3.select(".zoom-in").on("click", zoomIn);

d3.select(".zoom-out").on("click", zoomOut);

function zoomed() {

  if ( d3.event ){
    d3_event_translate = zoom.translate()//d3.event.translate;
    d3_event_scale = zoom.scale()//d3.event.scale;
  }else{
    d3_event_translate = global_translate;
    d3_event_scale = global_scale;
  };

  g.attr("transform", "translate(" + zoom.translate() + ")scale(" + zoom.scale() + ")");
  d3.selectAll(".countries").style("stroke-width", .5 / d3_event_scale + "px");

  d3_scale_level = d3_event_scale;

  d3.selectAll(".airport").attr("r", circle_r);
  d3.selectAll(".freeport").attr("r", circle_r);
  d3.selectAll(".artist").attr("r", circle_r);

  thin_stroke = .5 / d3_event_scale;
  if ( thin_stroke > .5 ){
    thin_stroke = .5;
  };
  med_stroke = 1 / d3_event_scale;
  if ( med_stroke > 1 ){
    med_stroke = 1;
  };

  d3.selectAll(".flight-line").style("stroke-width", med_stroke + "px");

  d3.selectAll(".port-link")
      .attr("transform", function(d) {
        return "translate(" + d.x + "," + d.y + ")scale("+(1/d3_event_scale)+")"
      })
      .style("stroke-width", thin_stroke + "px");


  vectorForeignObjects = d3.selectAll("foreignObject.vector-foreignObject");

  vectorForeignObjects.attr("transform", function(){
    var dataX = d3.select(this).attr("data-x");
    var dataY = d3.select(this).attr("data-y");

    var detail_container = d3.select(this).select(".detail-container");
    detail_container.style("transform", "translate("+ 100 +","+ 100 +"), scale(1)");

  });

  var port_stroke_width = 3/d3_event_scale;
  var artist_stroke_width = 1/d3_event_scale;

  if ( port_stroke_width < 1.5 ){
    port_stroke_width = 1.5;
  }
  if ( artist_stroke_width < 1 ){
    artist_stroke_width = 1;
  }

  d3.selectAll(".airport").style("stroke-width", port_stroke_width + "px");
  d3.selectAll(".freeport").style("stroke-width", port_stroke_width + "px");
  d3.selectAll(".artist").style("stroke-width", artist_stroke_width + "px");

  circle_r = ( initial_circle_r / d3_event_scale );// circle_r is a global variable that sets circle radii, changed based on a constant that isn't updated

  var scale_stroke = 10 / d3_event_scale;

  if ( scale_stroke >= 5 ){
    d3.selectAll(".vector-arrow").style("stroke-width", scale_stroke + "px");
  }else{
    d3.selectAll(".vector-arrow").style("stroke-width", 5 + "px");
  };

  d3.selectAll(".label-test").attr("transform", "scale(" + (0.5/d3_event_scale) + ")");
  d3.selectAll(".vector-arrow").attr("transform", "scale(" + (vector_arrow_scale/d3_event_scale) + ")");

};


  var gradient = svg.append("defs")
  .append("linearGradient")
    .attr("id", "gradient")
    .attr("x1", "50%")
    .attr("y1", "0%")
    .attr("x2", "0%")
    .attr("y2", "0%")
    .attr("spreadMethod", "pad");

  gradient.append("stop")
      .attr("offset", "0%")
      .attr("stop-color", "blue")
      .attr("stop-opacity", 0);

  gradient.append("stop")
      .attr("offset", "100%")
      .attr("stop-color", "green")
      .attr("stop-opacity", 0.5);

  var airportMap = {};

  function transition(plane, route, label, duration) {
    var l = route.node().getTotalLength();
    //console.log("duration: ", duration);
    var duration_adjust = 0.0005;//50;
    duration = duration*duration_adjust

    var label = plane.select(".detail-container")
    var plane = plane.select(".vector");

    plane.transition()
        .delay(function(d, i) { return 1000 })
        .duration(l * duration)
        .ease("linear")
        .attrTween("transform", delta(plane, route.node()))
        .each("end", function() { done_flight_counter += 1; if(done_flight_counter >= OD_PAIRS.length){ done_flight_counter=0; start_flights(); }; route.remove(); })// increment flight counter then remove
        .remove();

      label.transition()
        .duration(l * duration)
        .ease("linear")
        .attrTween("transform", deltaText(label, route.node()))
        .tween("text", function() {
            return function(t) {
                var l = route.node().getTotalLength();
                var p = route.node().getPointAtLength(t * l);
                var t2 = Math.min(t + 0.05, 1);
                var p2 = route.node().getPointAtLength(t2 * l);
                var coords = projection.invert([p2.x, p2.y]);
                var lat = coords[1].toFixed(4);
                var lng = coords[0].toFixed(4);

                var d3_this = d3.select(this);

                var flight_id = d3_this.attr("id").split("-")[1]
                d3.select("#flight-listing-"+flight_id).attr("data-lat", lat);
                d3.select("#flight-listing-"+flight_id).attr("data-lng", lng);

                d3_this.select(".lat").html( lat );
                d3_this.select(".lng").html( lng );
                d3_this.attr("data-lat", lat);
                d3_this.attr("data-lng", lng);
            };
        })
        .each("end", function() {
          remove_transition(d3.select(this).select(".cta"));
          route.remove();
        })
        .remove();

  }

  // returns a translate for the plane along a path, including scaling and rotation
  function delta(plane, path) {
    var l = path.getTotalLength();
    var plane = plane;
    var vector_arrow = plane.select(".vector-arrow");
    //var start = plane.attr("start");
    return function(i) {
      return function(t) {

        var p = path.getPointAtLength(t * l);

        var t2 = Math.min(t + 0.005, 1);
        var p2 = path.getPointAtLength(t2 * l);

        var x = p2.x - p.x;
        var y = p2.y - p.y;

        var r = 180 - Math.atan2(-y, x) * 180 / Math.PI;
        var s = 0.5;

        if ( t > 0 ){
          vector_arrow.style("display", "inline-block");
        };

        return "translate(" + p.x + "," + p.y + ") scale(" + s + ") rotate(" + r + ")";

      }
    }
  }

  // only translates doesn't rotate or scale
  function deltaText(plane, path) {
    var l = path.getTotalLength();
    return function(i) {
      return function(t) {

        var p = path.getPointAtLength(t * l);

        var t2 = Math.min(t + 0.05, 1);
        var p2 = path.getPointAtLength(t2 * l);

        var x = p2.x - p.x;
        var y = p2.y - p.y;

        plane.attr("data-x", p.x);
        plane.attr("data-y", p.y);

        var s = Math.min(Math.sin(Math.PI * t) * 0.7, 0.3);

        return "translate(" + p.x + "," + p.y + ") scale( "+ (1/d3_scale_level) +" )"
      }
    }
  }

/* ---------------------------- */
  // this is made up by me...
  function layoutPlanes(plane, route){

    var l = route.node().getTotalLength();
    var time = plane.attr("time");
    plane.transition()
        .duration(l * time)
        .attrTween("transform", delta(plane, route.node()))
        .each("end", function() { route.remove(); })
        .remove();

  };
/* ---------------------------- */

  function fly(origin, destination, flightCode, duration, detailsObj) {

    //console.log("detailsObj: ", detailsObj);

    if ( detailsObj["upgraded"] ){

      var original_origin = origin;
      var original_destination = destination;

      destination = original_origin;
      origin = original_destination;

    };

    var route = g.append("path")
                   .datum({type: "LineString", coordinates: [airportMap[origin], airportMap[destination]]})
                   .attr("class", "route")
                   .attr("id", "route-"+detailsObj.flightID)
                   .attr("d", path);

    var line = g.append("path")
                   .datum({type: "LineString", coordinates: [airportMap[origin], airportMap[destination]]})
                   .attr("d", function(d){return path(d)})
                   .attr("stroke", "transparent")
                   .attr("class", "destination-"+detailsObj.destinationID+ " flight-line")
                   .attr("id", "line-"+detailsObj.flightID)
                   .attr("stroke-width", "1")
                   .attr("fill", "none");


    /* random starting point between 1 & 100... so % */
    var start = Math.floor((Math.random() * 10) + 1);

    var vectorGroup = g.append("g")
      .attr("class", "vector-group")
      .attr("id", "vectorGroup-"+detailsObj.flightID)

    var vector = vectorGroup.append("g")
      .attr("class", "vector")
      .style("padding", "10px;");

        // SVG text
      var label = vectorGroup.append("g")
      .attr("class", "detail-container")
      .attr("id", "label-"+detailsObj.flightID)
      .style("display", "none")
      .attr("data-link", detailsObj.galleryLink)

      label.append("text")
      .attr("class", "flightName")
      .style("font", ""+label_font_size+" 'isocpeur'")
      .style("color", label_font_color)
      .style("stroke", label_font_color)
      .style("fill", label_font_color)
      .style("stroke-width", "0.5")
      .attr("x", "0px")
      .attr("y", "0px")
      .text(detailsObj.flightNumber + " " + detailsObj.surname );

      label.append("text")
      .attr("class", "title")
      .style("font", ""+label_font_size+" 'isocpeur'")
      .style("color", label_font_color)
      .style("stroke", label_font_color)
      .style("fill", label_font_color)
      .style("stroke-width", "0.5")
      .attr("x", "0px")
      .attr("y", "14px")
      .text( detailsObj.title );

      label.append("text")
      .attr("class", "lat")
      .style("font", ""+label_font_size+" 'isocpeur'")
      .style("color", label_font_color)
      .style("stroke", label_font_color)
      .style("fill", label_font_color)
      .style("stroke-width", "0.5")
      .attr("x", "0px")
      .attr("y", "28px")
      .text("LAT");

      label.append("text")
      .attr("class", "lng")
      .style("font", ""+label_font_size+" 'isocpeur'")
      .style("color", label_font_color)
      .style("stroke", label_font_color)
      .style("fill", label_font_color)
      .style("stroke-width", "0.5")
      .attr("x", "0px")
      .attr("y", "42px")
      .text("LNG");

      label.append("text")
      .attr("class", "from")
      .style("font", ""+label_font_size+" 'isocpeur'")
      .style("color", label_font_color)
      .style("stroke", label_font_color)
      .style("fill", label_font_color)
      .style("stroke-width", "0.5")
      .attr("x", "0px")
      .attr("y", "56px")
      .text(detailsObj.from);

      label.append("text")
      .attr("class", "to")
      .style("font", ""+label_font_size+" 'isocpeur'")
      .style("color", label_font_color)
      .style("stroke", label_font_color)
      .style("fill", label_font_color)
      .style("stroke-width", "0.5")
      .attr("x", "0px")
      .attr("y", "70px")
      .text(detailsObj.to);

      label.append("text")
      .attr("class", "cta animated infinite")
      .style("font", ""+label_font_size+" 'isocpeur'")
      .style("color", cta_font_color)
      .style("stroke", "#F1F100")
      .style("fill", "#F1F100")
      .style("stroke-width", "0.5")
      .attr("x", "0px")
      .attr("y", "84px")
      .text("View");

      var start_point = route.node().getPointAtLength(0);

      vector.append("path")
        .attr("class", "vector-arrow vector-destination-"+detailsObj.destinationID)
        .attr("id", "vector-arrow-"+detailsObj.flightID)
        .style("cursor", "pointer")
        .style("display", "none")
        .attr("stroke", "#F1F100")
        .attr("stroke-width", "10px")
        .attr("fill", "rgba(0,0,0,0)")
        .attr("d", "M15.2,61.3l112,48 c0,0-20-30.7-20-48s20-48,20-48L15.2,61.3z")
        .attr("transform", "scale("+ (vector_arrow_scale / d3_scale_level) +")")

    /* modify vector if its featured */
    if ( detailsObj.featured ){
      vector.selectAll("path")
        .style("stroke", selected_vector_color)
        .style("fill", selected_vector_color);

      line.style("stroke", selected_vector_color);

      label.style("display", "inline-block");

    };

      vectorGroup.on('click', function(d){

        clear_selections();

        var vectorGroup = d3.select(this);
        var baseID = vectorGroup.attr("id").split("-")[1];

        var line = d3.select("#line-"+baseID);

        line.style("stroke", selected_vector_color);

        var vector_arrow = d3.select(this).select(".vector-arrow");

        vector_arrow.style("stroke", selected_vector_color);
        vector_arrow.style("fill", selected_vector_color);

        var detail_container = d3.select(this).select(".detail-container");
        detail_container.style("display", "inline-block");
        var coords = [detail_container.attr("data-lng"),detail_container.attr("data-lat")]

        svg.transition()
          .duration(1000)
          .call(zoomTo(coords, 6).event)

        //pulseColor();

      });
      // there's also a reset type function defined below, that clears selection on body click

      label.on("click", function(){
        var link = d3.select(this).attr("data-link");
        window.location.href=link;
      });



   /* This triggers the animation of the flying planes */
    transition(vectorGroup, route, label, duration);
    /* ---------------------------- */
    //layoutPlanes(plane, route);
    /* ---------------------------- */
  }

  function drawPath (origin, destination){
    //console.log("points...", [points]);
    var path = svg.append("path")
    .data([points])
    .attr("d", d3.svg.line()
    .tension(0) // Catmull–Rom
    .interpolate("cardinal-closed"));
  };

  function loaded(error, countries, airports, apiData) {
    //console.log("ports", apiData);

    ports = apiData["ports"]
    console.log("ports: ", ports);
    console.log("connections: ", apiData["connections"]);
    //ports = check_overlapped_ports(ports);

    OD_PAIRS = apiData["connections"]

    // drawing the countries
    countries_global = g.append("g")
       .attr("class", "countries")
       .selectAll("path")
       .data(topojson.feature(countries, countries.objects.countries).features)
       .enter()
       .append("path")
       .attr("d", path);

    // setting x and y transform attributes in dataset.
    for ( j=0; j<ports.length; j++ ){
      var xx = projection([ ports[j].geometry.coordinates[0], ports[j].geometry.coordinates[1] ])[0];
      var yy = projection([ ports[j].geometry.coordinates[0], ports[j].geometry.coordinates[1] ])[1];

      ports[j]["radius"] = 12;
      ports[j].x = xx;
      ports[j].y = yy;

    };

// =============================================================================
// START Collisions
// =============================================================================

    var enable_collisions = false;

    if ( enable_collisions ){

      function collide(node) {
        var r = node.radius + 16,
            nx1 = node.x - r,
            nx2 = node.x + r,
            ny1 = node.y - r,
            ny2 = node.y + r;

        return function(quad, x1, y1, x2, y2) {
          //console.log( "quad !== node?", quad.point !== node )
          if (quad.point && (quad.point !== node)) {
            var x = node.x - quad.point.x,
                y = node.y - quad.point.y,
                l = Math.sqrt(x * x + y * y),
                r = node.radius + quad.point.radius;
                // points overlap exactly
                if ( x==0 && y==0 ){
                  x = node.x - r,
                  y = node.y - r,
                  l = r - 0.0000001,
                  r = node.radius + quad.point.radius;
                  //console.log("l: "+l);
                };
            if (l < r) {
                //console.log("change");
                l = (l - r) / l * .5;
                console.log("l: " + l);
                node.x -= x *= l;
                node.y -= y *= l;
                console.log("new x: ", node.x);
                console.log("new y: ", node.y);
                quad.point.x += x;
                quad.point.y += y;
            }
          }
          //console.log("node f x: " + node.x + " y: " + node.y);
          return x1 > nx2
              || x2 < nx1
              || y1 > ny2
              || y2 < ny1;
        };
      }

      // was on tick, but now just iterate 10 times
      for ( var ii=0; ii<10; ii++ ){
        var q = d3.geom.quadtree(ports),
        i = 0,
        n = ports.length;
        while (++i < n) {
          q.visit(collide(ports[i]));
        }
      };

  };

// =============================================================================
// END Collisions
// =============================================================================


    // add a circle at each port
    //console.log("PORTS: ", ports);
    var enterSelection = g.selectAll(".port")
      .data(ports)
      .enter()
      .append("g")
      .attr("class", "port-g");

    enterSelection
      .append("circle")
      .attr("r", initial_circle_r)
      .attr("class", function(d){ return "port ground " + d.properties.portType + " " + d.properties.sonic})
      .attr("data-lat", function(d){return d.geometry.coordinates[1]})
      .attr("data-lng", function(d){return d.geometry.coordinates[0]})
      .attr("id", function(d) { return d.id;})
      //.attr("stroke-width", 20)
      //.attr("stroke", "red")
      .attr("fill", "rgba(0,0,0,0)")
      .style("cursor", "pointer")
      .attr("transform", function(d) {
        //console.log("COORDS: ", d.geometry.coordinates[0]);
        return "translate(" + d.x + "," + d.y + ")"
      });
      //.each(pulse);

      var text_container = enterSelection.append("g")
        .attr("class", "port-link")
        .attr("id", function(d){ return "label-"+d.id })
        .style("display", "none")
        .style("cursor", "pointer")
        .attr("data-link", function(d){ return d.properties.portLink })
        .attr("transform", function(d) {
          return "translate(" + d.x + "," + d.y + ")"
        });

      text_container.append("text")
      .style("font", ""+label_font_size+" 'isocpeur'")
      .attr("class", "flightCode")
      .style("color", label_font_color)
      .style("stroke", label_font_color)
      .style("fill", label_font_color)
      .style("stroke-width", "0.5")
      .attr("transform", "translate("+ (circle_r + 10) +", 0)")
      .text(function(d){ return d.properties.portFlightNumber });

      text_container.append("text")
      .style("font", ""+label_font_size+" 'isocpeur'")
      .attr("class", "flightName")
      .style("color", label_font_color)
      .style("stroke", label_font_color)
      .style("fill", label_font_color)
      .style("stroke-width", "0.5")
      .attr("transform", "translate("+ (circle_r + 10) +",14)")
      .text(function(d){ return d.properties.portTitle });

      text_container.append("text")
      .style("font", ""+label_font_size+" 'isocpeur'")
      .attr("class", "title")
      .style("color", label_font_color)
      .style("stroke", label_font_color)
      .style("fill", label_font_color)
      .style("stroke-width", "0.5")
      .attr("transform", "translate("+ (circle_r + 10) +",28)")
      .text(function(d){ return d.properties.portLat });

      text_container.append("text")
      .style("font", ""+label_font_size+" 'isocpeur'")
      .attr("class", "lat")
      .style("color", label_font_color)
      .style("stroke", label_font_color)
      .style("fill", label_font_color)
      .style("stroke-width", "0.5")
      .attr("transform", "translate("+ (circle_r + 10) +",42)")
      .text(function(d){ return d.properties.portLng });

      text_container.append("text")
      .style("font", ""+label_font_size+" 'isocpeur'")
      .attr("class", "lng")
      .style("color", label_font_color)
      .style("stroke", label_font_color)
      .style("fill", label_font_color)
      .style("stroke-width", "0.5")
      .attr("transform", "translate("+ (circle_r + 10) +",56)")
      .text(function(d){ return d.properties.portLocality });

      text_container.append("text")
      .style("font", ""+label_font_size+" 'isocpeur'")
      .attr("class", "from")
      .style("color", label_font_color)
      .style("stroke", label_font_color)
      .style("fill", label_font_color)
      .style("stroke-width", "0.5")
      .attr("transform", "translate("+ (circle_r + 10) +",70)")
      .text(function(d){ return d.properties.portCountry });

      text_container.append("text")
      .style("font", ""+label_font_size+" 'isocpeur'")
      .attr("class", function(d){ if(d.properties.portType == "artist"){return "cta animated infinite"}else{ return "hidden" }})
      .style("color", "#ce2d2d")
      .style("stroke", "#ce2d2d")
      .style("fill", "#ce2d2d")
      .style("stroke-width", "0.5")
      .attr("transform", "translate("+ (circle_r + 10) +",84)")
      .text("View");

      d3.selectAll(".port").on("click", function(){
        clear_selections();
        var d3_this = d3.select(this);
        var ID = d3_this.attr("id");
        if ( ID.indexOf("artist") > -1 ){
          pulseEl("#"+ID);
        };
        d3.select("#label-"+ID).style("display", "inline-block");
        var ID_num = ID.split("-")[1];
        d3.selectAll(".destination-"+ID_num).style("stroke", selected_vector_color);
        var vectors = d3.selectAll(".vector-destination-"+ID_num);
        vectors.style("stroke", selected_vector_color);
        vectors.style("fill", selected_vector_color);

        var coords = [ d3_this.attr("data-lng"), d3_this.attr("data-lat")];
        svg.transition()
        .duration(2000)
        .call(zoomTo(coords, 6).event)

      });
      d3.selectAll(".port-link").on("click", function(){
        var link = d3.select(this).attr("data-link");
        if ( link ){
          window.location.href = link;
        }
      });

    svg.call(zoom);

    for (i in ports) {
      airportMap[ports[i].id] = ports[i].geometry.coordinates;
    };

    start_flights();

    d3.select("#showAirspace").on("click", function(d){
        clear_selections();
        d3.selectAll(".port-link")
          .style("display", "none");
        d3.selectAll(".vector-group")
          .style("display", "inline-block");
        d3.selectAll(".ground")
          .style("display", "none");
      });
      d3.select("#showGround").on("click", function(d){
        clear_selections();
        d3.selectAll(".port-link")
          .style("display", "none");
        d3.selectAll(".ground")
          .style("display", "inline-block");
        d3.selectAll(".vector-group")
          .style("display", "none");
        d3.selectAll(".flight-line")
          .style("display", "none");
      });
      d3.select("#showAll").on("click", function(d){
        clear_selections();
        d3.selectAll(".port-link")
          .style("display", "none");
        d3.selectAll(".vector-group")
          .style("display", "inline-block");
        d3.selectAll(".ground")
          .style("display", "inline-block");
      });
      d3.select("#showAirport").on("click", function(d){
        clear_selections();
        d3.selectAll(".port-link")
          .style("display", "none");
        d3.selectAll(".freeport")
          .style("display", "none");
        d3.selectAll(".artist")
          .style("display", "none");
        d3.selectAll(".vector-group")
          .style("display", "none");
        d3.selectAll(".flight-line")
          .style("display", "none");

        d3.selectAll(".airport")
          .style("display", "inline-block");

      });
      d3.select("#showFreeport").on("click", function(d){
        clear_selections();
        d3.selectAll(".port-link")
          .style("display", "none");
        d3.selectAll(".airport")
          .style("display", "none");
        d3.selectAll(".artist")
          .style("display", "none");
        d3.selectAll(".vector-group")
          .style("display", "none");
        d3.selectAll(".flight-line")
          .style("display", "none");

        d3.selectAll(".freeport")
          .style("display", "inline-block");
      });
      d3.select("#showArtist").on("click", function(d){
        clear_selections();
        d3.selectAll(".port-link")
          .style("display", "none");
        d3.selectAll(".airport")
          .style("display", "none");
        d3.selectAll(".freeport")
          .style("display", "none");
        d3.selectAll(".vector-group")
          .style("display", "none");
        d3.selectAll(".flight-line")
          .style("display", "none");

        d3.selectAll(".artist")
          .style("display", "inline-block");
      });

      d3.selectAll(".waiting-to-take-off-listing").on("click", function(e){
        //console.log("href === ", $(this).attr("href"));
        //console.log("title === ", $(this).data("title"));

        var link = $(this).attr("href");
        var title = $(this).data("title");

        clear_selections();
        d3.selectAll(".port-link")
          .style("display", "none");
        var el = d3.select(this);

        var userID = el.attr("data-user-id");
        pulseEl("#artist-"+userID);

        var label_container = d3.select("#label-artist-"+userID).style("display", "inline-block");
        label_container.attr("data-link", link);
        var flightName = label_container.selectAll(".flightName");
        flightName.text(title);
        //$("#label-artist-"+userID).children("flightName").text(title);

        var coords = [ el.attr("data-lng"), el.attr("data-lat")];
        svg.transition()
        .duration(2000)
        .call(zoomTo(coords, 6).event)

        if ( width > 600 ){
          d3.event.preventDefault();
        };

      });

      d3.selectAll(".flight-listing").on("click", function(){
        clear_selections();
        d3.selectAll(".port-link")
          .style("display", "none");
        var el = d3.select(this);

        var flightID = el.attr("data-flight-id");

        var vector = d3.select("#vector-arrow-"+flightID);
        vector.style("stroke", selected_vector_color);
        vector.style("display", "inline-block");
        vector.style("stroke", selected_vector_color);

        d3.select("#label-"+flightID).style("display", "inline-block");
        d3.select("#line-"+flightID).style("stroke", selected_vector_color);

        d3.select("#label-artist-"+flightID).style("display", "inline-block");

        var coords = [ el.attr("data-lng"), el.attr("data-lat")];
        svg.transition()
        .duration(2000)
        .call(zoomTo(coords, 6).event)

        if ( width > 600 ){
          d3.event.preventDefault();
        };

      });


  };

  queue().defer(d3.json, "/static/data/countries2.topo.json")
         .defer(d3.json, "/static/data/airports2.topo.json")
         .defer(d3.json, "/api/gallery/"+gallery_flightCode+"/topojson")
         .await(loaded);

  $(window).resize(function() {
    currentWidth = $("#map").width();
    svg.attr("width", currentWidth);
    svg.attr("height", currentWidth * height / width);
  });


// end of the self calling function

});
