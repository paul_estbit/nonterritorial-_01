

// =================================================================
//  UTC clock
// =================================================================

function startTime() {
    var today = new Date();

    var h = today.getUTCHours();
    var m = today.getUTCMinutes();
    var s = today.getSeconds();
    
    m = checkTime(m);
    s = checkTime(s);

    var $clock = $(".utc_clock");

    if ( $clock.length ){
    	document.getElementById('utc_clock').innerHTML = "UTC " + h + ":" + m;//"UTC "+hh+":"+mm;//h + ":" + m + ":" + s;
    	var t = setTimeout(startTime, 500);
    };
}
function checkTime(i) {
    if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
}
$("document").ready(function(){
	startTime();
});


// =================================================================
//  Back Button
// =================================================================
function goBack() {
    window.history.back();
}


$("body").on("click", ".save-slideshow", function(e){
	e.preventDefault();
	var $this = $(this);
	var slideShowID = $this.data("slideshow-id");
	//console.log("slideShowID: ", slideShowID);

	function success(data){
		if ( data["savedSlideShowID"] ){
			$this.children(".check").removeClass("hidden");
			$this.text("Saved");
		}
	};

	$.ajax({
		url: "/api/save-slideshow",
		type: "post",
		data: {slideShowID: slideShowID},
		success: success
	}).fail(function(){
		console.log("couldn't save slideshow... apologies")
	});	
});


// =================================================================
//  me.html - user password/email change
// =================================================================

$("body").on("submit", ".changeSettingsForm", function(e){
	e.preventDefault();
	var $form = $(this);
	var method = $form.attr("method");
	var action = $form.attr("action");
	var success = function(data){
		if ( data["message"] == "success" ){
			console.log(".................. e");
			//$("#savedModal").openModal();
			feedback_overlay("Changed", 1000);
		}else{
			//$errorModal = $("#errorModal");
			var errors = data["errors"];
			$errorMessage = $form.find(".errorMessage");
			for ( var i=0; i<errors.length; i++ ){
				$errorMessage.html("");
				$errorMessage.append(errors[i]+"<br>");
			};
			//$errorModal.openModal();
			feedback_overlay("There was a problem.", 2000);
		};
	};
	submit_form_ajax($form, method, action, success);//ajax.js
});

// =================================================================
//  Slideshow - user editing etc.
// =================================================================


function next_slide(){
	console.log("go to next slide...");
	$(".slick-slider").slick('slickNext');

}
function prev_slide(){
	console.log("go to next slide...");
	$(".slick-slider").slick('slickPrev');
}

/* slide types to add dynamically */

/*function text_slide(slideShowID, slideOrder){
	var slide_type = "text";
	var text_slide = '<div class="flex-container-center">'+
				  	'<div class="flex-container-center">'+
				  		'<div class="slide-content-container flex-item center slide-edit">'+
			  				'<h1 class="canEdit edit-title">Title</h1>'+
			  				'<div class="canEdit edit-description">Subtitle</div>'+
			  				'<form method="post" action="/me/save-slide" class="slide-input-container hidden">'+
				  				'<input type="hidden" name="slideTitle" class="title-input"></input>'+
				  				'<input type="hidden" name="slideDescription" class="description-input"></input>'+
				  				'<input type="hidden" name="slideShowID" class="slideShowID" value="'+slideShowID+'"></input>'+
				  				'<input type="hidden" name="slideID" value=""></input>'+
				  				'<input type="hidden" name="slideType" value="'+slide_type+'"></input>'+
				  				'<input type="hidden" name="slideOrder" value="'+slideOrder+'"></input>'+
					  		'</form>'+
				  		'</div>'+
				  		'<div class="slide-done-edit content-save-btn btn-flat hidden">done editing</div>'+
				  	'</div>'+
				  '</div>';

  	return text_slide

};*/

function save_blank_slide(slideShowID, slideType, videos, slideOrder){

	var success = function(data){
		console.log("success:", data);
		next_slideOrder = parseInt(data['slideOrder']) + 1;
		$(".slick-slider").data("slide-order", next_slideOrder);

		if ( data["slideType"] == "text" ){
			new_slide = text_slide(slideShowID, next_slideOrder, data["slideID"]);
  		}else if (data["slideType"] == "image"){
  			new_slide = image_slide(slideShowID, next_slideOrder, data["slideID"]);
  		}else if(data["slideType"] == "video"){
  			new_slide = video_slide(slideShowID, next_slideOrder, videos, data["slideID"]);
  		};

  		var preview_slide = '<div id="slide-reorder-'+data["slideID"]+'" class="slide-preview slide-preview-sortable" data-index="'+data["slideOrder"]+'" data-id="'+data["slideID"]+'">'+
				'<p>-</p>'+
				'<button type="button" class="btn-flat delete-slide slide-thumb-btn" data-slide-id="'+data["slideID"]+'"><i class="ion-trash-a"></i></button>'+
				'<button type="button" class="btn-flat go-to-slide slide-thumb-btn" data-slide-id="'+data["slideID"]+'" data-slide-order="'+data["slideOrder"]+'"><i class="ion-forward"></i></button>'+
	  		'</div>';


		$("#sortable").append(preview_slide);

		$(".slick-slider").slick('slickAdd', new_slide);

		if ( data["slideType"] == "video" ){
			console.log("init material select video selection");
			$('select').material_select();
		};

		if (data["slideType"] == "image"){
			console.log("init dropzone - dropzone ID: #image-slide-"+data["slideID"]);
			Dropzone.autoDiscover = false;
			DZ_instances["image-slide-"+data["slideID"]] = $("#image-slide-"+data["slideID"]).dropzone({ 
				url: "/me/save-slide" ,
				init: function() {
			    	this.on("success", function(file, DZdata) {
				    	console.log(DZdata);
				    	console.log("Added file.");
				    	this.removeFile(file);
				    	$("#image-slide-"+DZdata["slideID"]).css("background-image", "url(" + DZdata["image_url"] + ")");
				    	$("#slide-reorder-"+DZdata["slideID"]).css("background-image", "url(" + DZdata["image_url"] + ")");
			    	});
			  	}
			});
		}

		// in ajax.js
		//feedback_overlay("Added New Slide", 1000);
		console.log("GO TO SLIDE: ", data["slideOrder"]);
		$(".slick-slider").slick("slickGoTo", data["slideOrder"]);
	};
	
	$.ajax({
		url: "/me/save-slide",
		"method": "post",
		data: {slideShowID: slideShowID, slideType: slideType, slideOrder: slideOrder},
		success: success
	}).fail(function(){
		console.log("couldn't add a new slide");
	});
};

function text_slide(slideShowID, slideOrder, slideID){
	slide_type = "text";

	var text_slide = '<div id="edit-slide-'+slideID+'" class="">'+
		  	'<div class="flex-container-center">'+
		  		'<div class="slide-content-container flex-item center">'+
	  				'<form id="slideShowTitle-'+slideID+'" class="slide-form" action="/me/save-slide" method="post">'+
				      	'<div class="row">'+
					        '<div class="col s12 center">'+
					          	'<textarea id="description-'+slideID+'" name="slideDescription" class="materialize-textarea"></textarea>'+
		          				'<label for="description-'+slideID+'">Text</label>'+
					        '</div>'+
				      	'</div>'+
				      	'<div class="row">'+
					        '<div class="input-field col s12">'+
					        	'<input type="hidden" name="slideShowID" class="slideShowID" value="'+slideShowID+'"></input>'+
				  				'<input type="hidden" name="slideID" value="'+slideID+'"></input>'+
				  				'<input type="hidden" name="slideType" value="text"></input>'+
				  				'<input type="hidden" name="slideOrder" value="'+slideOrder+'"></input>'+
					          	'<button type="submit" class="btn-flat">Save</button>'+
					        '</div>'+
				      	'</div>'+
	  				'</form>'+
		  		'</div>'+
		  	'</div>'+
	  	'</div>';

	/*var text_slide = '<div id="edit-slide-'+slideOrder+'" class="">'+
		  	'<div class="flex-container-center">'+
		  		'<div class="slide-content-container flex-item center">'+
	  				'<form id="slideShowTitle-'+slideOrder+'" class="slide-form" action="/me/save-slide" method="post">'+
				      	'<div class="row">'+
					        '<div class="col s12 center">'+
					          	'<textarea id="description-'+slideOrder+'" name="slideDescription" class="materialize-textarea"></textarea>'+
		          				'<label for="description-'+slideOrder+'">Text</label>'+
					        '</div>'+
				      	'</div>'+
				      	'<div class="row">'+
					        '<div class="input-field col s12">'+
					        	'<input type="hidden" name="slideShowID" class="slideShowID" value="'+slideShowID+'"></input>'+
				  				'<input type="hidden" name="slideID" value=""></input>'+
				  				'<input type="hidden" name="slideType" value="text"></input>'+
				  				'<input type="hidden" name="slideOrder" value="'+slideOrder+'"></input>'+
					          	'<button type="submit" class="btn-flat">Save</button>'+
					        '</div>'+
				      	'</div>'+
				      	'<div id="addSlideBtns-'+slideOrder+'" class="addSlideBtns hidden">'+
			      			'<span class="btn-flat add-new-text-slide">New Text</span>'+
			      			'<span class="btn-flat add-new-image-slide">New Image</span>'+
			      			'<span class="btn-flat add-new-video-slide">New Video</span>'+
				      	'</div>'+
	  				'</form>'+
		  		'</div>'+
		  	'</div>'+
	  	'</div>';*/

  	return text_slide
};

function image_slide(slideShowID, slideOrder, slideID){
	var slide_type = "image";

	var image_slide = '<div id="edit-slide-'+slideID+'" class="flex-container-center">'+
				  	'<div class="flex-container-center">'+
				  		'<div class="slide-content-container flex-item center slide-edit">'+
			  				'<form id="image-slide-'+slideID+'" method="post" action="/me/save-slide" class="dropzone slide-dropzone slide-input-container">'+
				  				'<input type="hidden" name="slideShowID" class="slideShowID" value="'+slideShowID+'"></input>'+
				  				'<input type="hidden" name="slideID" value="'+slideID+'"></input>'+
				  				'<input type="hidden" name="slideType" value="'+slide_type+'"></input>'+
				  				'<input type="hidden" name="slideOrder" value="'+slideOrder+'"></input>'+
					  		'</form>'+
					  		'<div id="addSlideBtns-'+slideID+'" class="addSlideBtns hidden">'+
				      			'<span class="btn-flat add-new-text-slide">New Text</span>'+
				      			'<span class="btn-flat add-new-image-slide">New Image</span>'+
				      			'<span class="btn-flat add-new-video-slide">New Video</span>'+
				      		'</div>'+
				  		'</div>'+
				  	'</div>'+
				  '</div>';

	/*var image_slide = '<div id="edit-slide-'+slideOrder+'" class="flex-container-center">'+
				  	'<div class="flex-container-center">'+
				  		'<div class="slide-content-container flex-item center slide-edit">'+
			  				'<form id="image-slide-'+slideOrder+'" method="post" action="/me/save-slide" class="dropzone slide-dropzone slide-input-container">'+
				  				'<input type="hidden" name="slideShowID" class="slideShowID" value="'+slideShowID+'"></input>'+
				  				'<input type="hidden" name="slideID" value=""></input>'+
				  				'<input type="hidden" name="slideType" value="'+slide_type+'"></input>'+
				  				'<input type="hidden" name="slideOrder" value="'+slideOrder+'"></input>'+
					  		'</form>'+
					  		'<div id="addSlideBtns-'+slideOrder+'" class="addSlideBtns hidden">'+
				      			'<span class="btn-flat add-new-text-slide">New Text</span>'+
				      			'<span class="btn-flat add-new-image-slide">New Image</span>'+
				      			'<span class="btn-flat add-new-video-slide">New Video</span>'+
				      		'</div>'+
				  		'</div>'+
				  	'</div>'+
				  '</div>';*/

  	return image_slide

};

	/*'<video id="video-'+slideOrder+'" preload="auto" class="bgvid" onloadeddata="showPlayButton(event)">'+
        '<source src="'+video_url+'" type="video/mp4">'+
    '</video>'+*/
/*function video_slide(slideShowID, slideOrder){
	var slide_type = "video";
	var video_url = '';
	var image_slide = '<div class="flex-container-center">'+
				  	'<div class="flex-container-center">'+
				  		'<div class="slide-content-container flex-item center slide-edit">'+
				  			'<div class="canEdit edit-videoUrl">Video Url</div>'+
			  				'<form id="slide-'+slideOrder+'" method="post" action="/me/save-slide" class="slide-input-container hidden">'+
				  				'<input type="hidden" name="video_embed" class="videoUrl-input"></input>'+
				  				'<input type="hidden" name="slideShowID" class="slideShowID" value="'+slideShowID+'"></input>'+
				  				'<input type="hidden" name="slideID" value=""></input>'+
				  				'<input type="hidden" name="slideType" value="'+slide_type+'"></input>'+
				  				'<input type="hidden" name="slideOrder" value="'+slideOrder+'"></input>'+
					  		'</form>'+
				  		'</div>'+
				  		'<div class="slide-done-edit content-save-btn btn-flat hidden">done editing</div>'+
				  	'</div>'+
				  '</div>';

  	return image_slide

};*/

function video_slide(slideShowID, slideOrder, videos, slideID){
	var slide_type = "video";
	var video_url = '';

	var video_options = "";
	if ( videos.length > 0 ){
		for ( var i=0; i<videos.length; i++ ){
			video_options += '<option value="'+videos[i]["download_url"]+'" class="video-url-option">'+videos[i]["filename"]+'</option>';
		};
		var video_form = '<form id="slideShowTitle-'+slideID+'" class="slide-form" action="/me/save-slide" method="post">'+
				      	'<div class="input-field col s12">'+
						    '<select name="slideVideoUrl" class="video-select" data-id="'+slideID+'">'+
						      	'<option value="" disabled selected>Choose a video</option>'+
						      	video_options+
						    '</select>'+
						    '<label>Videos</label>'+
					  	'</div>'+
				      	'<div class="row">'+
					        '<div class="input-field col s12">'+
					        	'<input type="hidden" name="slideShowID" class="slideShowID" value="'+slideShowID+'"></input>'+
				  				'<input type="hidden" name="slideID" value="'+slideID+'"></input>'+
				  				'<input type="hidden" name="slideType" value="video"></input>'+
				  				'<input type="hidden" name="slideOrder" value="'+slideOrder+'"></input>'+
				  				'<input type="hidden" id="slideVideoUrl-'+slideID+'" class="slideVideoUrl" name="slideVideoUrl" value=""></input>'+
					          	'<button type="submit" class="btn-flat">Save</button>'+
					        '</div>'+
				      	'</div>'+
	  				'</form>';
	}else{
		var video_form = '<a href="/me/my-videos">UPLOAD</a> a video.';
	};

	var video_slide = '<div id="edit-slide-'+slideID+'" class="">'+
		  	'<div class="flex-container-center">'+
		  		'<div class="slide-content-container flex-item center">'+
	  				video_form+
		  		'</div>'+
		  	'</div>'+
	  	'</div>';


	/*var video_options = "";
	if ( videos.length > 0 ){
		for ( var i=0; i<videos.length; i++ ){
			video_options += '<option value="'+videos[i]["download_url"]+'">'+videos[i]["filename"]+'</option>';
		};
		var video_form = '<form id="slideShowTitle-'+slideOrder+'" class="slide-form" action="/me/save-slide" method="post">'+
				      	'<div class="input-field col s12">'+
						    '<select name="slideVideoUrl">'+
						      	'<option value="" disabled selected>Choose a video</option>'+
						      	video_options+
						    '</select>'+
						    '<label>Videos</label>'+
					  	'</div>'+
				      	'<div class="row">'+
					        '<div class="input-field col s12">'+
					        	'<input type="hidden" name="slideShowID" class="slideShowID" value="'+slideShowID+'"></input>'+
				  				'<input type="hidden" name="slideID" value=""></input>'+
				  				'<input type="hidden" name="slideType" value="video"></input>'+
				  				'<input type="hidden" name="slideOrder" value="'+slideOrder+'"></input>'+
					          	'<button type="submit" class="btn-flat">Save</button>'+
					        '</div>'+
				      	'</div>'+
				      	'<div id="addSlideBtns-'+slideOrder+'" class="addSlideBtns hidden">'+
			      			'<span class="btn-flat add-new-text-slide">New Text</span>'+
			      			'<span class="btn-flat add-new-image-slide">New Image</span>'+
			      			'<span class="btn-flat add-new-video-slide">New Video</span>'+
				      	'</div>'+
	  				'</form>';
	}else{
		var video_form = 'You don\'t have any videos, why not <a href="/me/my-videos">UPLOAD</a> a video.';
	};

	var video_slide = '<div id="edit-slide-'+slideOrder+'" class="">'+
		  	'<div class="flex-container-center">'+
		  		'<div class="slide-content-container flex-item center">'+
	  				video_form+
		  		'</div>'+
		  	'</div>'+
	  	'</div>';
*/
	  	return video_slide


	/*<div class="input-field col s12">
    <select>
      <option value="" disabled selected>Choose your option</option>
      <option value="1">Option 1</option>
      <option value="2">Option 2</option>
      <option value="3">Option 3</option>
    </select>
    <label>Materialize Select</label>
  </div>*/
	/*var image_slide = '<div class="flex-container-center">'+
				  	'<div class="flex-container-center">'+
				  		'<div class="slide-content-container flex-item center slide-edit">'+
				  			'<div class="canEdit edit-videoUrl">Video Url</div>'+
			  				'<form id="slide-'+slideOrder+'" method="post" action="/me/save-slide" class="slide-input-container hidden">'+
				  				'<input type="hidden" name="video_embed" class="videoUrl-input"></input>'+
				  				'<input type="hidden" name="slideShowID" class="slideShowID" value="'+slideShowID+'"></input>'+
				  				'<input type="hidden" name="slideID" value=""></input>'+
				  				'<input type="hidden" name="slideType" value="'+slide_type+'"></input>'+
				  				'<input type="hidden" name="slideOrder" value="'+slideOrder+'"></input>'+
					  		'</form>'+
				  		'</div>'+
				  		'<div class="slide-done-edit content-save-btn btn-flat hidden">done editing</div>'+
				  	'</div>'+
				  '</div>';

  	return image_slide*/
};


$("body").on("focus", ".slick-slider input", function(){
	$(".slick-slider").slick("slickSetOption", "draggable", false, false);
});
$("body").on("blur", ".slick-slider input", function(){
	$(".slick-slider").slick("slickSetOption", "draggable", true, true);
});



$('body').on('click', '.slide-edit', function(){
	var $this = $(this);
	var $title = $this.children('.edit-title');
	var $description = $this.children('.edit-description');
	var $titleInput = $this.children('.slide-input-container').children('.title-input');
	var $descriptionInput = $this.children('.slide-input-container').children('.description-input');

	var $videoUrl = $this.children('.edit-videoUrl');
	var $videoUrlInput = $this.children('.slide-input-container').children('.videoUrl-input');

	$(".slick-slider").slick("slickSetOption", "draggable", false, false);
	$(".slick-slider").slick("slickSetOption", "accessibility", false, false);
	$this.siblings('.slide-done-edit').removeClass("hidden");
	$this.children('.canEdit').attr('contentEditable', true);

	$title.on('keyup', function(){
		$titleInput.val($title.text());
	});
	$description.on('keyup', function(){
		$descriptionInput.val($description.text());
	});
	$videoUrl.on('keyup', function(){
		$videoUrlInput.val($videoUrl.text());
	});

});
$('body').on('click', '.slide-done-edit', function(){
	var $this = $(this);
	var $form = $this.siblings('.slide-edit').children('.slide-input-container');
	$this.addClass("hidden");
	$(".slick-slider").slick("slickSetOption", "draggable", true, true);
	$(".slick-slider").slick("slickSetOption", "accessibility", true, true);
	$this.siblings('.slide-edit').children('.canEdit').attr('contentEditable', false);

	var success = function(data){
		console.log("saved slide");
		console.log(data);

		next_slideOrder = parseInt(data['slideOrder']) + 1;
		$(".slick-slider").data("slide-order", next_slideOrder);

		if ( parseInt( data['slideOrder']) == 1 ){

			//new_slide = text_slide(data['slideShowID'], next_slideOrder);
			$(".slick-slider").data("slideshow-id", data['slideShowID']);
			//$(".slick-slider").slick('slickAdd', new_slide);
			$("#addSlide").removeClass("hidden");
			$(".slick-slider").slick('slickNext');

		};
	}
	var method = $form.attr("method");
	var action = $form.attr("action");
	submit_form_ajax($form, method, action, success);

});



$('body').on('click', '.add-new-text-slide', function(){
	var slideShowID = $('.slick-slider').data('slideshow-id');
	var slide_order = $('.slick-slider').data('slide-order');
	//var next_slide_order = slide_order + 1;
	//new_slide = text_slide(slideShowID, next_slide_order);
	//$(".slick-slider").slick('slickAdd', new_slide);

	//$("#sortable").append(placeholder_slide);
	save_blank_slide(slideShowID, 'text', null, slide_order);
	
	//$(".slick-slider").slick('slickNext');
	//$(".slick-slider").slick("slickGoTo", slide_order);
});

var DZ_instances = {};
$('body').on('click', '.add-new-image-slide', function(){
	var slideShowID = $('.slick-slider').data('slideshow-id');
	var slide_order = $('.slick-slider').data('slide-order');
	//var next_slide_order = slide_order + 1;
	//new_slide = image_slide(slideShowID, next_slide_order);
	/*$('.slick-slider').data('slide-order', next_slide_order);
	$(".slick-slider").slick('slickAdd', new_slide);
	
	//$(".slick-slider").slick('slickNext');
	$(".slick-slider").slick("slickGoTo", slide_order);

	$(".addSlideBtns").addClass("hidden");// hide the slide buttons*/
	save_blank_slide(slideShowID, 'image', null, slide_order);
	
	/*console.log("dropzone ID: #image-slide-"+next_slide_order);
	Dropzone.autoDiscover = false;
	DZ_instances["image-slide-"+next_slide_order] = $("#image-slide-"+next_slide_order).dropzone({ 
		url: "/me/save-slide" ,
		init: function() {
	    	this.on("success", function(file, data) {
		    	console.log(data);
		    	console.log("Added file.");
		    	this.removeFile(file);
		    	$("#image-slide-"+next_slide_order).css("background-image", "url(" + data["image_url"] + ")");

		    	//$(".addSlideBtns").addClass("hidden");
				$("#addSlideBtns-"+next_slide_order).removeClass("hidden");

	    	});
	  	}
	});*/

	/*Dropzone.options.myAwesomeDropzone = {
	  init: function() {
	    this.on("addedfile", function(file) { alert("Added file."); });
	  }
	};*/


	//DZ_instances["slide-"+next_slide_order] = DZ;

	/*console.log("DZ", DZ);

	DZ.on("success", function(file, data){
		console.log(data);
    	console.log("Added file.");
    	this.removeFile(file);
    	$("#slide-"+next_slide_order).css("background-image", "url(" + data["profile_image"] + ")");
	});*/

});
$('body').on('click', '.add-new-video-slide', function(){
	var slideShowID = $('.slick-slider').data('slideshow-id');
	var slide_order = $('.slick-slider').data('slide-order');
	var next_slide_order = slide_order + 1;
	$('.slick-slider').data('slide-order', next_slide_order);

	var success = function(data){

		var videos = data;
		console.log(videos);

		save_blank_slide(slideShowID, 'video', videos, slide_order);

		/*new_slide = video_slide(slideShowID, next_slide_order, videos);
		$(".slick-slider").slick('slickAdd', new_slide);
		//$(".slick-slider").slick('slickNext');
		$(".slick-slider").slick("slickGoTo", slide_order);
		$(".addSlideBtns").addClass("hidden");// hide the add slide buttons
		//$(document).ready(function() {
	    $('select').material_select();
	  	//});*/

	};

	$.ajax({
		url: '/api/userVideos',
		type: "get",
		success: success
	}).fail(function(){
		console.log("failed to get user's videos");
	});

	/*new_slide = video_slide(slideShowID, next_slide_order);
	
	$(".slick-slider").slick('slickAdd', new_slide);*/

});


/* ================================ */
// User profile editing
/* ================================ */
$('body').on('click', '.user-edit', function(){
	console.log("user editing...");
	var $this = $(this);
	var $name = $this.find('.edit-name');
	var $surname = $this.find('.edit-surname');
	var $about = $this.find('.edit-about');
	//var $nameInput = $this.children('.user-input-container').children('.name-input');
	var $nameInput = $this.find('.name-input');
	//var $surnameInput = $this.children('.user-input-container').children('.surname-input');
	var $surnameInput = $this.find('.surname-input');
	//var $aboutInput = $this.children('.user-input-container').children('.about-input');
	var $aboutInput = $this.find('.about-input');

	$this.find('.user-done-edit').removeClass("hidden");
	$this.find('.canEdit').attr('contentEditable', true);

	$name.on('keyup', function(){
		$nameInput.val($name.text());
	});
	$surname.on('keyup', function(){
		$surnameInput.val($surname.text());
	});
	$about.on('keyup', function(){
		$aboutInput.val($about.text());
	});

});
/*Content editable version*/
$('body').on('click', '.user-done-edit', function(){
	var $this = $(this);
	//var $form = $this.siblings('.user-edit').children('.user-input-container');
	var $form = $this.siblings('.user-input-container');
	$this.addClass("hidden");

	//$this.siblings('.user-edit').children('.canEdit').attr('contentEditable', false);
	$this.find('.canEdit').attr('contentEditable', false);

	var success = function(data){
		console.log("saved user");
		console.log(data);
	}
	var method = $form.attr("method");
	var action = $form.attr("action");

	submit_form_ajax($form, method, action, success)

});
/* Plain HTML form version */
$('body').on('submit', '#userProfileEdit', function(e){
	e.preventDefault();

	var $form = $(this);
	var userType = $("#userType").val();

	var success = function(data){
		console.log("saved user");
		console.log(data);
		feedback_overlay("Saved", 1000);
		if ( data["flightCode"] && data["flightCode"].length > 0 && data["flightCode"] != "None" && userType != "artist" ){
			$("#new-idea-li").removeClass("hidden");
		};
		if ( userType != "artist" ){
			$("#"+userType+"-user-prompt").removeClass("hidden");
		};
	}
	var method = $form.attr("method");
	var action = $form.attr("action");

	if ( $("#address").val().length > 0 ){
		var geocoder = new google.maps.Geocoder();
		console.log("is there a map: ", map);
		var geocoder = new google.maps.Geocoder();
		var address = $("#address").val();
		geocodeAddress(geocoder, map, address)
	}else{
		$("#address").addClass("invalid");
	};
	if ( !$("#name").val().length > 0 ){
		$("#name").addClass("invalid");
	};
	if ( !$("#surname").val().length > 0 ){
		$("#surname").addClass("invalid");
	};

	function geocodeAddress(geocoder, map, address) {

	  	//var address = document.getElementById('address').value;

	  	geocoder.geocode({'address': address}, function(results, status) {
		    if (status === google.maps.GeocoderStatus.OK) {
		    	var latlng = results[0].geometry.location;
		    	var lat = latlng.lat();
		    	var lng = latlng.lng();
		    	var stringLocation = JSON.stringify({"lat": lat, "lng": lng});
		    	$("#ideaLocation").val(stringLocation);

		    	// set formatted address
		    	$("#address").val( results[0].formatted_address );

		    	var country = '';
		    	var locality = '';
		    	var arrAddress = results[0].address_components;
		    	$.each(arrAddress, function (i, address_component) {
				    if (address_component.types[0] == "locality"){
				        console.log("town:"+address_component.long_name);
				        locality = address_component.long_name;
				    };
				    if (address_component.types[0] == "country"){ 
				        console.log("country:"+address_component.long_name); 
				        country = address_component.long_name;
				    }; 
				});
				console.log("found address and now setting country and locality: ", country);
		    	// set country and locality inputs
		    	$("#country").val( country );
		    	$("#locality").val( locality );

		    	submit_form_ajax($form, method, action, success);
		    	
		    } else {
		      console.log('Geocode was not successful for the following reason: ' + status);
		      $("#address").addClass("invalid");
		    };
	  	});
	};


	//submit_form_ajax($form, method, action, success);

});

$('body').on('submit', '#exhibitionForm', function(e){
	e.preventDefault();

	var $form = $(this);
	var userType = $("#userType").val();

	var success = function(data){
		console.log("data: ", data);
		window.location.href = data["url"];
	};
	var method = $form.attr("method");
	var action = $form.attr("action");

	if ( $("#address").val().length > 0 ){
		var geocoder = new google.maps.Geocoder();
		console.log("is there a map: ", map);
		var geocoder = new google.maps.Geocoder();
		var address = $("#address").val();
		geocodeAddress(geocoder, map, address)
	}else{
		$("#address").addClass("invalid");
	};
	
	// validate other fields
	var title = $("#title");
	var artist_first_name = $("#artist_first_name");
	var artist_last_name = $("#artist_last_name");
	var select_artist = $("#select_artist");
	var address = $("#address");
	var ideaLocation = $("#ideaLocation");
	var date = $("#date");
	var exhibitionEndDate = $("#exhibitionEndDate");

	var error = false;
	$("#invitation_error").addClass("hidden");

	if ( !select_artist.val() && !artist_first_name.val() && !artist_last_name.val() ){
		error = true;
		select_artist.addClass("invalid");
		artist_first_name.addClass("invalid");
		artist_last_name.addClass("invalid");
	};
	console.log("error: ", error);

	if ( !title.val() ){
		if( $("#slideShowID").val().length > 0 ){
			console.log("slideshow ID: ", $("#slideShowID").val());
		}else{
			error = true;
			title.addClass("invalid");
		}
	};
	console.log("error: ", error);
	if ( !ideaLocation.val() ){
		error = true;
		address.addClass("invalid");
	};
	console.log("error: ", error);
	if ( !date.val() ){
		error = true;
		$("#invitation_error").removeClass("hidden");
	};
	console.log("error: ", error);
	if ( !exhibitionEndDate.val() ){
		error = true;
		$("#invitation_error").removeClass("hidden");
	};

	console.log("error: ", error);

	function geocodeAddress(geocoder, map, address) {

	  	geocoder.geocode({'address': address}, function(results, status) {
		    if (status === google.maps.GeocoderStatus.OK) {
		    	var latlng = results[0].geometry.location;
		    	var lat = latlng.lat();
		    	var lng = latlng.lng();
		    	var stringLocation = JSON.stringify({"lat": lat, "lng": lng});
		    	$("#ideaLocation").val(stringLocation);

		    	// set formatted address
		    	$("#address").val( results[0].formatted_address );

		    	var country = '';
		    	var locality = '';
		    	var arrAddress = results[0].address_components;
		    	$.each(arrAddress, function (i, address_component) {
				    if (address_component.types[0] == "locality"){
				        //console.log("town:"+address_component.long_name);
				        locality = address_component.long_name;
				    };
				    if (address_component.types[0] == "country"){ 
				        //console.log("country:"+address_component.long_name); 
				        country = address_component.long_name;
				    }; 
				});
				//console.log("found address and now setting country and locality: ", country);
		    	// set country and locality inputs
		    	$("#country").val( country );
		    	$("#locality").val( locality );
		    	if ( !error ){
		    		submit_form_ajax($form, method, action, success);
		    	};
		    	
		    } else {
		      //console.log('Geocode was not successful for the following reason: ' + status);
		      $("#address").addClass("invalid");
		    };
	  	});
	};

});


// not using this yet
function geocodeAddress(geocoder, map, address) {

	var deferred = $.Deferred()

  	//var address = document.getElementById('address').value;

  	geocoder.geocode({'address': address}, function(results, status) {
	    if (status === google.maps.GeocoderStatus.OK) {
	    	var latlng = results[0].geometry.location;

	    	// set formatted address
	    	$("#address").val( results[0].formatted_address );

	    	var country = '';
	    	var locality = '';


	    	var arrAddress = results[0].address_components;
	    	$.each(arrAddress, function (i, address_component) {
			    if (address_component.types[0] == "locality"){
			        console.log("town:"+address_component.long_name);
			        locality = address_component.long_name;
			    };
			    if (address_component.types[0] == "country"){ 
			        console.log("country:"+address_component.long_name); 
			        country = address_component.long_name;
			    }; 
			});

	    	// set country adn locality inputs
	    	$("#country").val( country );
	    	$("#locality").val( locality );

	    	deferred.resolve({
                country: country,
                locality: locality,
                latlng: results[0].geometry.location
            });

	    	//placeMarkerAndPanTo(latlng, map);
	    } else {
	      alert('Geocode was not successful for the following reason: ' + status);
	      deferred.reject();
	    };
  	});
};

// ========================================================
//  Toggle Map markers
// ========================================================

function toggle_beam(){
	$(".beam").removeClass("hidden");
	setTimeout(function(){
		$(".beam").addClass("hidden");
	}, 2000);
};

/*$('body').on('click', "#showAirspace", function(){
	console.log("Show Airpsace");
	toggle_beam();
	for ( var i=0; i<markers.length; i++ ){
		var category = markers[i]["category"];
		var status = markers[i]["title"];
		if ( category == "artist" ){
			if ( status == "in flight" || status == "waiting to land" || status == "taking off" || status == "arriving soon" ){
				markers[i].setVisible(true);
			}else{
				markers[i].setVisible(false);
			};
		}else{
			//console.log("hide marker");
			markers[i].setVisible(false);
		};
	};
});
$('body').on('click', "#showGround", function(){
	console.log("Show showGround");
	toggle_beam();
	for ( var i=0; i<markers.length; i++ ){
		var category = markers[i]["category"];
		var status = markers[i]["title"];
		if ( category == "airport" || category == "freeport" || category == "artist" ){
			if ( status != "in flight" && status != "waiting to land" && status != "taking off" && status != "arriving soon" ){
				markers[i].setVisible(true);
			}else{
				markers[i].setVisible(false);
			};
		}else{
			//console.log("hide marker");
			markers[i].setVisible(false);
		};
	};
});
$('body').on('click', "#showAirport", function(){
	console.log("Show showAirport");
	toggle_beam();
	for ( var i=0; i<markers.length; i++ ){
		if ( markers[i]["category"] == "airport" ){
			markers[i].setVisible(true);
		}else{
			//console.log("hide marker");
			markers[i].setVisible(false);
		};
	};

	//console.log("markers: ", markers);
	//console.log("map: ", map);
});
$('body').on('click', "#showFreeport", function(){
	console.log("Show showFreeport");
	toggle_beam();
	for ( var i=0; i<markers.length; i++ ){
		if ( markers[i]["category"] == "freeport" ){
			markers[i].setVisible(true);
		}else{
			//console.log("hide marker");
			markers[i].setVisible(false);
		};
	};
});
$('body').on('click', "#showAll", function(){
	console.log("Show showAll");
	toggle_beam();
	for ( var i=0; i<markers.length; i++ ){
		markers[i].setVisible(true);
	};
});*/




// ========================================================
//  User interactions / invites, flight permissions etc
// ========================================================


/*$('body').on('change', '.invite-button', function(){

	var $this = $(this);
	var slideShowID = $this.data("id");

	function toggle_invitation(slideShowID, invited){
		$.ajax({
			url: "/toggle_invitation",
			type: "post",
			data: { slideShowID: slideShowID, invited: invited },
			success: function(){
				console.log("successfully toggled invitation");
			}
		})
	};

	if ($this.is(':checked')){
		//console.log(".... invited: " + slideShowID);
		toggle_invitation(slideShowID, true);
	}else{
		//console.log(".... uninvited " + slideShowID);
		toggle_invitation(slideShowID, false);
	};

});*/

















