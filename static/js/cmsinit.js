(function($){
  $(function(){

    $('.button-collapse').sideNav();

    //$('.collapsible').collapsible();



    $('.datepicker').pickadate({
	    selectMonths: true, // Creates a dropdown to control month
	    selectYears: 15 // Creates a dropdown of 15 years to control year
  	});

  	$('select').material_select();

    // Medium Editor
    var elements = document.querySelectorAll('.editable'),
    editor = new MediumEditor(elements, {
    	paste: {
    		forcePlainText: true,
        	cleanPastedHTML: true,
        	cleanAttrs: ['style', 'dir', 'class'],
        	cleanTags: ['html', 'body', 'meta', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'ol', 'ul', 'li', 'div', 'span', 'title']
    	},
    	toolbar: {
	        allowMultiParagraphSelection: true,
	        buttons: ['bold', 'italic', 'underline', 'anchor', 'h3', 'h4', 'orderedlist', 'unorderedlist', 'justifyLeft', 'justifyRight', 'justifyCenter', 'quote'],
	        diffLeft: 0,
	        diffTop: -10,
	        firstButtonClass: 'medium-editor-button-first',
	        lastButtonClass: 'medium-editor-button-last',
	        standardizeSelectionStart: false,
	        static: false,
	        relativeContainer: null,
	        /* options which only apply when static is true */
	        align: 'center',
	        sticky: false,
	        updateOnEmptySelection: false
	    },
    	placeholder: {
        text: 'Type here'
    }
	});

  }); // end of document ready
})(jQuery); // end of jQuery name space