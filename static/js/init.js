(function($){
  $(function(){

  	//Materialize
  	$('.datepicker').pickadate({
	    selectMonths: true, // Creates a dropdown to control month
	    selectYears: 15, // Creates a dropdown of 15 years to control year
	    format: 'yyyy/mm/dd'
  	});

  	/*$('.button-collapse').sideNav({
      	edge: 'right', // Choose the horizontal origin
    });*/

    /*setTimeout(function(){
    	console.log("trying to pop it right");
    	$(window).trigger("resize");

    }, 500);*/


	// Extend jQuery for animate.css
	$.fn.extend({
	    animateCss: function (animationName) {
	        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
	        $(this).addClass('animated ' + animationName).one(animationEnd, function() {
	            $(this).removeClass('animated ' + animationName);
	        });
	    }
	});



    // Medium Editor
    var elements = document.querySelectorAll('.editable'),
    editor = new MediumEditor(elements, {
    	paste: {
    		forcePlainText: true,
        	cleanPastedHTML: true,
        	cleanAttrs: ['style', 'dir', 'class'],
        	cleanTags: ['html', 'body', 'meta', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'ol', 'ul', 'li', 'div', 'span', 'title']
    	},
    	toolbar: {
	        allowMultiParagraphSelection: true,
	        buttons: ['bold', 'italic', 'underline', 'anchor', 'h3', 'h4', 'orderedlist', 'unorderedlist', 'justifyLeft', 'justifyRight', 'justifyCenter', 'quote'],
	        diffLeft: 0,
	        diffTop: -10,
	        firstButtonClass: 'medium-editor-button-first',
	        lastButtonClass: 'medium-editor-button-last',
	        standardizeSelectionStart: false,
	        static: false,
	        relativeContainer: null,
	        /* options which only apply when static is true */
	        align: 'center',
	        sticky: false,
	        updateOnEmptySelection: false
	    },
    	placeholder: {
        text: ''// was the placeholder text 'Type here'
    }
	});


	// Dropzone initialisation

	var myDropzone = new Dropzone("#dropzone");

	myDropzone.options.myAwesomeDropzone = {
	  paramName: "file", // The name that will be used to transfer the file
	  maxFilesize: 25, // MB
	};

	// disable all other forms on the page
	myDropzone.on("sending", function(){
		$('form').children(':input').attr('disabled', 'disabled');
		$('form').children(':button').attr('disabled', 'disabled');
	});

	myDropzone.on("success", function(data, response) {
    	
		if ( response["message"] == "success" && response["entityID"] ){
			//alert(JSON.stringify(response));
	    	$("#entityID").val(response["entityID"]);

	    	// specific to ideas... should probably refactor
	    	$("#ideaID").val(response["entityID"]);
	    	console.log("give it and ID!!!!!!! ", $("#ideaID").val());
	    	console.log(response["entityID"]);
	    	$("#uploadedFiles").append(response["preview_html"]);

		}else{
			$errorModal = $("#errorModal");
			var errors = data["errors"];
			$errorMessage = $errorModal.find("#errorMessage");
			for ( var i=0; i<errors.length; i++ ){
				$errorMessage.html("");
				$errorMessage.append(errors[i]+"<br>");
			};
			$errorModal.openModal();
		};
    });
    myDropzone.autoDiscover = false;
    myDropzone.on("complete", function(file) { 
    	this.removeFile(file);
    });


    // Video Dropzone

	/*var videoDropzone = new Dropzone("#videoDropzone");
	videoDropzone.autoDiscover = false;
	videoDropzone.options.myAwesomeDropzone = {
	  paramName: "file", // The name that will be used to transfer the file
	  autoProcessQueue: false,// need to trigger upload automatically
	};*/
	//$("div#videoDropzone").dropzone({ url: "#" });
	
  }); // end of document ready
})(jQuery); // end of jQuery name space