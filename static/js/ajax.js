
function submit_form_ajax($form, method, action, success){

  $form.find('button').prop('disabled', true);
  $form.find('input[type="submit"]').prop('disabled', true);
  var form = $form[0]; // You need to use standart javascript object here
  var formData = new FormData(form);

  console.log("formDATA serialize: ", $form.serialize());

  if (!method){
    method = $form.attr("method");
  };
  if (!action){
    action = $form.attr("action");
  };

  var loader = $form.find(".loader-icon");
  var loader_success = $form.find(".loader-success");
  var loader_error = $form.find(".loader-error");
  // reset loaders
  if(!loader_success.hasClass("hide")){
    loader_success.addClass("hide");
  };
  if(!loader_error.hasClass("hide")){
    loader_error.addClass("hide");
  };

  loader.toggleClass("hide");

  function success_fn(data){
    loader.addClass("hide");
    loader_error.addClass("hide");
    loader_success.removeClass("hide");

    setTimeout(function(){
      loader_success.addClass("hide");
    }, 3000);

    $form.find('button').prop('disabled', false);
    $form.find('input[type="submit"]').prop('disabled', false);

    success(data);

  };

  $.ajax({
      url: action,
      type: method,
      data: formData,
      
      // THIS MUST BE DONE FOR FILE UPLOADING
      contentType: false,
      processData: false,
      
      success: success_fn
  }).fail(function(){
    if ( Materialize ){
      Materialize.toast('Something went wrong, please try again later.', 4000);
    }else{
      alert("something went wrong");
    };
    /*$form.find('button').prop('disabled', false);
    $form.find('input[type="submit"]').prop('disabled', false);
    loader.addClass("hide");
    loader_error.removeClass("hide");
    loader_success.addClass("hide");*/
  });

};

function feedback_overlay(message, time){

  $('body').append('<div id="feedback_overlay"><div id="feedback_overlay_message">'+message+'</div></div>');
  setTimeout(function(){
    $("#feedback_overlay").remove();
  }, time);

};







