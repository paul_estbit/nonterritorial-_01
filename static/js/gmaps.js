
// =================================
// SVG ICONS
// =================================

var iconA = '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 23 21" ><g id="marker"><circle class="core" cx="11.3" cy="10.5" r="0.1"/><circle class="ring" fill="none" stroke="#000000" stroke-miterlimit="10" cx="11.3" cy="10.5" r="1" stroke-width="1"/></g></svg>'







// =====================================
// User / Artist / Curator Google Maps
// =====================================

// Specify features and elements to define styles.
	  	/*var styleArray = [
		    {
		      featureType: "all",
		      stylers: [
		       { saturation: -80 }
		      ]
		    },{
		      featureType: "road.arterial",
		      elementType: "geometry",
		      stylers: [
		        { hue: "#00ffee" },
		        { saturation: 50 }
		      ]
		    },{
		      featureType: "poi.business",
		      elementType: "labels",
		      stylers: [
		        { visibility: "off" }
		      ]
		    }
	  	];*/

	  	var styleArray = [{
				"stylers": [{
					"visibility": "simplified"
				}]
			}, {
				"stylers": [{
					"color": "#131314"
				}]
			}, {
				"featureType": "water",
				"stylers": [{
					"color": "#131313"
				}, {
					"lightness": 7
				}]
			}, {
				"elementType": "labels.text.fill",
				"stylers": [{
					"visibility": "on"
				}, {
					"lightness": 25
				}]
			},
				{
			    "elementType": "labels.text",
			    "stylers": [
			      { "visibility": "off" }
			    ]
			  }
			];

	  	var $map = $("#gmap");
	  	var map;
	  	var marker;
	  	var initialLocation;

		function initMap() {

			// fallback initial location
			if ( $("#ideaLocation").val() ){
				//initialLocation = JSON.parse( $("#ideaLocation").val() );
				loc = $("#ideaLocation").val();
				console.log("loc.............", loc);
				if ( loc.length > 0 ){
					initialLocation = {
						lat: parseFloat( loc.split(",")[0] ),
						lng: parseFloat( loc.split(",")[1] )
					}
				}else{
					initialLocation = null;
				};
			}else{
				initialLocation = null;
			};

		  	console.log("initialLocation", initialLocation);
		  	if ( !initialLocation ){
		  		initialLocation = {lat: 41.902678, lng: 12.453403};
		  	};

		  	map = new google.maps.Map(document.getElementById('gmap'), {
			    //center: {lat: -34.397, lng: 150.644},
			    center: initialLocation,
			    zoom: 3,
			    styles: styleArray
		  	});

		  	map.addListener('click', function(e) {
		    	placeMarkerAndPanTo(e.latLng, map);
		  	});

		  	// Geocoding
		  	var geocoder = new google.maps.Geocoder();
			$("body").on("click", "#geocodeAddress", function(){
				console.log("going to geocode address");
				geocodeAddress(geocoder, map);
			});

		  	map.addListener('click', function(e) {
		  		geocodeLocation(geocoder, map, e.latLng);
		    	placeMarkerAndPanTo(e.latLng, map);
		  	});

		  	console.log("initialLocation", initialLocation);
		  	placeMarkerAndPanTo(initialLocation, map);

		};

		function placeMarkerAndPanTo(latLng, map) {
			console.log("Place Marker");
			console.log(latLng);
			console.log(marker);

			/*var image = new google.maps.MarkerImage(
	            '/static/img/circle.png',
	            null, // size
	            null, // origin
	            new google.maps.Point( 8, 8 ), // anchor (move to center of marker)
	            new google.maps.Size( 17, 17 ) // scaled size (required for Retina display icon)
	        );*/

		  	if ( !marker ){
		  			console.log("placing 1st marker");
			  		marker = new google.maps.Marker({
			    	//flat: true,
            		//icon: image,
            		title: "parked",
			    	position: latLng,
			    	map: map
	  			});
			}else{
				console.log("placing existing marker");
				marker.setPosition( latLng );
			};
		  	map.panTo(latLng);
		  	$("#ideaLocation").val(JSON.stringify(latLng));

		  	console.log('marker: ', marker);

		};

		// setting map to a users location
	  	/*if (navigator.geolocation) {
	  		$(".loading-container").removeClass("hidden");
        	$map.addClass("hidden");
		     navigator.geolocation.getCurrentPosition(function (position) {
		     	console.log("position", position)
		     	if ( !position.coords.lattitude && !position.coords.longitude ){
		     		map.setCenter(initialLocation);
		     	}else{
			        initialLocation = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
			        map.setCenter(initialLocation);
			    };

		        $(".loading-container").addClass("hidden");
		        $map.removeClass("hidden");

		        // place initial marker
		        marker = new google.maps.Marker({
				    position: initialLocation,
				    map: map
			  	});
			  	map.panTo(initialLocation);

			  	console.log("marker", marker);

		     });
		};*/

// ===========================================================================
// Some page specific functions to render map in hidden collapsible etc.
// ===========================================================================

$("body").on("click", ".collapsible-header", function(){
	google.maps.event.trigger(map, 'resize');
	map.panTo(initialLocation);
});






/*  Geocoding */


function geocodeAddress(geocoder, map) {

  	var address = document.getElementById('address').value;
  	geocoder.geocode({'address': address}, function(results, status) {
	    if (status === google.maps.GeocoderStatus.OK) {
	    	var latlng = results[0].geometry.location;
	    	console.log("GEOCODE RESULT::::::::");
	    	console.log(results)
	    	//console.log(results[1].formatted_address);

	    	// set formatted address
	    	$("#address").val( results[0].formatted_address );

	    	var country = '';
	    	var locality = '';


	    	var arrAddress = results[0].address_components;
	    	$.each(arrAddress, function (i, address_component) {
			    if (address_component.types[0] == "locality"){
			        console.log("town:"+address_component.long_name);
			        locality = address_component.long_name;
			    };
			    if (address_component.types[0] == "country"){ 
			        console.log("country:"+address_component.long_name); 
			        country = address_component.long_name;
			    }; 
			});

	    	// set country adn locality inputs
	    	$("#country").val( country );
	    	$("#locality").val( locality );

	    	placeMarkerAndPanTo(latlng, map);
	    } else {
	      alert('Geocode was not successful for the following reason: ' + status);
	    };
  	});
};

function geocodeLocation(geocoder, map, latlng) {
  	geocoder.geocode({'location': latlng}, function(results, status) {
      	if (status === google.maps.GeocoderStatus.OK) {
	    	var latlng = results[0].geometry.location;
	    	console.log("GEOCODE RESULT::::::::");

	    	// set formatted address
	    	$("#address").val( results[0].formatted_address );
	    	// in case there's a display address... note its .text(), not .val()
	    	$("#displayAddress").text( results[0].formatted_address );

	    	var country = '';
	    	var locality = '';


	    	var arrAddress = results[0].address_components;
	    	$.each(arrAddress, function (i, address_component) {
			    if (address_component.types[0] == "locality"){
			        console.log("town:"+address_component.long_name);
			        locality = address_component.long_name;
			    };
			    if (address_component.types[0] == "country"){ 
			        console.log("country:"+address_component.long_name); 
			        country = address_component.long_name;
			    }; 
			});

	    	// set country adn locality inputs
	    	$("#country").val( country );
	    	$("#locality").val( locality );
    	} else {
      		alert('Geocode was not successful for the following reason: ' + status);
    	};
  	});
}





