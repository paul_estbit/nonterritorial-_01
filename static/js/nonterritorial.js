
//========================================================
//   Admin.html
//========================================================

$("body").on("submit", "#tagForm", function(e){
      e.preventDefault();

      var $form = $(this);
      var method = $form.attr("method");
      var action = $form.attr("action");

      var success = function(data){
        var tags = data["tags"];

        if( tags.length > 0 ){
          for ( i=0; i<tags.length; i++ ){

            var tag = tags[i]["tag"];
            var tagID = tags[i]["tagID"];
            var html = '<div class="chip">'+tag+'<i class="material-icons" data-tag-id="'+tagID+'">close</i></div>';
            $("#tagList").append(html);

          };
        };
        $("#tagForm")[0].reset();

      };

      console.log("save these tags....");

      submit_form_ajax($form, method, action, success);

});

$("body").on("click", ".delete-tag", function(){

      var tagID = $(this).data("tag-id");

      function success(data){
        console.log(data);
      };

      $.ajax({
        type: "post",
        url: "/admin/remove-tag",
        data: {"tagID": tagID},
        success: success
      }).fail(function(){
        alert("something went wrong, please try again later")
      });

});




//========================================================
//   Home page video
//========================================================

$(document).on('ready',function(){


    if($('.intro_video_w').length > 0 && !sessionStorage.getItem('videoViewed')){

        var videoPath = '/static/video/Natacha_Nisic_Extrait-30806_compressed.mp4';

        // Create request
        var req = new XMLHttpRequest();
        req.open('GET', videoPath, true);
        req.responseType = 'blob';

        req.onload = function() {

            if (this.status === 200) {

                var videoBlob = this.response;
                var vid = URL.createObjectURL(videoBlob); // IE10+

                $('.intro_video video')[0].src = vid;

                $('.video_loader_w').remove();
                $('.video_w').show();

            }

        }

        req.addEventListener('progress', function(e){

            if(e.lengthComputable){

                var diff = e.loaded/e.total;
                var prcnt = diff*100;

                // Update the progress bar
                $('.vl_progress_bar span').css({ left: -(100-prcnt)+'%' });
                // Update the text
                $('.vl_progress_text').text('Loading '+Math.round(prcnt)+'%');

            }

        });

        req.send();

        // Bind video close
        $('.close_video i').on('click',function(){

            $('.intro_video video')[0].pause();
            $('.intro_video_w').css({ opacity: 0 });

            var t = setTimeout(function(){ $('.intro_video_w').remove() },250);

        });

        sessionStorage.setItem('videoViewed','yes');

    }
    else $('.intro_video_w').remove();
});

//========================================================
//   Responsive stuff
//========================================================
$(window).on('load resize',function(){

    $('#map svg').css({ width: window.innerWidth, height: window.innerHeight });

});


//========================================================
//   user-page.html
//========================================================


$("body").on("click", ".requires-password", function(){
  $(this).next(".slide-vertical").addClass("open");
});
$("body").on("blur", ".requires-password", function(){
  $(this).next(".slide-vertical").removeClass("open");
});

$("body").on("submit", ".changeSettingsForm", function(e){
  e.preventDefault();
  var $form = $(this);
  var method = $form.attr("method");
  var action = $form.attr("action");
  var success = function(data){
    if ( data["message"] == "success" ){

      console.log(".................. e");
      $("#savedModal").openModal();

    }else{
      $errorModal = $("#errorModal");
      var errors = data["errors"];
      $errorMessage = $errorModal.find("#errorMessage");
      for ( var i=0; i<errors.length; i++ ){
        $errorMessage.html("");
        $errorMessage.append(errors[i]+"<br>");
      };
      $errorModal.openModal();
    };
  };
  submit_form_ajax($form, method, action, success);
});

$("body").on("click", ".selectable-tag-input", function(){

  var $input =  $("#tags");
  var $this = $(this);

  // not sure if this works, but is trying to add the hidden input if its not there
  if ( !$input.length ){
    $this.parents("form").append( '<input type="hidden" id="tags" name=""tags>' );
    $input =  $("#tags");
  };

  var tagID = $this.data("tag-id");

  var tags = $input.val();

  function append_tag(tag_array, tagID){
    if ( tag_array.length > 0 ){
      tag_array += ","+tagID;
    }else{
      tag_array = tagID;
    };
    //console.log("tag array: ", tag_array);
    return tag_array
  };

  if ( !$this.hasClass("selected") ){
    tags = append_tag(tags, tagID);
    $input.val(tags);
    $this.addClass("selected");
  }else{
    var tag_array = tags.split(",");
    var new_tags = [];
    for ( var i=0; i<tag_array.length; i++ ){
      if ( tag_array[i] != tagID ){
        new_tags.push(tag_array[i]);
      };
    };
    tags = new_tags.join(",");
    $input.val(tags);
    $this.removeClass("selected");
  };


});
